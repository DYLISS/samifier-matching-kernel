***********************************
Documentation of reference database
***********************************

Main importer
=============
.. automodule:: matching_kernel.reference_database.main_importer
   :members:
   
Import MetaCyc
==============
.. automodule:: matching_kernel.reference_database.import_metacyc
   :members:

Import MetaNetx
===============
.. automodule:: matching_kernel.reference_database.import_metanetx
   :members:

Mongo Wrapper
=============
.. automodule:: matching_kernel.reference_database.mongo_wrapper
   :members:
