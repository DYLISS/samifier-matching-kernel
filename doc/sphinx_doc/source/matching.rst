*********************************
Documentation of matching package
*********************************

Exact identifier matching
=========================
.. automodule:: matching_kernel.matching.exact_identifier_matching
   :members:

Exact name matching
===================
.. automodule:: matching_kernel.matching.exact_name_matching
   :members:

Get in validation
=================
.. automodule:: matching_kernel.matching.get_in_validation
   :members:
   
Inferred name matching
======================
.. automodule:: matching_kernel.matching.inferred_name_matching
   :members:
   
Samir
=====
.. automodule:: matching_kernel.matching.samir
   :members:
