******************************************
Documentation of Input file parser package
******************************************

Check integrity
===============
.. automodule:: matching_kernel.input_file_parser.check_integrity
   :members:

Populate database
=================
.. automodule:: matching_kernel.input_file_parser.populate_database
   :members: