********************************
Documentation of convert package
********************************

Convert identifiers
===================
.. automodule:: matching_kernel.convert.convert
   :members:

Convert names
=============
.. automodule:: matching_kernel.convert.elementnames_handling
   :members:
