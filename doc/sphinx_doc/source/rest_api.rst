********************************
Documentation of REST API module
********************************

REST API
========
.. automodule:: matching_kernel.rest_api
   :members:
