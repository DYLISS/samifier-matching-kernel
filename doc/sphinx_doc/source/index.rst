.. Matching Kernel documentation master file, created by
   sphinx-quickstart on Sun Mar 20 22:21:27 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Matching Kernel's documentation!
===========================================


Database Conceptual Data Model (CDM)

.. image:: _static/schema-mars-2016.png
   :align: center

:Download: `Image in high quality <./_images/schema-mars-2016.png>`_.

Contents:

.. toctree::
   :maxdepth: 4

   convert
   rest_api
   unifier_database
   reference_database
   input_file_parser
   output_file_writer
   matching
   test
   commons


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

