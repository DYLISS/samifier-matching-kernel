CMD_PYTHON=python3
PYTHON=$(CMD_PYTHON) -m matching_kernel
#LOGLEVEL=--loglevel=debug
#LOGLEVEL=--loglevel=info

COMMAND=$(PYTHON) $(LOGLEVEL)

install:
	pip3 install -r requirements.txt

dev_flask_start:
	# 1 worker, bind localhost:5000
	# Binding to nginx proxy
	gunicorn --log-level=debug --timeout 10 --workers 2 --threads 4 --bind 127.0.0.1:4000 matching_kernel.rest_api:app

unit_test:
	@echo Unit tests...
	py.test

clean_metacyc_dump:
	@echo Update references on metabolites...
	@sed -i -e 's#BIGG:#BIGG1:#g' ./data/all_metabolites.tbl
	@sed -i -e 's#LIGAND-CPD:#KEGG:#g' ./data/all_metabolites.tbl
	@sed -i -e 's#KEGG-GLYCAN:#KEGG:#g' ./data/all_metabolites.tbl

	@echo Update references on reactions...
	@sed -i -e 's#LIGAND-RXN#KEGG#g' ./data/all_reactions.tbl
	@sed -i -e 's#EC-#ECNUMBER:#g' ./data/all_reactions.tbl

	@echo "Remove bad identifiers (bad separators)..."
	@sed -i -e 's#::#:#g' ./data/all_metabolites.tbl
	@sed -i -e 's#;PANTHER:PTHR11551:SF4##g' ./data/all_metabolites.tbl
	@sed -i -e 's#;PANTHER:PTHR11551:SF4##g' ./data/all_metabolites.tbl
	@sed -i -e 's#;PANTHER:PTHR23426:SF8##g' ./data/all_metabolites.tbl

	@sed -i -e 's#OCTADECNUMBER:9-ENE-118-DIOIC-ACID#OCTADEC-9-ENE-118-DIOIC-ACID#g' ./data/all_metabolites.tbl
	@sed -i -e 's#OCTADECNUMBER:9-ENE-118-DIOIC-ACID#OCTADEC-9-ENE-118-DIOIC-ACID#g' ./data/all_reactions.tbl

	@sed -i -e 's#POLY-OH-DECNUMBER:N#POLY-OH-DEC-N#g' ./data/all_metabolites.tbl
	@sed -i -e 's#POLY-OH-DECNUMBER:N#POLY-OH-DEC-N#g' ./data/all_reactions.tbl

# Be careful with the order of commands for "SECNUMBER:tRNAs" replacement...
	@sed -i -e 's#L-seryl-SECNUMBER:tRNAs#L-seryl-SEC-tRNAs#g' ./data/all_metabolites.tbl
	@sed -i -e 's#L-seryl-SECNUMBER:tRNAs#L-seryl-SEC-tRNAs#g' ./data/all_reactions.tbl

	@sed -i -e 's#Charged-SECNUMBER:tRNAs#Charged-SEC-tRNAs#g' ./data/all_metabolites.tbl
	@sed -i -e 's#Charged-SECNUMBER:tRNAs#Charged-SEC-tRNAs#g' ./data/all_reactions.tbl

# At the end !
	@sed -i -e 's#SECNUMBER:tRNAs#tRNA-Sec#g' ./data/all_metabolites.tbl
	@sed -i -e 's#SECNUMBER:tRNAs#tRNA-Sec#g' ./data/all_reactions.tbl

pinput:
	@echo Bulk insertions in reference database...
	$(COMMAND) parse_input_file

init_db_idx:
	@echo Database indexes initilization...
	$(COMMAND) init_db_idx
	
init_db:
	@echo Database initilization...
	$(COMMAND) init_db

t:
	$(COMMAND) test

git_ssh:
	ssh-add ~/.ssh/id_forge_inria

push:
	git push inria master

updatedoc:
	@echo Update sphinx doc
	cd ./doc/sphinx_doc/ && make html

zipdoc:
	# remove prefix '/doc/sphinx_doc/' from path
	tar cjvf doc.tar.bz2 --strip-components=2 ./doc/sphinx_doc/build

version:
	$(COMMAND) --version

help:
	$(COMMAND) --help

