# -*- coding: utf-8 -*-
"""Unit tests to support the encoding / decoding of the identifiers
from the various databases managed.

"""

# Standard imports
import pytest

# Custom imports
from matching_kernel.convert \
    import decode_bigg, \
           decode_metacyc, \
           encode_metacyc, \
           encode_bigg, \
           universal_decoder


@pytest.fixture()
def feed_bigg_decoder():

    return {'M_tartr__L_e': ('tartr__L', 'e'),
            'M_tartr_DASH_L_e': ('tartr__L', 'e'),
            'M_taur_c': ('taur', 'c'),
            'M_12ppd_DASH_S_c': ('12ppd__S', 'c'),
            'R_12PPDt': ('12PPDt', None),
            'R_EX_12ppd__S_e': ('EX_12ppd__S', 'e'),
            'R_MAN6Pt6_2': ('MAN6Pt6_2', None),
            '_4M__45__TOTO_c': None,
            '2-oxoglutarate-dehydrogenase-E2-lipoyl-c': None,
            '4M-TOTO_c': None,
            '_9__45__cis__45__Epoxycarotenoids': None,
            }


@pytest.fixture()
def feed_metacyc_decoder():

    return {'_4M__45__TOTO_c': ('4M-TOTO', 'c'),
            '2-oxoglutarate-dehydrogenase-E2-lipoyl-c': ('2-oxoglutarate-dehydrogenase-E2-lipoyl-c', None),
            '_9__45__cis__45__Epoxycarotenoids': ('9-cis-Epoxycarotenoids', None),
           }


@pytest.fixture()
def feed_basic_universal_decoder():

    return {'M_tartr__L_e': ('tartr__L', 'e'),
            'M_tartr_DASH_L_e': ('tartr__L', 'e'),
            'M_taur_c': ('taur', 'c'),
            'M_12ppd_DASH_S_c': ('12ppd__S', 'c'),
            'R_12PPDt': ('12PPDt', None),
            'R_EX_12ppd__S_e': ('EX_12ppd__S', 'e'),
            'R_MAN6Pt6_2': ('MAN6Pt6_2', None),
            '_4M__45__TOTO_c': ('4M-TOTO', 'c'),
            '2-oxoglutarate-dehydrogenase-E2-lipoyl-c': ('2-oxoglutarate-dehydrogenase-E2-lipoyl-c', None),
            '4M-TOTO_c': ('4M-TOTO', 'c'),
            '_9__45__cis__45__Epoxycarotenoids': ('9-cis-Epoxycarotenoids', None),
            'S__40_15S_41__45_15_45_Hydroxy_45_5_44_8_44_11_45_cis_45_13_45_trans_45_eicosatetraenoate_c': ('d461e03952d4e7efbd538cc9363691e9', 'c'),
            'M__45__CRESOL__45__METHYLCATECHOL__45__RXN': ('M-CRESOL-METHYLCATECHOL-RXN', None),
            'R__45__2__45__HYDROXYGLUTARATE': ('R-2-HYDROXYGLUTARATE', None),
            }


@pytest.fixture()
def feed_advanced_universal_decoder():

    return {('_4M__45__TOTO', "bigg"): None,
           }


def test_decode_bigg(feed_bigg_decoder):
    """Handle BiGG identifiers and not handle Metacyc identifiers.
    Should return None (Metacyc ids passed to a bigg function)
    """

    for key, value in feed_bigg_decoder.items():
        assert decode_bigg(key) == value


def test_decode_metacyc(feed_metacyc_decoder):
    """Handle Metacyc identifiers.
    """

    for key, value in feed_metacyc_decoder.items():
        assert decode_metacyc(key) == value


def test_encode_metacyc():
    """Test encoding parameters according to Metacyc rules
    """

    assert encode_metacyc('4M-TOTO_c') == '_4M__45__TOTO_c'
    assert encode_metacyc('|M-TOTO', compartment='c') == '__124__M__45__TOTO_c'


def test_encode_bigg():
    """Test encoding parameters according to Bigg rules
    """

    assert encode_bigg('12PPDt', metabolite=False, compartment='c') == 'R_12PPDt_c'


def test_universal_decoder(feed_basic_universal_decoder,
                           feed_advanced_universal_decoder):
    """Handle all identifiers. With and without specify the database.

    .. note:: The 2 very problematic elements (Metacyc)
        that are handled despite everything, are fixed with this function:
            - 'M__45__CRESOL__45__METHYLCATECHOL__45__RXN'
            - 'R__45__2__45__HYDROXYGLUTARATE'

    """

    # Test VERY ugly identifiers
    assert universal_decoder(
            'S__40_15S_41__45_15_45_Hydroxy_45_5_44_8_44_11_45_cis_45_13_45_trans_45_eicosatetraenoate_c'
           ) == ('d461e03952d4e7efbd538cc9363691e9', 'c')

    for key, value in feed_basic_universal_decoder.items():
        assert universal_decoder(key) == value

    for key, value in feed_advanced_universal_decoder.items():
        assert universal_decoder(*key) == value



#    print(clean_name('_2-Oxo-3-hydroxy-4-phosphobutanoate'))
#    print(fh.html_entities_to_names(fh.html_entities_to_names('galactosaminyl-&amp;alpha;1,3-N,N&apos;-diacetylbacillosaminyl-&amp;alpha;1-di')))

