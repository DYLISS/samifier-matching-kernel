# -*- coding: utf-8 -*-
"""
This module includes functions used to clean names from Metacyc raw files.

:Documentation & examples about html entities:
    - https://docs.python.org/3/library/html.html#html.unescape
    - https://docs.python.org/3/library/html.entities.html
    - https://alexandre.alapetite.fr/doc-alex/alx_special.html

    .. code-block:: python
        :linenos:

        import html.entities as he
        print(he.entitydefs)
        print(he.html5)


:Examples of raw files export from metacyc:

    - Metacyc: a β-D-galactosyl-(1,4)-N-acetyl-β-D-glucosaminyl-(1-3)-β-D-galactosyl-1,4-β-D-glucosyl-(1↔1)-ceramide
    - Metacyc bdd: a &beta;-D-galactosyl-(1,4)-N-acetyl-&beta;-D-glucosaminyl-(1-3)-&beta;-D-galactosyl-1,4-&beta;-D-glucosyl-(1&harr;1)-ceramide
    - chebi: β-D-galactosyl-(1→4)-N-acetyl-β-D-galactosaminyl-(1→3)-β-D-galactosyl-(1→4)-β-D-glucosylceramide
    - chebi ascii: beta-D-galactosyl-(1->4)-N-acetyl-beta-D-galactosaminyl-(1->3)-beta-D-galactosyl-(1->4)-beta-D-glucosylceramide

    .. code-block:: javascript

        {'&pi;', '&alpha;', '&Delta;', '&mu;', '&chi;', '&plusmn;', '&tau;',
        '&DElta;', '&omega;', '&zeta;', '&gamma;', '&psi;', '&harr;', '&kappa;',
        '&lambda;', '&beta;', '&iota;', '&xi;', '&epsilon;', '&rarr;', '&nu;',
        '&delta;'}

:Problems:
    - &pi => pros
        CPD-1823, Nπ-methyl-L-histidine,
        The nitrogen atoms of the imidazole ring of histidine are denoted by pros
        ('near', abbreviated π) and tele ('far', abbreviated τ)
        to show their position relative to the side chain

        http://goldbook.iupac.org/P04890.html
    - &tau => τ => tele
        N-METHYL-HISTAMINE &
        Nτ-methylhistamine
    - &harr => ↔ => <->
    - &rarr => → => ->
    - &plusmn => ± => +-
        CPD-16445 (±)-pavine


:SBML metacyc:

    .. code-block:: javascript

        {'&quot;', '&gt;', '&amp;', '&apos;'}

    Ceci, plus toutes les versions au dessus préfixées de &amp; car il y a double
    encodage... cf juste en dessous:

:SBML metacyc false entities:

    .. code-block:: javascript

        {'&amp;iota;', '&amp;lambda;', '&amp;gamma;', '&amp;omega;', '&amp;pi;',
        '&amp;prime;', '&amp;mu;', '&amp;plusmn;', '&amp;tau;', '&amp;chi;',
        '&amp;delta;', '&amp;harr;', '&amp;Delta;', '&amp;kappa;', '&amp;alpha;',
        '&amp;beta;', '&amp;epsilon;', '&amp;zeta;', '&amp;rarr;', '&amp;psi;',
        '&amp;mdash;', '&amp;nu;', '&amp;xi;'}


:Commands used on tgdb dump & sbml dump from Metacyc:

    - cat all_metabolites.tbl | egrep -o --color -e '&\w+;' >> html_entities.txt
    - cat metacyc_18.5.xml | egrep -o --color -e '&\w+;' >> metacyc_html_entities.txt
    - cat metacyc_18.5.xml | egrep -o --color -e '&\w+;\w+;' >> metacyc_html_false_entities.txt

Please take a look `at test_raw_files_and_functions()`

"""

# Standard imports
import html as h
import re

# Custom imports
from matching_kernel import commons as cm


CHEMICAL_HTML_ENTITIES = \
    {
        'pi': '(pros)', # /!\ 1 component with multiples () CPD-1823
        'tau': '(tele)',
        'harr': '<->',
        'rarr': '->',
        'plusmn': '+-', # - should be a demi-quadratin
        'amp': '&',
        'apos': "'",
        'quot': '"',
        'gt': '>',
        'lt': '<',
        'prime': "'",
        'mdash': '-', # Replace cadratin by quart-quadratin
        'ndash': '-' # Replace demi-quadratin by quart-quadratin
    }


def html_entities_to_names(text):
    """Convert html entities in a string to their ascii name.

    :Example:
        'a &beta;-D-galactosyl-(1,4)' => 'a beta-D-galactosyl-(1,4)'

    .. note:: In IUPAC, these prefixes are not supposed to be in upper case;
        So we put them in lower case before the convertion.

    .. note:: For some html entities, we have to correct their translation,
        according to chemical nomenclature.

        :Examples:
            - 'pi': '(pros)' at least 1 component with multiples (): CPD-1823
            - 'tau': '(tele)'
            - 'harr': '<->'
            - 'rarr': '->'
            - 'plusmn': '+-' - should be a demi-quadratin
            - 'amp': '&'
            - 'apos': "'"
            - 'quot': '"'
            - 'gt': '>'
            - 'lt': '<'
            - 'prime': "'"
            - 'mdash': '-'  Replace cadratin by quart-quadratin
            - 'ndash': '-'  Replace demi-quadratin by quart-quadratin
    """

    def utf8_replace(match):
        # Return pattern in lower case or correction for chemical nomenclature
        char = match.group(1).lower()
        return CHEMICAL_HTML_ENTITIES.get(char, char)

    reg_expr_unicode = re.compile('&(\w+);')
    return reg_expr_unicode.sub(utf8_replace, text)


def html_entities_to_utf8(text):
    """Convert html entities in a string to utf8 character.

    :Example:
        'a &beta;-D-galactosyl-(1,4)' => 'a β-D-galactosyl-(1,4)'

    .. note:: In IUPAC, these prefixes are not supposed to be in upper case;
        So we put them in lower case before the convertion.
    """

    def utf8_replace(match):
        return h.unescape(match.group(1).lower())

    reg_expr_unicode = re.compile('(&\w+;)')
    return reg_expr_unicode.sub(utf8_replace, text)


def test_raw_files_and_functions():
    """This function is used to display html entities in raw files.

    .. warning:: All these entities have to be processed before
        any import in database.

    .. note:: Shell commands used on tgdb dump & sbml dump
        from Metacyc, to generate problematic files:

        - cat all_metabolites.tbl | egrep -o --color -e '&\w+;' >> html_entities.txt
        - cat metacyc_18.5.xml | egrep -o --color -e '&\w+;' >> metacyc_html_entities.txt
        - cat metacyc_18.5.xml | egrep -o --color -e '&\w+;\w+;' >> metacyc_html_false_entities.txt
    """

    test_files = ('html_entities.txt',
                  'metacyc_html_entities.txt',
                  'metacyc_html_false_entities.txt')
    for file in test_files:
        with open(cm.DIR_DATA + file, 'r', encoding='utf-8') as f:
            s = {line.rstrip('\n') for line in f}

        print(file, ':\n', s)

    print(html_entities_to_utf8('&delta;&amp;beta;'))
    print(html_entities_to_names('coucou&amp;xxxxx&harr;'))
    print(html_entities_to_utf8(
        html_entities_to_names(
            'galactosaminyl-&amp;alpha;1,3-N,N&apos;-diacetylbacillosaminyl-&amp;alpha;1-di')
        )
    )


if __name__ == "__main__":

    test_raw_files_and_functions()



