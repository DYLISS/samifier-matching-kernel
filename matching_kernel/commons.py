# -*- coding: utf-8 -*-

# Standard imports
from logging.handlers import RotatingFileHandler
import logging
import itertools as it
from collections import defaultdict

# Custom imports
from matching_kernel import info
#from matching_kernel.utils import ColoursInTheShell as colors


# Paths
DIR_LOGS                = 'logs/'
DIR_DATA                = 'data/'
DIR_SERVER_USER_DATA    = '/var/www/html/Symfony/web/uploads/'


# TGDB export of Metacyc
TGDB_METABS_EXPORT      = 'all_metabolites.tbl'
TGDB_REACS_EXPORT       = 'all_reactions.tbl'

# Metanetx files
MNX_METABS              = 'chem_xref2.0.tsv'
MNX_REACTIONS           = 'reac_xref.tsv'
MNX_REACTIONS_DEF       = 'reac_prop.tsv'

# Logging
LOGGER_NAME             = info.PACKAGE_NAME
LOG_LEVEL               = logging.DEBUG

# PostgreSQL (native support of unicode instead of ascii)
POSTGRESQL_PATH         = 'postgresql+psycopg2://lex:test@/symfony?client_encoding=utf8'
POSTGRESQL_ADMIN_PATH   = 'postgresql+psycopg2://postgres:@/symfony?client_encoding=utf8'
# Which similarity function do you want to use ?
PG_SIMILARITY_FUNC      = ('jaro', 'jarowinkler', 'hamming_text')[0]
# If you add PG_SIMILARITY_FUNC, you have to update this constant too.
PG_SIMILARITY_FUNCS_OPS = {'jaro': '~%%',
                           'jarowinkler': '~@@',
                           'hamming_text': '~@~',
                          }
PG_SIMILARITY_THRESHOLD = 0.8

# MongoDB
MONGO_PATH              = 'localhost'
MONGO_PORT              = 27017

# SPARQL endpoint
#SPARQL_PATH             = "http://localhost:3030/test/query"
SPARQL_PATH             = "http://localhost:8890/sparql/"

# Which reference database would you use for exact/inferred name matching ?
REF_DB                  = ('MONGO', 'SPARQL')[0]

# Authorized database constants (don't forget to update constants below)
AUTH_STATUS             = ("already matched", "proposal", "to be matched", "cancelled", "validation")
AUTH_STEPS              = ("exact name", "word", "formula", "samir", "exact id")
AUTH_ORIGINS            = ("metacyc", "bigg", "kegg", "user dst", "user src",
                           "chemspider", "metanetx", "chebi", "inchikey", "smiles")
PREFIXES_ORIGINS        = {"metacyc": "http://biocyc.org/META/new-image?object=",
                           "bigg": "http://bigg.ucsd.edu/universal/metabolites/",
                           "kegg": "http://www.genome.jp/dbget-bin/www_bget?",
                           "chemspider": "",
                           "metanetx": "http://identifiers.org/metanetx.chemical/",
                           "chebi": "http://identifiers.org/chebi/CHEBI:",
                           "inchikey": "",
                           "smiles": "",
                          }


STA_AM, STA_PROP, STA_TBM, STA_CANC, STA_VA = AUTH_STATUS
STE_EX, STE_WO, STE_FO, STE_SAMIR, STE_EX_ID = AUTH_STEPS
O_MET, O_BIG, O_KEGG, O_USRDST, O_USRSRC, \
O_CHEMSPIDER, O_METNTX, O_CHEBI, O_INCHI, \
O_SMILES = AUTH_ORIGINS

################################################################################

def logger(name=LOGGER_NAME, logfilename=None):
    """Return logger of given name, without initialize it.

    Equivalent of logging.getLogger() call.
    """
    return logging.getLogger(name)

_logger = logging.getLogger(LOGGER_NAME)
_logger.setLevel(LOG_LEVEL)

# log file
formatter    = logging.Formatter(
    '%(asctime)s :: %(levelname)s :: %(message)s'
)
file_handler = RotatingFileHandler(
    DIR_LOGS + LOGGER_NAME + '.log',
    'a', 10000000, 1
)
file_handler.setLevel(LOG_LEVEL)
file_handler.setFormatter(formatter)
_logger.addHandler(file_handler)

# terminal log
stream_handler = logging.StreamHandler()
formatter      = logging.Formatter('%(levelname)s: %(message)s')
stream_handler.setFormatter(formatter)
stream_handler.setLevel(LOG_LEVEL)
_logger.addHandler(stream_handler)

def log_level(level):
    """Set terminal log level to given one"""
    handlers = (_ for _ in _logger.handlers
                if _.__class__ is logging.StreamHandler
               )
    for handler in handlers:
        handler.setLevel(level.upper())


# Some convenient functions
def chunk_this(iterable, length):
    """Split iterable in chunks of equal sizes"""
    iterator = iter(iterable)

    # For dictionnaries
    if (type(iterable) == type(dict())) or (type(iterable) == type(defaultdict())):
        # + 1: adjust length
        for i in range(0, len(iterable), length):
            yield {k:iterable[k] for k in it.islice(iterator, length + 1)}
    else:
        # For (all) other iterables (?)
        while True:
            # + 1: adjust length
            chunk = tuple(it.islice(iterator, length + 1))
            if not chunk:
               return
            yield chunk


def merge_dicts(*dict_args):
    """Given any number of dicts, shallow copy and merge into a new dict,
    precedence goes to key value pairs in latter dicts.
    """
    # Syntax for Python 3.5
    # result = {**dict1, **dict2}
    result = {}
    for dictionary in dict_args:
        result.update(dictionary)
    return result

