# -*- coding: utf-8 -*-
"""
Created on Mon Jan  6 00:58:31 2014

.. sectionauthor:: pierre.vignet@caramail.fr
"""

class ColoursInTheShell:
    HEADER    = '\033[95m'
    OKBLUE    = '\033[94m'
    OKGREEN   = '\033[92m'
    PINK      = '\033[35m'
    WARNING   = '\033[93m'
    FAIL      = '\033[91m'
    ENDC      = '\033[0m'
    BOLD      = '\033[1m'
    UNDERLINE = '\033[4m'
    BLINK     = '\033[5m'
    
