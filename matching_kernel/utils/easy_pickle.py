# -*- coding: utf-8 -*-
"""
Created on Sept 2015

.. sectionauthor::
"""

import pickle

def easy_pickle(variablePath, variableValue):
    """Dump a variable in pickle format according to the given path.
    
    """
    with open(variablePath + '.pkl', 'wb') as output:
        pickle.dump(variableValue, output)
    
def easy_unpickle(variablePath):
    """Load a variable in pickle format according to the given path.
    
    """
    variableValue = None
    with open(variablePath + '.pkl', 'rb') as pklFile:
        variableValue = pickle.load(pklFile)
    return variableValue
