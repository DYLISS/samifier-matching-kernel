# -*- coding: utf-8 -*-

# Custom imports
from matching_kernel.reference_database.import_metacyc import load_tgdb
from matching_kernel.reference_database.import_metanetx import load_metanetx
from matching_kernel.reference_database.mongo_wrapper import BiologicalObject


def main():
    """Import TGDB export then Metanetx to MongoDB,
    and fill text_matching tables of PostgreSQL.

    .. warning:: Please respect the call order of functions !
        Metanetx import updates the "bone" made during the Metacyc import.

    """

    # TGDB => Mongo
    load_tgdb()

    # Metanetx => Mongo
    load_metanetx()

    # Ensure the reconstruction of indexes
    BiologicalObject.ensure_indexes()
