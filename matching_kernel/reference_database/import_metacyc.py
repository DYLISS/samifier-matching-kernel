# -*- coding: utf-8 -*-
"""This module is used to load Metacyc data to reference database.

Reference database could be MongoDB or a SPARQL endpoint.

"""

# Standard imports
import csv
from collections import defaultdict
import itertools as it
from sqlalchemy import text

try:
    from rdflib import Graph, Literal, BNode, Namespace, RDF, RDFS, term
except ImportError:
    raise ImportError("rdflib seems not to be installed. \
                      Please install the module with the following command:\n \
                      sudo pip install rdflib \n \
                      or \
                      pip install --user rdflib")

# Custom imports
from matching_kernel import commons as cm
from matching_kernel.unifier_database.sqla_wrapper import simple_engine
from matching_kernel.reference_database.mongo_wrapper import \
    Mongo_Wrapper, BiologicalObject, Metabolite, Reaction, \
    search_document_with_database_id
from matching_kernel.convert import \
    html_entities_to_names, html_entities_to_utf8

LOGGER = cm.logger()


def load_tgdb_export_from_metacyc(filepath):
    """Load Metacyc content taken from TGDB export...

    With the last file 'all_metabolites.tbl', we have 12490 metabolites
    and 3647 metabolites without xrefs.

    We are waiting a 'csv' file... With these headers:
        - METACYC
        - NAME
        - SYNONYMS
        - XREF (ex : "BDD1:id1;BDD2:id2")

    PS: Inside these headers (NAME, SYNONYMS, XREF) delimiter is ';'

    .. warning: LIGAND-CPD & KEGG-GLYCAN refs were renamed manually to KEGG !
        BIGG refs were renamed to BIGG1

    :return: Generator of tuples of names and cross references.
    :rtype: <generator <tuple <set>, <defaultdict(list)>>>

    """

    with open(filepath, 'r', encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')

        nb_entities_without_xref = 0

        for row in reader:

#            print(row)
            names = set()
            xrefs = defaultdict(list)

            if row['SYNONYMS'] != '':
                synonyms = html_entities_to_utf8(row['SYNONYMS']) + ';' + \
                           html_entities_to_names(row['SYNONYMS'])
#                print(synonyms)
                names.update(synonyms.split(';'))

            if row['XREF'] != '':
                try:
                    g = (xref.split(':') for xref in row['XREF'].split(';'))
                    #xrefs = {bdd.upper(): id for bdd, id in g}
                    [xrefs[bdd.upper()].append(id) for bdd, id in g]
                except ValueError:
                    LOGGER.error("Error during file parsing. "
                                 "Please verify delimiters & separators...")
                    LOGGER.error("MetaCyc id: " + str(row['METACYC']))
                    LOGGER.error("xrefs state: " + str(xrefs))
                    raise

            # Remove entities without xrefs => NO !
            if len(xrefs) == 0:
                nb_entities_without_xref += 1
#                continue

            xrefs['METACYC'] = [row['METACYC']]

#            print("NAMES", names)
#            print("XREFS", xrefs)
#            print("Entities without xrefs:", nb_entities_without_xref)

            yield names, xrefs


def load_reactions_tgdb_export_from_metacyc(filepath):
    """Load Metacyc content taken from TGDB export...

    With the last file 'all_metabolites.tbl', we have 12490 metabolites
    and 3647 metabolites without xrefs.

    We are waiting a 'csv' file... With these headers:
        - METACYC
        - NAME
        - SYNONYMS
        - XREF (ex : "BDD1:id1;BDD2:id2")

    PS: Inside these headers (NAME, SYNONYMS, XREF) delimiter is ';'

    .. warning: LIGAND-CPD & KEGG-GLYCAN refs were renamed manually to KEGG !
        BIGG refs were renamed to BIGG1

    :return: Generator of tuples of names and cross references.
    :rtype: <generator <tuple <set>, <defaultdict(list)>>>

    """

    with open(filepath, 'r', encoding='utf-8') as csvfile:
        reader = csv.DictReader(csvfile, delimiter='\t')

        nb_entities_without_xref = 0

        for row in reader:

#            print(row)
            names    = set()
            reagents = set()
            products = set()
            xrefs    = defaultdict(list)

            if row['SYNONYMS'] != '':
                synonyms = html_entities_to_utf8(row['SYNONYMS']) + ';' + \
                           html_entities_to_names(row['SYNONYMS'])
#                print(synonyms)
                names.update(synonyms.split(';'))

            if row['XREF'] != '':
                try:
                    g = (xref.split(':') for xref in row['XREF'].split(';'))
                    #xrefs = {bdd.upper(): id for bdd, id in g}
                    [xrefs[bdd.upper()].append(id) for bdd, id in g]
                except ValueError:
                    LOGGER.error("Error during file parsing. "
                                 "Please verify delimiters & separators...")
                    LOGGER.error("MetaCyc id: " + str(row['METACYC']))
                    LOGGER.error("xrefs state: " + str(xrefs))
                    raise

            if row['REAGS'] != '':
                [reagents.add(id) for id in row['REAGS'].split(';')]

            if row['PRODS'] != '':
                [products.add(id) for id in row['PRODS'].split(';')]

#            if row['REV'] != '':
#                pass

            # Remove entities without xrefs => NO !
            if len(xrefs) == 0:
                nb_entities_without_xref += 1
#                continue

            xrefs['METACYC'] = [row['METACYC']]

#            print("NAMES", names)
#            print("XREFS", xrefs)
#            print("REAGS", reagents)
#            print("PRODS", products)

#        print("Entities without xrefs:", nb_entities_without_xref)

            yield names, xrefs, reagents, products, row.get('REV', False)


def import_metacyc_to_mongo(biobjects):
    """deprecated, to mongodb"""

    i = 0
    for names, xrefs in biobjects:

        # Itération sur les clés des xrefs
        for db_name, db_ids in xrefs.items():

            db_objs = list()
            # Transformation de la liste d'identifiants en liste de références
            # vers objets IdentifierRepo
            for db_id in db_ids:
                try:
                    db_objs.append(IdentifierRepo(database=db_name, name=db_id).save())
                except NotUniqueError:
                    print('Already exists DB:', db_name, 'ID:', db_id)

                    try:
                        db_objs.append(IdentifierRepo.objects.get(database=db_name, name=db_id))
                    except Exception as e:
                        print(e)
                        input('Fatal error')
            xrefs[db_name] = db_objs
        print(xrefs)
        input('pause')

        Metabolite(names=names, databases=xrefs).save()
        i += 1
    print(i)


def import_metacyc_to_mongo2(biobjects):
    """Import metabolites to MongoDB.

    .. warning:: This func drops the collection of MongoDB before insertion.

    :Example of database attribute:

    .. code-block:: javascript
        :linenos:

        defaultdict(<class 'list'>,
            {'INCHIKEY': ['SKVVAASQKFJYKN-UHFFFAOYSA-L'],
             'PUBCHEM': ['25203614'],
             'LIGAND-CPD': ['C04503'],
             'SMILES': ['C(COP(=O)([O-])OC)NC(=[N+])NP(=O)([O-])[O-]'],
             'METACYC': ['N-PHOSPHOGUANIDINOETHYL-METHYL-PHOSPHAT'],
             'CHEBI': ['58599']
            }
        )

    :param arg1: Generator of tuples of names and cross references.
    :type arg1: <generator <tuple <set>, <defaultdict(list)>>>

    """

    with Mongo_Wrapper(alias='default') as (alias, _):

        # Import and count all imported metabolites
        for i, (names, xrefs) in enumerate(biobjects):

            Metabolite(names=names, databases=xrefs).save()

    LOGGER.info("Metacyc to MongoDB: " + str(i) + " metabolites imported.")


def import_metacyc_reactions_to_mongo2(biobjects):
    """
    """

    with Mongo_Wrapper(alias='default') as (alias, _):

        # Import and count all imported metabolites
        for i, (names, xrefs, reagents_ids, products_ids, reversible) in enumerate(biobjects):

            reagents = search_document_with_database_id(reagents_ids,
                                                        'metacyc',
                                                        bio_object_type=Metabolite)
            products = search_document_with_database_id(products_ids,
                                                        'metacyc',
                                                        bio_object_type=Metabolite)

            if len(reagents_ids) != len(reagents):
                LOGGER.error("Inconsistencies in reagents: " +\
                             str(names) + str(xrefs))
                LOGGER.error("Expected ids: " + str(reagents_ids))
                LOGGER.error("Present ids: " + \
                             str([metab.databases['METACYC'] for metab in reagents]))

            if len(products_ids) != len(products):
                LOGGER.error("Inconsistencies in products: " +\
                             str(names) + str(xrefs))
                LOGGER.error("Expected ids: " + str(products_ids))
                LOGGER.error("Present ids: " + \
                             str([metab.databases['METACYC'] for metab in products]))

            Reaction(names=names,
                     databases=xrefs,
                     reagents=reagents,
                     products=products,
                     reversible=reversible).save()

    LOGGER.info("Metacyc to MongoDB: " + str(i) + " reactions imported.")


def to_rdf_graph(biobjects):
    """

    """


    IDENTIFIER = Namespace("http://identifiers.org/")
    BIOVOC     = Namespace('http://identifiers.org/biomodels.vocabulary/')
    BQBIOL     = Namespace('http://biomodels.net/biology-qualifiers/')
    bqbiol_is  = term.URIRef('http://biomodels.net/biology-qualifiers/is')

    g = Graph()
    g.bind("rdf", RDF)
    g.bind("bqbiol", BQBIOL)
    g.bind("identifier", IDENTIFIER)

    db_xrefs = dict()


    for names, xrefs in biobjects:
        # Initialize definition of the entity
        entity = BNode()
        g.add( (entity, RDF.type, BIOVOC.Species) )

#        print(names, xrefs)
        for db_name, db_ids in xrefs.items():

            # 1 blank node per database
            # Each blanck node is stored & reused

            try:
                prefix = db_xrefs[db_name]
            except KeyError:
                db_xrefs[db_name] = prefix = \
                    term.URIRef("http://identifiers.org/" + db_name.strip())

                db_entity = BNode()


#            db_entity, prefix = \
#                db_xrefs.get(
#                    db_name,
#                    (BNode(),
#                     term.URIRef("http://identifiers.org/" + db_name.strip())
#                    )
#                )

            # Initialize database ids
            for db_id in db_ids:

                tpl = (db_entity, prefix, Literal(db_id))

                if tpl not in g:
                    g.add(tpl)
                else:
#                    print(tpl)
#                    input("already")
                    pass

                # Attach database id
                g.add( (entity, bqbiol_is, db_entity) )

        # Attach names
        for name in names:
            g.add( (entity, RDFS.label, Literal(name)) )

        print(g.serialize(format='turtle').decode('utf8'))
        exit()

    with open('/home/Lex/Desktop/test2.ttl', 'wb') as file:
        file.write(g.serialize(format='turtle'))
    # print(g.serialize(format='turtle').decode('utf8'))


def mok_graph_test():

    g = Graph()

    # Namespace
    IDENTIFIER = Namespace("http://identifiers.org/")
    BQBIOL     = Namespace('http://biomodels.net/biology-qualifiers/')
    BIOVOC     = Namespace('http://identifiers.org/biomodels.vocabulary/')
    bqbiol_is = term.URIRef('http://biomodels.net/biology-qualifiers/is')
    g.bind("rdf", RDF)
    g.bind("bqbiol", BQBIOL)
    g.bind("biovoc", BIOVOC)
    g.bind("identifier", IDENTIFIER)

    # Create an identifier to use as the subject for Donna.
    entity = BNode()
    db1_1 = BNode()
    db2_1 = BNode()
    db2_2 = BNode()
    db3_3 = BNode()

    # Add triples using store's add method.
    g.add( (entity, RDF.type, BIOVOC.Species) )
    g.add( (entity, RDFS.label, Literal('a')) )
    g.add( (entity, RDFS.label, Literal('b')) )

    g.add( (entity, bqbiol_is, db1_1) )
    g.add( (entity, bqbiol_is, db2_1) )
    g.add( (entity, bqbiol_is, db2_2) )
    g.add( (entity, bqbiol_is, db3_3) )

    # Identifiers & databases
    g.add( (db1_1, IDENTIFIER.db1, Literal('1')) )
    g.add( (db2_1, IDENTIFIER.db2, Literal('1')) )
    g.add( (db2_2, IDENTIFIER.db2, Literal('2')) )
    g.add( (db3_3, IDENTIFIER.db3, Literal('3')) )

    print(g.serialize(format='turtle').decode('utf8'))

    # Test identifier
    print((db1_1, RDF.resource, IDENTIFIER.db1) in g)


def fill_text_matching_table(biobjects, bio_obj_type):
    """Initialize the PostgreSQL table for name matching.

    .. warning:: This function drops text_matching tables !

    :param arg1: Generator of tuples of names and cross references.
    :param arg2:
    :type arg1: <generator <tuple <set>, <defaultdict(list)>>>
    :type arg2:

    """

    # 2 authorized types
    assert (bio_obj_type in ('metabolites', 'reactions'))

    # Get admin connection
    raw_connection = simple_engine(admin=True)

    # Partial unpacking *_
    unique_names = set(it.chain(*(names for names, *_ in biobjects)))

    # Activate pg_trgm extension
    #raw_connection.execute("CREATE SCHEMA IF NOT EXISTS extensions")
    raw_connection.execute("CREATE EXTENSION IF NOT EXISTS pg_trgm WITH SCHEMA public")
    # WITH SCHEMA extensions"

    # Drop index before insertions
    raw_connection.execute(
        "DROP INDEX IF EXISTS {}_idx".format(bio_obj_type)
    )

    # Drop the table
    raw_connection.execute(
        "DROP TABLE IF EXISTS {}_matching CASCADE".format(bio_obj_type)
    )

    # Use basic user connection
    # We do this because if we keep the admin connection,
    # user will not be able to use the following tables.
    raw_connection = simple_engine()

    # Create the table if not exists
    raw_connection.execute(
        "CREATE TABLE IF NOT EXISTS {}_matching ( \
        id      SERIAL  PRIMARY KEY,\
        name    TEXT    NOT NULL    UNIQUE,\
        tsv     TSVECTOR)".format(bio_obj_type)
    )

    # Delete and recreate trigger
    raw_connection.execute(
        "DROP TRIGGER IF EXISTS tsvector_update ON {}_matching".format(bio_obj_type)
    )
    raw_connection.execute(
        "CREATE TRIGGER tsvector_update BEFORE INSERT OR UPDATE \
        ON {}_matching FOR EACH ROW EXECUTE PROCEDURE \
        tsvector_update_trigger(tsv, 'pg_catalog.simple', name)".format(bio_obj_type)
    )

#    statement = "INSERT INTO metabolites_matching (name) VALUES (:name)"
#    for name in unique_names:
#        try:
#            raw_connection.execute(text(statement), name=name)
#        except IntegrityError:
#            pass

    statement = \
        "INSERT INTO {}_matching (name) VALUES (:name)".format(bio_obj_type)

    # Run a transaction
    # => much more efficient but needs a rollback in case of IntegrityError...
    # Transac is auto-commited by context manager
    with raw_connection.begin() as transaction:

        [raw_connection.execute(text(statement), name=name)
            for name in unique_names]

        # Manual commit:
        # transaction.commit()

    # Create trigram GIN (Generalized Inverted Index) index
    raw_connection.execute(
        "CREATE INDEX {0}_idx ON {0}_matching \
        USING gin(name gin_trgm_ops)".format(bio_obj_type)
    )

    LOGGER.info("Matching table {}: {} unique names."
                .format(bio_obj_type,
                        len(unique_names)))


def load_tgdb():
    """TGDB dump => Mongo.

    Loads metabolites & reactions.
    Makes inverted index in PostgreSQL.

    """

    # Drop database
    with Mongo_Wrapper(alias='default') as (alias, _):
        BiologicalObject.drop_collection()

    # Metabolites
    import_metacyc_to_mongo2(load_tgdb_export_from_metacyc(
        cm.DIR_DATA + cm.TGDB_METABS_EXPORT)
    )

    # Reactions
    import_metacyc_reactions_to_mongo2(load_reactions_tgdb_export_from_metacyc(
        cm.DIR_DATA + cm.TGDB_REACS_EXPORT)
    )

    load_postgres_indexes()


def load_postgres_indexes():
    """Load only indexes for names of reactions & metabolites"""

    # Metabolites - PostgreSQL "Inverted index"
    fill_text_matching_table(load_tgdb_export_from_metacyc(
        cm.DIR_DATA + cm.TGDB_METABS_EXPORT),
        "metabolites"
    )

    # Reactions - PostgreSQL "Inverted index"
    fill_text_matching_table(load_reactions_tgdb_export_from_metacyc(
        cm.DIR_DATA + cm.TGDB_REACS_EXPORT),
        "reactions"
    )


if __name__ == "__main__":

    import_metacyc_reactions_to_mongo2(load_reactions_tgdb_export_from_metacyc(cm.DIR_DATA + 'all_reactions.tbl'))

    exit()
    import_metacyc_to_mongo2(load_tgdb_export_from_metacyc(cm.DIR_DATA + cm.TGDB_EXPORT))
    fill_text_matching_table(load_tgdb_export_from_metacyc(cm.DIR_DATA + cm.TGDB_EXPORT))

    exit()

    import time
    start = time.time()
    to_rdf_graph(load_tgdb_export_from_metacyc(cm.DIR_DATA + 'all_metabolites.tbl'))
    print(time.time() - start)
    input('pause')

    mok_graph_test()
