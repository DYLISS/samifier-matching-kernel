# -*- coding: utf-8 -*-
"""Module used to query SPARQL endpoint.

"""

# Standard imports
from collections import defaultdict

try:
    from SPARQLWrapper import SPARQLWrapper, JSON
except ImportError:
    raise ImportError("SPARQLWrapper seems not to be installed. \
          Please install the module with the following command:\n \
          sudo pip install SPARQLWrapper \n \
          or \
          pip install --user SPARQLWrapper")

# Custom imports
from matching_kernel import commons as cm

LOGGER = cm.logger()


def load_sparql_endpoint():
    """Make a connection to SPARQL endpoint & retrieve a cursor.

    :return: sparql cursor in version 2!
             => this cursor is made for servers that return JSON by default !
    :rtype: <SPARQLWrapper2>

    """

    return SPARQLWrapper(cm.SPARQL_PATH, 'POST') # CHECK THIS


def sparql_query(query):
    """Wait for a valid database URI, and a SPARQL query.
    Yields all triplets returned by the query.
    The query need to yield three values, named object, relation and subject.

    .. warning:: SPARQLWrapper2: http://rdflib.github.io/sparqlwrapper/doc/latest/
              with SPARQLWrapper2, server must return JSON data !
              if not, please use SPARQLWrapper:
              sparql.setReturnFormat(SPARQLWrapper.JSON)
              followed by: sparql.queryAndConvert()
    """

    sparql = load_sparql_endpoint()

    # data in JSON format => proper python dict()
    sparql.setQuery(query)
    sparql.setReturnFormat(JSON)

    try:
        # PS: if XML stream is not used: don't use sparql.query(),
        #but sparql.queryAndConvert() instead.
        results = sparql.queryAndConvert()

        # Dictionary of dictionnaries in result
        # ex:
        # {
        #  "head": {
        #    "vars": [ "METACYC" , "name" ]
        #  } ,
        #  "results": {
        #    "bindings": [
        #      {
        #        "METACYC": { "type": "literal" , "value": "PROPANOL" }
        #      }
        #    ]
        #  }
        # }
#        print(results)
        LOGGER.debug("SPARQL: Number of results: " + \
                     str(len(results['results']['bindings'])))

    except Exception as e:
        LOGGER.error("SPARQL query error" + str(e))
        raise

    for binding in results['results']['bindings']:

        yield tuple(binding[var]['value']
                    for var in results['head']['vars'])


def search_ids_and_syns_with_names(names, bdd_queried):
    """
ret ('MALONYL-COA',) d'où le chainage à la fin

    :Example:

    .. code-block:: javascript
        :linenos:

        (
            defaultdict(<class 'set'>,
                {'D-fructose 6-phosphate': {'CPD-15709', 'FRUCTOSE-6P'}}
            ),
            defaultdict(<class 'set'>,
                {'CPD-15709': {'D-fructose 6-phosphate'},
                 'FRUCTOSE-6P': {'beta-D-fructofuranose 6-phosphate', 'fructose-6P'}
                }
            )
        )

    """

    secured_names = '" "'.join([name.replace('"', '\"') for name in names])

    query = """
        PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
        PREFIX bbq: <http://biomodels.net/biology-qualifiers/>
        PREFIX identifier: <http://identifiers.org/>

        SELECT DISTINCT ?given_names ?synonyms ?bdd
        WHERE {
            ?id bbq:is ?object .
            ?object identifier:""" + bdd_queried.upper() + """ ?bdd .
            ?id rdfs:label ?synonyms .
            ?id rdfs:label ?given_names .
            VALUES ?given_names { \"""" + secured_names + """\" }
        }
        """

    given_name_identifiers = defaultdict(set)
    identifier_synonyms = defaultdict(set)
    for given_name, synonym, identifier in sparql_query(query):
        given_name_identifiers[given_name].add(identifier)
        identifier_synonyms[identifier].add(synonym)


    return given_name_identifiers, identifier_synonyms

if __name__ == "__main__":
    print(search_ids_and_syns_with_names(["1-propanol"], 'METACYC'))
    print(search_ids_and_syns_with_names(["D-fructose 6-phosphate"], 'METACYC'))
