# -*- coding: utf-8 -*-

# Custom imports
from matching_kernel import commons as cm

# Go with SPARQL endpoint
if cm.REF_DB == "SPARQL":
    from matching_kernel.reference_database.sparql_wrapper import \
        search_ids_and_syns_with_names
else:
# Go with Mongo database
    from matching_kernel.reference_database.mongo_wrapper import \
        search_ids_and_syns_with_names, \
        search_document_with_database_id, \
        search_ids_with_ids, \
        Mongo_Wrapper, \
        Metabolite, \
        Reaction
    from matching_kernel.reference_database.main_importer import main
