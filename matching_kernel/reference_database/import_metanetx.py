# -*- coding: utf-8 -*-
"""This module is used to load Metanetx data to reference databases such like
MongoDB.

"""

# Standard imports
import csv
import re
from collections import defaultdict
from multiprocessing import Pool, cpu_count
from functools import partial
import itertools as it
from mongoengine.context_managers import switch_db

# Custom imports
from matching_kernel import commons as cm
from . import mongo_wrapper as rdb # Fix circular import in python 3.4
from matching_kernel.reference_database.mongo_wrapper \
    import Metabolite, Reaction

LOGGER = cm.logger()


def load_metanetx_file(filepath):
    """Load a file downloaded from Metanetx.

    .. warning:: This functions filters all databases that are not in this list:
        ('metacyc', 'kegg', 'chebi', 'bigg')

    .. code-block:: javascript

        'MNXM150002': defaultdict(<class 'set'>, {
            'CHEBI': {'74922', '75112'}
        }),
        'MNXM1679': defaultdict(<class 'set'>, {
            'METACYC': {'CPD-3462'},
            'CHEBI': {'58535', '21900', '7420', '27911'},
            'BIGG': {'n8aspmd'},
            'KEGG': {'C01029'}
        })

    :param arg1: Path of the file.
    :type arg1: <str>
    :return: A defaultdict which has Metanetx identifiers as keys;
        Have a look at the example above.
    :rtype: <defaultdict <set>>

    """

    # Keys in this dict are metanetx identifiers
    d_dict     = lambda : defaultdict(set)
    metanetx   = defaultdict(d_dict)
    reg_bdd_id = re.compile('(?P<bdd>\w*):(?P<id>.*)')

    # Filter "useless databases"
    authorized_databases = ('metacyc', 'kegg', 'chebi', 'bigg')

    # Open tsv file downloaded from Metanetx
    with open(filepath, 'r', encoding='utf-8') as csvfile:
        # Remove headers
        filterer = lambda s: s.startswith('#')
        reader = csv.reader(it.dropwhile(filterer, csvfile), delimiter='\t')

        for row in reader:
            match = reg_bdd_id.match(row[0])

            if match.group('bdd') not in authorized_databases:
                continue

            metanetx[row[1]][match.group('bdd').upper()].add(match.group('id'))

            # Debug
            # print(match.groups())
            # print(row)
            # input('pause')

    LOGGER.info("Metanetx to MongoDB: Read {} elements from {}.".format(
        len(metanetx),
        filepath)
    )

    return metanetx


def load_metanetx_reacs_definitions_file(filepath):
    """Load the description of reactions file downloaded from Metanetx.

    .. warning:: This functions filters all databases that are not in this list:
        ('metacyc', 'kegg', 'bigg')

    :Example:

        .. code-block:: javascript

            'MNXR46': {
                'PRODS': {'MNXM2', 'MNXM5', 'MNXM9599'},
                'REAGS': {'MNXM1', 'MNXM4', 'MNXM6', 'MNXM812'}
            })

    :param arg1: Path of the file.
    :type arg1: <str>
    :return: A dict which has Metanetx identifiers as keys;
        Have a look at the example above.
    :rtype: <dict <dict <str>, <str>>>

    """

    # Keys in this dict are metanetx identifiers
    metanetx_reactions = dict()
    reg_bdd_id = re.compile('(?P<bdd>\w*):(?P<id>.*)')


    # Filter "useless databases"
    # Not Chebi (no reactions)
#    authorized_databases = ('metacyc', 'kegg', 'bigg')

    def parse_equation(eq):
        """Return 1 dictionary of 2 lists (reagents, products)"""
        d_reactants = dict()

        new_eq = re.sub('^(\d* )|( \d* )|( *)', '', eq)
        # print(new_eq)
        reactants = new_eq.split('=')

        d_reactants['REAGS'] = \
            [reagent for reagent in reactants[0].split('+')]
        d_reactants['PRODS'] = \
            [product for product in reactants[1].split('+')]

        return d_reactants


    # Open tsv file downloaded from Metanetx
    with open(filepath, 'r', encoding='utf-8') as csvfile:
        # Remove headers
        filterer = lambda s: s.startswith('#')
        reader = csv.DictReader(it.dropwhile(filterer, csvfile),
                                fieldnames=['MNX', 'EQ', 'DESCRIPT', 'BAL', 'EC', 'SRC'],
                                delimiter='\t')

        for row in reader:

            # Test the source of the equation
            # print(row['SRC'])
            # TODO: Don't know why i remove database filtering here ? :-(

#            match = reg_bdd_id.match(row['SRC'])
#
#            try:
#                if match.group('bdd') not in authorized_databases:
#                    continue
#            except:
#                # Source is not in regex...
#                continue

            # Get products & reagents ids (metanetx ids)
            metanetx_reactions[row['MNX']] = parse_equation(row['EQ'])

    LOGGER.info("Metanetx to MongoDB: Read {} elements from {}.".format(
        len(metanetx_reactions),
        filepath)
    )

    return metanetx_reactions


def save_bigg_metacyc_mapping(metanetx):
    """Take data from load_metanetx_file() and saves
    equivalent identifiers from Metacyc & BiGG to a file.

    :param arg1: A defaultdict which has Metanetx identifiers as keys;
        Have a look at the example above.
    :type arg1: <defaultdict <set>>
    """

    with open(cm.DIR_DATA + 'metanetx.txt', 'w') as file:

        for met_id in metanetx:
            bigg = metanetx[met_id].get('BIGG', '')
#            kegg = metanetx[met_id].get('KEGG', '')
            metacyc = metanetx[met_id].get('METACYC', '')

            if bigg != '' and metacyc != '':

                file.write(met_id + '\n' + \
                           '\tBIGG:' + ','.join(bigg) + '\n'\
                           '\tMETACYC:' + ','.join(metacyc) + '\n')


def multiprocess_this(metanetx_data, biological_object_type):
    """This function is used during multiprocessing phase to fill MongoDB
    with Metanetx data.

    This function is called AFTER TGDB import.

    Metacyc identifiers are searched in DB; If they are finded, the documeent
    will be updated. If not, the document will be not updated (we don't create
    elements that are not involved in reactions. The assertion that documents
    in place reference elements involved in reactions only come from TGDB export).
    If the Metanetx "node" does not contain Metacyc identifier, a new document
    is created (without names).

    .. note:: If indexes are disabled, the insertion process is much more
        efficient.

    :param arg1: A defaultdict which has Metanetx identifiers as keys;
        Have a look at the example above.
    :param arg2: Mongo BiologicalObject type (Metabolite or Reaction).
    :type arg1: <defaultdict <set>>
    :type arg2: <Metabolite> or <Reaction>
    :return: Tuple of the number of documents updated/created.
        + A dict of created documents. This dict will be used in order to
        update new reactions with reagents & products (see update_reactions())
        keys: mnx_id; values: Mongo reaction
    :rtype: <tuple <int>, <int>, <dict <str>: <Mongo Reaction>>>

    """

    # list of created objects (it will be returned by this func)
    metanetx_created_elements = dict()

#    with rdb.Mongo_Wrapper() as (alias, _):
    with rdb.Mongo_Wrapper(alias='default') as (alias, _):

        LOGGER.debug("Mongo multiprocessing: We are in connection " + alias)

        with switch_db(biological_object_type, alias) as biological_object_type:
            nb_updated = 0
            nb_created = 0

            for mnx_id, xrefs in metanetx_data.items():


                # Get all documents referenced for this Metanetx identifier
                # (which have this METACYC cross reference)
                if 'METACYC' in xrefs:
                    docs = \
                        rdb.search_document_with_database_id(
                            xrefs['METACYC'],
                            'METACYC',
                            bio_object_type=biological_object_type
                        )

                    # Multiple docs retrieved for this mnx_id
                    # if len(docs) > 1:
                    #     for doc in docs:
                    #     print(doc.names, doc.databases)
                    #     input("pause")

                    # Update databases with Metanetx data for each document retrieved
                    for doc in docs:
                        # print("Names", doc.names)
                        # print("Before", doc.databases)

                        # Copy is important here !
                        # You don't want the current document be updated
                        # by other Metacyc identifiers !
                        # => Metanetx nodes are there lax about inferred ids...
                        # => They group together too many elements !
                        # Se below that we don't update Metacyc ids
                        xrefs_copy = xrefs.copy()

                        [xrefs_copy[database].update(ids)
                            for database, ids in doc.databases.items()]

                        # Add the Metanetx id
                        xrefs_copy['METANETX'] = [mnx_id]
                        # We refuse the update of Metacyc ids.
                        # 1 doc = 1 metacyc id
                        xrefs_copy['METACYC'] = doc.databases['METACYC']

                        # Save the doc
                        doc.databases = xrefs_copy
                        doc.save()

                        # print("After", doc.databases)
                        nb_updated += 1

                    # If Metacyc id was not found in database,
                    # we add the new element (Becareful: There are no names here)
                    # EDIT: Metacyc ids that are already in database are
                    # involved in reactions.
                    # Here you can add ids that are not involved in reactions.
                    # You can flood your database with this kind of elements...
                    # if docs is None:
                    #     biological_object_type(databases=xrefs).save()
                    #     nb_created += 1

                else:
                    # The element has no references for Metacyc according to Metanetx
                    xrefs['METANETX'] = [mnx_id]
                    new_obj = biological_object_type(databases=xrefs)
                    # Save the object in db
                    new_obj.save()
                    # Append to the list of created objects
                    metanetx_created_elements[mnx_id] = new_obj
                    nb_created += 1

    return nb_updated, nb_created, metanetx_created_elements


def import_metanetx_to_mongo(metanetx, biological_object_type):
    """Entry point that takes data from load_metanetx_file() and saves it to
    MongoDB.

    Match is made on Metacyc identifiers => Have a look at multiprocess_this().

    :param arg1: A defaultdict which has Metanetx identifiers as keys;
        Have a look at the example above.
    :param arg2: BiologicalObject type (Metabolite or Reaction).
    :type arg1: <defaultdict <set>>
    :type arg2: <Metabolite> or <Reaction>

    """

    # We split the queries by the number of physical CPU cores.
    nb_process = cpu_count() # // 2

    # Multiprocessing => split database queries
    # This step of multiprocessing is very efficient against a classical RDBMS
    p = Pool(processes=nb_process)

    results = p.imap(partial(multiprocess_this,
                             biological_object_type=biological_object_type),
                     cm.chunk_this(metanetx, len(metanetx) // nb_process))

    # Merge tuple of results
    unzip = lambda l: list(zip(*l))

    r = tuple([(r1, r2, r3) for r1, r2, r3 in results])
    results = unzip(r)

    # Merge dictionaries of new created elements
    metanetx_created_elements = cm.merge_dicts(*results[2])

    # print(len(metanetx_created_elements))

    LOGGER.info("Metanetx to MongoDB: {} {} updated.".format(
        sum(results[0]),
        biological_object_type.__name__)
    )
    LOGGER.info("Metanetx to MongoDB: {} {} created.".format(
        sum(results[1]),
        biological_object_type.__name__)
    )

    return metanetx_created_elements


def update_reactions(metanetx_created_elements, metanetx_reactions):
    """
    """

    with rdb.Mongo_Wrapper(alias='default') as (alias, _):

        # Import and count all imported metabolites
        for i, (mnx_id, reac) in enumerate(metanetx_created_elements.items()):

            reagents_ids, products_ids = \
                metanetx_reactions[mnx_id].values()

            reagents = \
                rdb.search_document_with_database_id(reagents_ids,
                                                     'METANETX',
                                                     bio_object_type=Metabolite)
            products = \
                rdb.search_document_with_database_id(products_ids,
                                                     'METANETX',
                                                     bio_object_type=Metabolite)

            if len(reagents_ids) != len(reagents):
                LOGGER.error("Inconsistencies in reagents:")
                LOGGER.error("Expected ids: " + str(reagents_ids))
                LOGGER.error("Present ids: " + \
                             str([metab.databases['METANETX'] for metab in reagents]))

            if len(products_ids) != len(products):
                LOGGER.error("Inconsistencies in products: ")
                LOGGER.error("Expected ids: " + str(products_ids))
                LOGGER.error("Present ids: " + \
                             str([metab.databases['METANETX'] for metab in products]))

            # Update the database
            reac.reagents = reagents
            reac.products = products

    LOGGER.info("Metacyc to MongoDB: " + str(i) + " reactions updated.")


def detect_problematic_identifiers(identifier):
    """
    ex: metacyc:A-DARA-HEX-2:5_2:KETO
    """
    from collections import Counter

    if (Counter(identifier)[':'] != 1):
        print("value error " + identifier)

def load_metanetx():
    """
    """

    # Load & create mnx metabolites cross refs
    import_metanetx_to_mongo(
        load_metanetx_file(cm.DIR_DATA + cm.MNX_METABS),
        Metabolite
    )

    # Load & create mnx reactions cross refs
    metanetx_created_elements = import_metanetx_to_mongo(
        load_metanetx_file(cm.DIR_DATA + cm.MNX_REACTIONS),
        Reaction
    )

    # Load definitions of mnx reactions
#    metanetx_reactions = load_metanetx_reacs_definitions_file(
#        cm.DIR_DATA + cm.MNX_REACTIONS_DEF
#    )

    # Create mnx reactions with full equations
    # Pb: metanetx can't be used as a reference for equations of reactions...
    # False genericity (1 mnx id for many close but different entities)
    # cf => H2SO3 = HS03- = SO32- (sic ...)
    # update_reactions(metanetx_created_elements, metanetx_reactions)


if __name__ == "__main__":

    load_metanetx_reacs_definitions_file(cm.DIR_DATA + cm.MNX_REACTIONS_DEF)
    exit()

    # Metabolites:
    # Updated: 11566.00
    # Created: 50408.00
    # Not modified (not in Metanetx): 925
    # Final count: 62899
    # Metacyc: 12490
    # Metanetx: 65361

    # Reactions:
    # Updated: 13050
    # Created: 10305
    # Final count (not in Metanetx): 24
    # Metanetx: 23618
    # Metacyc: 13074

    # db.getCollection('biological_object').find({_cls: "BiologicalObject.Reaction"})

    load_metanetx()


