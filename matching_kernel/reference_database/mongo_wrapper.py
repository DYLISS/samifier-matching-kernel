# -*- coding: utf-8 -*-
"""Module used to query a MongoDB instance.


Explanations about the connect=False are here:
    http://api.mongodb.org/python/current/faq.html#using-pymongo-with-multiprocessing
    https://jira.mongodb.org/browse/PYTHON-1016

This module could initializes a default connection.
However, you may want to use multiprocessing without warnings, so you have to
use both context managers mongoengine.context_managers.switch_db & Mongo_Wrapper.

"""

# Standard imports
from mongoengine import *
from mongoengine import connect
from mongoengine.connection import disconnect
from mongoengine.connection import _connections
from mongoengine.context_managers import switch_db
from collections import defaultdict
from contextlib import contextmanager

# Custom imports
from matching_kernel import commons as cm

LOGGER = cm.logger()

# Explanations about the connect=False are here:
# http://api.mongodb.org/python/current/faq.html#using-pymongo-with-multiprocessing
# Here you have a default connection
# If you want to use multiprocessing without warnings, you have to use the
# both context managers mongoengine.context_managers.switch_db & Mongo_Wrapper
#connect('unifier',
#        host='mongodb://' + cm.MONGO_PATH, port=cm.MONGO_PORT,
#        connect=False)


@contextmanager
def Mongo_Wrapper(alias=None, **kwargs):
    """Context Manager for MongoDB"""
    # We use a random string for each connection
    if alias is None:
        import random
        alias = str(random.random())

    try:
        """Yield the alias and the MongoClient object"""
        _connect = connect('unifier', alias=alias,
                           host='mongodb://' + cm.MONGO_PATH,
                           port=cm.MONGO_PORT, **kwargs)
        LOGGER.debug("Mongo: Connexion created: " + alias)
        yield alias, _connect
    finally:
        disconnect(alias)
        LOGGER.debug("Mongo: List of remaining connections: [" + \
                     ", ".join(_connections.keys()) + "]")


class BiologicalObject(Document):
    """Abstract class for biological objects (i.e. Reaction & Metabolite)"""

    names     = ListField(StringField(required=True))
    databases = DictField()

    meta = {'allow_inheritance': True,
            'auto_create_index': False,
            'indexes': [
                # Hashed indexes are only on single fields
                '_cls',
                'names',
                'databases'
            ]
           }

    def str(self):
        return "Names: {}, Database: {}".format(names, databases)


class Metabolite(BiologicalObject):
    """Metabolite class"""

    formula   = StringField()

class Reaction(BiologicalObject):
    """Reaction class"""

    reversible = BooleanField(default=False)
    reagents   = ListField(ReferenceField(Metabolite))
    products   = ListField(ReferenceField(Metabolite))

################################################################################

def search_document_with_database_id(identifiers, bdd_queried,
                                     bio_object_type=None):
    """This function returns all documents that reference the given identifier
    for the given database.

    .. note::
        https://docs.mongodb.org/manual/tutorial/query-documents/#specify-multiple-criteria-for-array-elements

        use unifier
        db.getCollection('biological_object').find({names: "D-fructose 6-phosphate"})
        db.getCollection('biological_object').find({"databases.METACYC": "CPD-15709agaga"})
        db.getCollection('biological_object').count()

    :param arg1: Iterable of identifiers !!!.
    :param arg2: Database queried.
    :param arg3: (Optional) You can pass the type of queried objects.
        Thus, the system will use indexes !
    :return: Document for the BiologicalObject queried
        (if exists, None otherwise)
    :rtype: <BiologicalObject> or None

    """
    q = {'databases__' + bdd_queried.upper() + '__in': list(identifiers)}

    # No index
    if bio_object_type is None:
        return BiologicalObject.objects(**q)
    else:
        # Indexes
        assert (bio_object_type == Metabolite or
                bio_object_type == Reaction), "Bad type !"
        return bio_object_type.objects(**q)


def search_ids_with_ids(identifiers, src_bdd, dst_bdd, biological_object_type):
    """Exact identifier search.

    key: old identifier, value: set of new identifiers
    key: new identifier, value: set of synonyms

    """

    mapping_src_dst_ids = defaultdict(set)
    identifier_synonyms = dict()

    with Mongo_Wrapper(alias='default') as (alias, _):

        LOGGER.debug("Mongo multiprocessing: We are in connection " + alias)

        with switch_db(biological_object_type, alias) as biological_object_type:

            # For each given identifier
            # We search all documents that contain it
            for identifier in identifiers:
                q = {'databases__' + src_bdd.upper() + '__in': [identifier]}
                # LOGGER.debug("Mongo query:" + str(q))

                for obj in biological_object_type.objects(**q):

                    dst_identifiers = obj.databases.get(dst_bdd.upper(), None)

                    if dst_identifiers is None:
                        continue

                    mapping_src_dst_ids[identifier].update(dst_identifiers)

                    # Yes, it is the same document
                    # Yes old id and new id share the same names...
                    # But to avoid misunderstanding, we return a dict with
                    # new ids as keys
                    for new_id in dst_identifiers:
                        identifier_synonyms[new_id] = obj.names

    return mapping_src_dst_ids, identifier_synonyms


def search_ids_and_syns_with_names(names, bdd_queried):
    """For each given name this function returns 2 dictionaries.

    :Example:

    .. code-block:: javascript
        :linenos:

        (
            defaultdict(<class 'set'>,
                {'D-fructose 6-phosphate': {'CPD-15709', 'FRUCTOSE-6P'}}
            ),
            defaultdict(<class 'set'>,
                {'CPD-15709': {'D-fructose 6-phosphate'},
                 'FRUCTOSE-6P': {'beta-D-fructofuranose 6-phosphate', 'fructose-6P'}
                }
            )
        )

    :param arg1: Set of valid names (we know that they are referenced)
        i.e: There is no regular expression or something else here.
    :param arg2: Identifier of the queried database.
    :type arg1: <set>
    :type arg2: <str>
    :return: Tuple of 2 dictionaries.
        - given_name_identifiers: \
            key: valid name, value: identifiers
        - identifier_synonyms: \
            key: identifier, value: synonyms
    :rtype: <tpl <dict>, <dict>>
    """

    with Mongo_Wrapper(alias='default') as (alias, _):

        LOGGER.debug("Mongo multiprocessing: We are in connection " + alias)

        with switch_db(BiologicalObject, alias) as bio_obj:

            given_name_identifiers = defaultdict(set)
            identifier_synonyms    = defaultdict(set)

            # For each given name
            for name in names:

                objects = bio_obj.objects(names__in=[name])

                for object in objects:

                    ids_queried = object.databases.get(bdd_queried.upper(), None)

                    if ids_queried is not None:

                        # Multiple identifiers for 1 bdd for this entity
                        for id_queried in ids_queried:

                            # Get all synonyms for each id
                            identifier_synonyms[id_queried].update(object.names)

                        # Get all identifiers for each given name
                        given_name_identifiers[name].update(ids_queried)


    LOGGER.debug("MongoDB: Number of identifiers found: " + \
                 str(len(given_name_identifiers)) + \
                 ", number of identifiers with synonyms: " + \
                 str(len(identifier_synonyms)))

    return given_name_identifiers, identifier_synonyms


if __name__ == "__main__":

    with Mongo_Wrapper(alias='default') as (alias, _):

        print(search_ids_with_ids(['10fthf'], 'bigg', 'metacyc', Metabolite))
#        print(search_ids_and_syns_with_names(["1-propanol"], 'METACYC'))
#        print(search_ids_and_syns_with_names(['D-fructose 6-phosphate'], 'METACYC'))
#        print(search_document_with_database_id(['10-FORMYL-THF'], 'METACYC'))

