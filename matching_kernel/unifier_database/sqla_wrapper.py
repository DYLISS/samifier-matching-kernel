# -*- coding: utf-8 -*-
"""
Definitions of classes:
    -
    -

Context Manager:
    - SQLA_Wrapper

Functions used to deal with these classes.
# TODO: list that

"""

# Standard imports
import itertools as it
from collections import Counter
from shutil import rmtree

# Custom imports
from matching_kernel import commons as cm

# SQL Alchemy
from sqlalchemy import Table, Column, Integer,  String, DateTime, Float, Boolean, Text, Sequence
from sqlalchemy import ForeignKey, Index, create_engine, func, text, or_
from sqlalchemy.schema import UniqueConstraint
from sqlalchemy.ext.declarative import declared_attr
from sqlalchemy.ext.declarative import has_inherited_table
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship, sessionmaker, scoped_session, backref, joinedload, aliased
from sqlalchemy.orm.session import make_transient
from sqlalchemy.orm.exc import NoResultFound, MultipleResultsFound

LOGGER = cm.logger()

# /!\ initialization for SQL Alchemy Object Mapping
# /!\ This line MUST BE before Profile class that inherits from it
# /!\ This line MUST BE CALLED before any loading of SQL Engine
Base = declarative_base()

association_table = \
    Table('has_synonyms', Base.metadata,
          Column('biologicalobject_id',
                 Integer,
                 ForeignKey('biologicalobject.id',
                            onupdate="CASCADE",
                            ondelete="CASCADE")),
          Column('synonym_id',
                 Integer,
                 ForeignKey('synonym.id',
                            onupdate="CASCADE",
                            ondelete="CASCADE")),
          Index('has_synonyms_index', 'biologicalobject_id', 'synonym_id')
         )


################################################################################
class SQLA_Wrapper():
    """Context manager for DB wrapper

    Auto flush & commit changes on exit.

    """

    def __init__(self, **kwargs):
        self._kwargs = kwargs

    def __enter__(self):
        """Get a pointer to SQL Alchemy session

        :return: SQLAlchemy session.
        :rtype: <SQL session object>
        """
        self._session = loading_sql(**self._kwargs)
        # Populate default database
        #populate_default_database(self._session)

        return self._session

    def __exit__(self, exc_type, exc_value, traceback):
        """Check to see if it is ending with an exception.

        In this case, exception is raised within the with statement.
        => We do a session rollback.
        Otherwise we flush changes before commit them.

        """

        if (exc_type is not None) and (exc_type is not SystemExit):
            LOGGER.error("Rollback the database")
            self._session.rollback()
            return

        self._session.flush()
        self._session.commit()
        self._session.close()


def loading_sql(**kwargs):
    """Create an engine & create all the tables we need.

    :param: Optional settings passed to sqlalchemy
    :type: <dict>
    :return: session object
    :rtype: <Session()>

    """
    #Base = declarative_base() => see above, near imports
    # Create an engine and create all the tables we need

    engine = create_engine(cm.POSTGRESQL_PATH, **kwargs)
    #, echo="debug")
    Base.metadata.create_all(engine)

    #returns an object for building the particular session you want

    #   bind=engine: this binds the session to the engine,
    #the session will automatically create the connections it needs.
    #   autoflush=True: if you commit your changes to the database
    #before they have been flushed, this option tells SQLAlchemy to flush them
    #before the commit is gone.
    #   autocommit=False: this tells SQLAlchemy to wrap all changes between
    #commits in a transaction. If autocommit=True is specified,
    #SQLAlchemy automatically commits any changes after each flush;
    #this is undesired in most cases.
    #   expire_on_commit=True: this means that all instances attached
    #to the session will be fully expired after each commit so that
    #all attribute/object access subsequent to a completed transaction
    #will load from the most recent database state.

    # PAY ATTENTION HERE:
    # http://stackoverflow.com/questions/21078696/why-is-my-scoped-session-raising-an-attributeerror-session-object-has-no-attr
    return scoped_session(sessionmaker(bind=engine, autoflush=True))

def populate_default_database(session):
    """Fill the database with default objects.

    YOU HAVE TO DO THIS EVERY TIME YOU USE THE DB !

    :param: SQLAlchemy session.
    :type: <SQL session object>

    """

    # Initialize experiment 0 for the admin
    try:
        # Verify if the Experiment 0 exists
        admin_exp = Experiment.test_object(session, 0)

    except NoResultFound:
        LOGGER.debug("Creation of Experiment 0...")

        # Get all administrators
        admins = [user for user in User.get_all(session)
                  if user.is_admin()]

        if len(admins) == 0:
            return

        # Create a mock source file
        mock_file              = ResourceFile()
        mock_file.col_type     = 'sourcefile'
        mock_file.real_name    = 'mock_file_dont_remove_me_please'
        mock_file.name         = 'mock_file'
        mock_file.md5_checksum = 'mock_md5'

        # Create a new experiment for the first admin in list
        admin_exp              = Experiment()
        admin_exp.id           = 0
        admin_exp.src_file     = mock_file
        # Mock Origin => Origin is not important for this experiment
        # which cumulates multiple associations.
        admin_exp.dst_origin   = Origin.test_object(session, cm.O_MET)
        admin_exp.user         = admins[0]

    # Create/get Status objects
    LOGGER.debug("Creation of various default objects...")
    [Status.test_object(session, name) for name in cm.AUTH_STATUS]
    [Step.test_object(session, name) for name in cm.AUTH_STEPS]
    [Origin.test_object(session, name) for name in cm.AUTH_ORIGINS]

    session.flush()
    session.commit()

def simple_engine(admin=False):
    """Return connection to DB for raw queries

    :param arg: If set to True, a connexion with full privileges is returned !
    :type arg: <bool>
    """
    if admin:
        return create_engine(cm.POSTGRESQL_ADMIN_PATH).connect()
    else:
        return create_engine(cm.POSTGRESQL_PATH).connect()

def remove_experiments_locks(session):
    """Unlock all experiments when the server is restarted"""

    [exp.unlock(session) for exp in Experiment.get_all(session)]

def vacuum(session):
    """Suppress orphans in BiologicalObjects and Synonyms.

    Since these objects are involved in many-to-many relationship,
    this step can not be made automatically with `ON DELETE` RDBMS statement
    nor with `cascade` argument of ForeignKey object.
    A real vaccum is made at the end of the processing.

    .. note:: We bypass ORM system of cascades that are vastly less effecient
        than RDBMS implementation.

    .. note:: This function is quite efficient BUT it should be used in a
        background context. This kind of routine/maintenance requests have
        not to be called systematically in a production environment.

    .. note:: PGSQL documentation:
        http://www.postgresql.org/docs/9.4/static/sql-vacuum.html

    :param: SQLAlchemy session.
    :type: <SQL session object>
    :return: Log information for the API.
    :rtype: <str>
    """

    # Common raw connection
    raw_connection = simple_engine()

    # Get all ids of Biological objects that are not involved in any Association
    # SELECT biologicalobject.id as bioid, association.id
    # FROM biologicalobject
    # LEFT OUTER JOIN association ON association.src_object_id = biologicalobject.id or association.dst_object_id = biologicalobject.id
    # WHERE association.id IS NULL
    a = aliased(Association)
    ids_marked_for_deletion = \
        (session
         .query(BiologicalObject.id)
         .outerjoin(a, or_(a.src_object_id == BiologicalObject.id,
                           a.dst_object_id == BiologicalObject.id))
         .filter(a.id.is_(None)))
    ids_marked_for_deletion = list(it.chain(*[i for i in ids_marked_for_deletion]))

    # Bypass ORM cascades of deletion
    raw_connection.execute(
            text("DELETE FROM biologicalobject \
                  WHERE biologicalobject.id = ANY(:ids)"),
            ids=ids_marked_for_deletion)

    LOGGER.info("Vacuum: {} BiologicalObjects.".format(len(ids_marked_for_deletion)))
    log = "{} BiologicalObjects; ".format(len(ids_marked_for_deletion))

    ############################################################################

    # Get all ids of Synonym objects that are not involved in any Association
    # SELECT synonym.id as bioid, has_synonyms.synonym_id
    # FROM synonym
    # LEFT OUTER JOIN has_synonyms ON has_synonyms.synonym_id = synonym.id
    # WHERE has_synonyms.synonym_id IS NULL
    # PS: don't forget the .c in the attribute name when you use the Table()
    # object from scratch.
    ids_marked_for_deletion = \
        (session
         .query(Synonym.id)
         .outerjoin(association_table, association_table.c.synonym_id == Synonym.id)
         .filter(association_table.c.synonym_id.is_(None)))
    ids_marked_for_deletion = list(it.chain(*[i for i in ids_marked_for_deletion]))

    # Bypass ORM cascades of deletion
    raw_connection.execute(
            text("DELETE FROM synonym \
                  WHERE synonym.id = ANY(:ids)"),
            ids=ids_marked_for_deletion)

    LOGGER.info("Vacuum: {} Synonyms.".format(len(ids_marked_for_deletion)))
    log += "{} Synonyms.".format(len(ids_marked_for_deletion))

    ############################################################################

    # Real vacuum
    # Isolation level to 0 (out of current transaction)
    raw_connection = raw_connection.execution_options(
        isolation_level="AUTOCOMMIT"
    )
    raw_connection.execute(text("VACUUM"))

    return log


################################################################################
class Tablename():
    """This class is used to avoid useless code repetition:
        - __tablename__ definition (except for child class in inheritance model)
    """
    @declared_attr
    def __tablename__(cls):
        if has_inherited_table(cls):
            return None
        return cls.__name__.lower()

class HasId():
    """This class is used to avoid useless code repetition:
        - id attribute definition (except for child class in inheritance model)
    """
    @declared_attr.cascading
    def id(cls):
        if has_inherited_table(cls):
            return None
        else:
            return Column('id', Integer, primary_key=True)

class Item():
    """Convenient class inherited by all objects.
    It brings a class method used for counting & geting all objects of
    any class.
    """

    @classmethod
    def get_number(cls, session):
        """Return the number of objects stored in DB.

        :param arg1: SQLAlchemy session.
        :type arg1: <SQL session object>

        :return: Number of objects.
        :rtype: <int>

        """
        return session.query(cls).count()

    @classmethod
    def get_all(cls, session):
        """Get a list of all objects

        :param arg1: SQLAlchemy session.
        :type arg1: <SQL session object>

        :return: List of objects.
        :rtype: <list <Object>>

        """
        return session.query(cls).all()

class TestName():
    """Convenient class inherited by Status, Step, Origin only.
    This class brings them a class method used for testing purpose.
    (Test is based on their name, that's why only these objects are affected).
    """

    @classmethod
    def test_object(cls, session, name):
        """Test if Object exists in database.

        .. warning:: This function is used for Status, Step & Origin objects ONLY!
            (all objects that have an attribute 'name')

        If True => returns the object in DB.
        Otherwise => creates a new object.

        :param arg1: SQLAlchemy session.
        :param arg2: Allowed parameter that defines the required object.
            Please see 'AUTH_xxx' global variables at the top of this file.
        :type arg1: <SQL session object>
        :type arg2: <str>

        :return: Object Status or Step
        :rtype: <Status> or <Step>
        """

        # Pattern strategy tentative..
        # Dynamic selection of the Class

        assert (cls is Status and name in cm.AUTH_STATUS) \
               or (cls is Step and name in cm.AUTH_STEPS) \
               or (cls is Origin and name in cm.AUTH_ORIGINS) \
               or (cls is Synonym), \
               "Bad given parameter. Type must be an authorized string; " + \
               "Please read the docstring ! : " + name

        try:
            # one() raises an exception if no result or if more than 1 result
            query = session.query(cls).filter_by(name=name).one()
#            query = session.query(cls).filter(cls.name.like(name)).one()
            LOGGER.debug(str(name) + " is already in database : " + cls.__name__)
            # Return the object
            return query
        except NoResultFound:
            LOGGER.debug(str(name) + " first time : " + cls.__name__)
            # Add to database
            new_obj = cls(name)
            session.add(new_obj)
            session.flush()
            return new_obj
        except MultipleResultsFound:
            # This error makes the program crash. We can't continue on it.
            # Begin a new transaction after exception
            # All flush & pending objects will be deleted !
            session.rollback()
            raise


class ResourceFile(Base, Item):
    """Many in Many-To-One relationship with Experiment.

    This class defines the ResourceFiles of an Experiment object.

    .. warning:: It's a convenient binding with Doctrine ORM.
    """

    # Doctrine binding
    __tablename__ = "resource_file"

    # Fix sequence reference made by Doctrine
    # Without that, SQLA is unable to create a new object with an
    # autoincremented id
    id           = Column(Integer, Sequence('resource_file_id_seq'),
                          primary_key=True)
    real_name    = Column(String(255), nullable=False)
    name         = Column(String(50), nullable=False)
    md5_checksum = Column(String(50), nullable=False)
    upload_date  = Column(DateTime, default=func.now(), nullable=False)
    col_type     = Column("type", String(20), nullable=False)

    exp_src_objects = relationship("Experiment",
                                   backref=backref("src_file",
                                                   cascade="save-update, merge, delete"),
                                   foreign_keys="Experiment.src_file_id")

    exp_dst_objects = relationship("Experiment",
                                   backref=backref("dst_file",
                                                   cascade="save-update, merge"),
                                   foreign_keys="Experiment.dst_file_id")


class User(Base, Item):
    """

    This class defines the Users of the website.

    .. warning:: It's a convenient binding with Doctrine ORM.

    """

    # Doctrine binding
    __tablename__ = "users"

    # Fix sequence reference made by Doctrine
    # Without that, SQLA is unable to create a new object with an
    # autoincremented id
    id                    = Column(Integer, Sequence('users_id_seq'),
                                   primary_key=True)
    username              = Column(String(255), nullable=False)
    username_canonical    = Column(String(255), nullable=False)
    email                 = Column(String(255), nullable=False)
    email_canonical       = Column(String(255), nullable=False)
    enabled               = Column(Boolean, nullable=False)
    salt                  = Column(String(255), nullable=False)
    password              = Column(String(255), nullable=False)
    last_login            = Column(DateTime, nullable=True)
    locked                = Column(Boolean, nullable=False)
    expired               = Column(Boolean, nullable=False)
    expires_at            = Column(DateTime, nullable=True)
    confirmation_token    = Column(String(255), nullable=True)
    password_requested_at = Column(DateTime, nullable=True)
    roles                 = Column(Text, nullable=False)
    credentials_expired   = Column(Boolean, nullable=False)
    credentials_expire_at = Column(DateTime, nullable=True)

    # user attribute will be reachable in Experiment object
    experiments = relationship("Experiment", backref="user")

    def is_admin(self):
        """Return True if the user is an admin"""
        return True if ("ROLE_ADMIN" in self.roles) else False

    def __repr__(self):
        return "User(id:{}, CName:{})".format(self.id,
                                              self.username_canonical)


class Experiment(Tablename, Base, Item):
    """One in One-To-Many relationship with Association.

    This class defines the Experiment of an Association object.
    You can access to User object from it.

    .. warning:: It's a convenient binding with Doctrine ORM.
    """

    # Fix sequence reference made by Doctrine
    # Without that, SQLA is unable to create a new object with an
    # autoincremented id
    id            = Column(Integer, Sequence('experiment_id_seq'),
                           primary_key=True)
    src_file_id   = Column(Integer,
                           ForeignKey('resource_file.id',
                                      onupdate="CASCADE",
                                      ondelete="CASCADE"),
                           nullable=False)
    dst_file_id   = Column(Integer,
                           ForeignKey('resource_file.id',
                                      onupdate="CASCADE",
                                      ondelete="SET NULL"),
                           nullable=True)
    user_id       = Column(Integer,
                           ForeignKey('users.id',
                                      onupdate="CASCADE",
                                      ondelete="CASCADE"),
                         nullable=False)
    dst_origin_id = Column(Integer,
                           ForeignKey('origin.id',
                                      onupdate="CASCADE",
                                      ondelete="CASCADE"),
                           nullable=False)
    src_origin_id = Column(Integer,
                           ForeignKey('origin.id',
                                      onupdate="CASCADE",
                                      ondelete="CASCADE"),
                           nullable=True)
    date          = Column(DateTime, default=func.now(), nullable=False)
    lock_state    = Column(Boolean, default=False, nullable=False)

    # experiment attribute will be reachable in Association object
    associations = relationship("Association",
                                backref="experiment",
                                cascade="save-update, merge, delete, delete-orphan")


    def lock(self, session):
        """Lock the experiment in order to avoid other further computations."""
        self.lock_state = True
        session.commit()

    def unlock(self, session):
        """Unlock the experiment in order to permit further computations."""
        self.lock_state = False
        session.commit()

    def get_statistics(self):
        """Get the number of all associations grouped by their status.

        :return: Counter of all status.
        :rtype: <collections.Counter>
        """
        return Counter(assoc.status.name for assoc in self.associations)

    @classmethod
    def test_object(cls, session, exp_id):
        """Test if Experiment exists in database.

        If True => returns the object in DB.
        Otherwise => raise an exception.

        :param arg1: SQLAlchemy session.
        :param arg2: id of an experiment.
        :type arg1: <SQL session object>
        :type arg2: <str>
        :return: Object Experiment or raises an exception if Experiment is not
            in database.
        :rtype: <Experiment> or <Exception>
        """

        # Pattern strategy tentative..
        # Dynamic selection of the Class
        try:
            # one() raises an exception if no result or if more than 1 result
            query = session.query(cls).filter_by(id=exp_id).one()
            LOGGER.debug("Experiment " + str(exp_id) + " is already in database")
            return query
        except NoResultFound:
            # Begin a new transaction after this exception
            session.rollback()
            LOGGER.error("NoResultFound : Experiment " + \
                         str(exp_id) + " is not in database !!! => rollback")
            raise

    def validate(self, session):
        """This function is used to validate an Experiment.

        All associations that haven't the step "Exact" but the status
        "Already matched" are sended to the queue of
        validation of the administrator account.
        Scores of these associations (community & trusted) are incremented.
        Then, the experiment is deleted.

        .. note:: If there are no administrators, the experiment could not
            be deleted nor validated.

        TODO: Handle multiple administrators.

        :param arg1: SQLAlchemy session.
        :type arg1: <SQL session object>
        """

        def switch_to_admin_validation(association):
            """Change status, experiment & scores of the given association"""

            # If the owner of the experiment is an Admin,
            # validation_trusted score is incremented.
            # If the association is already in Experiment 0,
            # the validation_community is also incremented.
            if self.user in admins:
                # User is an admin

                # Ask if we can add the association to the Experiment 0
                # Integrity constraint verification
                # Return None if we can create it, the already created object otherwise
                object = Association.verify_object(
                            session,
                            admin_exp.id,
                            association.src_object.id,
                            dst_object_id=association.dst_object.id
                         )

                if object is None:
                    # We can add the association to the Experiment 0
                    association.status             = validation_status
                    association.experiment         = admin_exp
                    association.validation_trusted += 1
                else:
                    # The association already exists => we update it
#                    object.experiment = admin_exp
                    object.validation_trusted      += 1
                    object.validation_community    += 1

            else:
                # The user is not an admin => we put the association in
                # the new experiment attached to the admnin account.
                # This association will be pending validation by the admin.
                print("user pas admin")
                association.experiment             = admin_exp
                association.validation_community   += 1



        # Get associations with status "Already matched",
        # and with a not null sim_score value
        # (ie an association which was manually selected by a user)
        user_assocs = [assoc
                       for assoc in it.chain(
                            self.get_associations_with_status(cm.STA_AM),
                            self.get_associations_with_status(cm.STA_VA))
                       if assoc.sim_score != None]

        # If there are no associations => return
        if len(user_assocs) == 0:
            LOGGER.warning("Validation: No association can be validated.")
            self.delete(session)
            return

        # Get all administrators
        admins = [user for user in User.get_all(session)
                  if user.is_admin()]

        # If there are no administrators => return
        if len(admins) == 0:
            LOGGER.error("Validation: "
                         "No administrator has been found in the database !")
            return

        validation_status = Status.test_object(session, cm.STA_VA)

        if self.user in admins:
            try:
                # Verify if the Experiment 0 exists
                admin_exp = Experiment.test_object(session, 0)
            except NoResultFound:
                LOGGER.error("Validation: "
                             "There is no Experiment 0 in Admin account.")
                return

        else:
            # Create a mock source file
            mock_file              = ResourceFile()
            mock_file.col_type     = 'sourcefile'
            mock_file.real_name    = 'mock_file_dont_remove_me_please'
            mock_file.name         = 'mock_file'
            mock_file.md5_checksum = 'mock_md5'

            # Create a new experiment for the first admin in list
            admin_exp              = Experiment()
            admin_exp.src_file     = mock_file
            admin_exp.dst_origin   = self.dst_origin
            admin_exp.user         = admins[0]

            # Sync with DB
            session.flush()


        # Do the job
        [switch_to_admin_validation(assoc) for assoc in user_assocs]

        # Delete experiment
        self.delete(session)

        session.commit()

        LOGGER.info("Validation: Experiment {} was created/updated with {} "
                    "associations to be validated".format(admin_exp.id,
                                                          len(user_assocs)))

    def delete(self, session):
        """This function properly deletes the current experiment.
        Uploaded files & folders are both deleted.

        :param arg1: SQLAlchemy session.
        :type arg1: <SQL session object>

        """

        # Delete the experiment (and all the attached associations)
        session.delete(self)

        try:
            rmtree(cm.DIR_SERVER_USER_DATA + self.user.username_canonical)
        except PermissionError:
            LOGGER.error("Exp deletion: PermissionError")


    def delete_proposal(self, session):
        """Delete all associations with the status "Proposal".

        .. note:: There is a commit here.

        :param arg1: SQLAlchemy session.
        :type arg1: <SQL session object>

        """

        # Get all associations with status "Proposal"
        proposal_assocs = self.get_associations_with_status(cm.STA_PROP)
        sta_tbm         = Status.test_object(session, cm.STA_TBM)

        # Delete all these associations => switch them to "To be matched"
        def force_assoc_reinit(assoc):
            # Ask if we can add the association to the Experiment 0
            # Integrity constraint verification
            # Return None if we can create it, the already created object otherwise
            object = Association.verify_object(
                        session,
                        self.id,
                        assoc.src_object.id,
                        dst_object_id=None
                     )

            if object is not None:
                session.delete(assoc)
                return

            assoc.status               = sta_tbm
            assoc.dst_object           = None
            assoc.sim_score            = None
            assoc.validation_community = 0
            assoc.validation_trusted   = 0

        [force_assoc_reinit(assoc) for assoc in proposal_assocs]

        session.commit()


    def get_associations_with_status(self, status_name):
        """Convenient function used to return all associations in the current
        experiment, with the given status name.

        :param arg1: Status name in AUTH_STATUS.
        :type arg1: <str>
        :return: List of Association objects that match the given status.
        :rtype: <generator <Association>>

        """

        return (assoc for assoc in self.associations
                if assoc.status.name == status_name)


    def forge_data(self, session, status_name):
        """Return all attributes of Associations objets which match the
        given status & experiment.

        This function is used by the api to get data from database.

        :Example:

        .. code-block:: javascript
            :linenos:

            {
              "associations": [
                {
                    "dst": null,
                    "id": 47,
                    "sim_score": null,
                    "src": {
                        "formula": "",
                        "id": "a",
                        "names": [],
                        "origin": "user src"
                    },
                    "step": "exact",
                    "validation_community": 0,
                    "validation_trusted": 0
                },
            }

        :param arg1: Status name in AUTH_STATUS.
        :type arg1: <str>
        :return: List of associations filtered by status name.
        :rtype: <list>
        """

        # Bad way (not optimized) of doing things
#        return [assoc.get_attributes()
#                    for assoc in self.get_associations_with_status(status_name)]
#
        query = session.query(Association).\
            options(#Load(Association).load_only('dst_object', 'src_object', 'sim_score', 'validation_community', 'validation_trusted'),
                    joinedload(Association.dst_object).joinedload('origin').load_only('name'),
                    joinedload(Association.dst_object).joinedload('synonyms'),
                    joinedload(Association.src_object).joinedload('origin').load_only('name'),
                    joinedload(Association.src_object).joinedload('synonyms'),
                    joinedload(Association.src_object.of_type(Reaction)).load_only('bio_id', 'ecnumber'),
                    joinedload(Association.dst_object.of_type(Reaction)).load_only('bio_id', 'ecnumber'),
                    joinedload(Association.src_object.of_type(Metabolite)).load_only('bio_id', 'formula'),
                    joinedload(Association.dst_object.of_type(Metabolite)).load_only('bio_id', 'formula'),
                    joinedload(Association.step),
                    ).\
                    select_from(Status).\
                        join(Association.status).\
                        join(Association.experiment).\
                        filter(Experiment.id == self.id,
                               Status.name == status_name
                        )

        return [result.get_attributes() for result in query]


    def get_server_directory(self):
        """Return the directory where files (log, input, output files)
        are stored.

        .. note:: The path ends with '/'

        :return: Directory path of the experiment.
        :rtype: <str>
        """

        return cm.DIR_SERVER_USER_DATA + self.user.username_canonical + \
               '/' + str(self.id) + '/'

    def append_to_logfile(self, text):
        """Append the given text to the logfile of the current experiment.

        :param arg1: Text to stor in logfile.
        :type arg1: <str>
        """
        with open(self.get_server_directory() + 'log.txt', 'a') as logfile:
            logfile.write(text + '\n')

    def get_logfile_content(self):
        """Return the content of the logfile

        :return: Logfile content.
        :rtype: <str>
        """
        with open(self.get_server_directory() + 'log.txt', 'r') as logfile:
            return logfile.read()

    def __repr__(self):
        return "Experiment(id:{}, src_file:{}, user_id:{}, dst_origin_id: {}, assocs:{})".format(
            self.id,
            self.src_file,
            self.user_id,
            self.dst_origin_id,
            len(self.associations))


class Status(Tablename, HasId, Base, Item, TestName):
    """One in One-To-Many relationship with Association.

    This class defines the status of an association.
    The user could change the status on the fly by changing this type of ref.
    """

    # Unicity constraint
    # Unicity creates an index
    __table_args__ = (UniqueConstraint('name', name="status_constraint_1"),)

    name = Column("name", String(50), nullable=False)

    # status attribute will be reachable in Association object
    associations = relationship("Association", backref="status")

    def __init__(self, name):
        """Constructor takes the string of the name"""
        self.set_name(name)

    def set_name(self, value):
        """Setter for attribute 'name'.

        :param: Name. It must be a valid name (see AUTH_STATUS global var)
        :type: <str>
        """
        assert (value in cm.AUTH_STATUS), "Integrity error: Bad Status name"
        self.name = value

    def __repr__(self):
        return "Status(id:{}, name:{})".format(self.id,
                                               self.name)


class Step(Tablename, HasId, Base, Item, TestName):
    """One in One-To-Many relationship with Association.

    This class defines the step of the association process.
    The program itself defines this object according to the origin of
    a matching.
    """

    # Unicity constraint
    # Unicity creates an index
    __table_args__ = (UniqueConstraint('name', name="step_constraint_1"),)

    name = Column("name", String(20), nullable=False)

    # step attribute will be reachable in Association object
    associations = relationship("Association", backref="step")

    def __init__(self, name):
        """Constructor takes the string of the name"""
        self.set_name(name)

    def set_name(self, value):
        """Setter for attribute 'name'.

        :param: Name. It must be a valid name (see AUTH_STEPS global var)
        :type: <str>
        """
        assert (value in cm.AUTH_STEPS), "Integrity error: Bad Step name"
        self.name = value

    def __repr__(self):
        return "Step(id:{}, name:{})".format(self.id,
                                             self.name)


class Origin(Tablename, HasId, Base, Item, TestName):
    """One in One-To-Many relationship with Association.

    This class defines the origin of biological objects
    (ie what database it came from).
    The program itself defines this origin during reference database querying.
    """

    # Unicity constraint
    # Unicity creates an index
    __table_args__ = (UniqueConstraint('name', name="origin_constraint_1"),)

    name = Column(String(20), nullable=False)
    prefix = Column(String(255), nullable=True)

    # Suppress Origin object cascades the deletion to Experiment object
    exp_src_objects = relationship("Experiment",
                                   backref="src_origin",
                                   foreign_keys="Experiment.src_origin_id",
                                   cascade="save-update, merge, delete")

    exp_dst_objects = relationship("Experiment",
                                   backref="dst_origin",
                                   foreign_keys="Experiment.dst_origin_id",
                                   cascade="save-update, merge, delete")

    # origin attribute will be reachable in Association object
    biologicalobjects = relationship("BiologicalObject", backref="origin")

    def __init__(self, name):
        """Constructor takes the string of the name"""
        self.set_name(name)

    def set_name(self, value):
        """Setter for attribute 'name'.

        :param: Name. It must be a valid name (see AUTH_ORIGINS global var)
        :type: <str>
        """
        assert (value in cm.AUTH_ORIGINS), "Integrity error: Bad Origin name"
        self.name = value
        self.prefix = cm.PREFIXES_ORIGINS.get(value, '')

    def __repr__(self):
        return "Origin(id:{}, name:{})".format(self.id,
                                               self.name)


class Synonym(Tablename, HasId, Base, Item, TestName):
    """Many in Many-To-Many relationship with Association.

    This class defines all names of biological objects.
    .. note:: There are many names for 1 object. But each synonym is unique.
    """

    # Unicity constraint
    # Unicity creates an index
    __table_args__ = (UniqueConstraint('name', name="synonym_constraint_1"),)

    name = Column(String(384), nullable=False)

    def __init__(self, name):
        """Constructor takes the string of the name.

        :param: A synonym for a biological object.
        :type: <str>
        """
        self.name = name

    def __repr__(self):
        return "Synonym(id:{}, name:{})".format(self.id,
                                                self.name)


class BiologicalObject(Tablename, HasId, Base, Item):
    """One in double One-To-Many relationship with Association.

    .. note:: Single Table Inheritance
        The table is specified in the base mapper only;
        for the inheriting classes, leave their table parameter blank

    Attributes:
        * 1 biological id: it's the id taken from the database, according to \
        the 'Origin' object. \
        `bio_id`

        * 1 Origin attached: a bidirectionnal relationship in many-to-one, \
        where the "reverse" side is a one-to-many 'biologicalobjects' \
        attribute in "Origin" object. \
        `origin`

        * N src Associations attached: a bidirectionnal relationship in one-to-many, \
        where the “reverse” side is a many to one, specified by the backref \
        'src_object' option in "Association" object. \
        `assoc_src_objects`

        * N dst Associations attached: a bidirectionnal relationship in one-to-many, \
        where the “reverse” side is a many to one, specified by the backref \
        'dst_object' option in "Association" object. \
        `assoc_dst_objects`

        * N Synonyms attached: a bidirectionnal relationship in many-to-many, \
        where the “reverse” side is specified with the backref \
        'biologicalobjects' option in "Synonym" object. \
        `synonyms`
    """

    # Unicity constraint
    # Unicity creates an index
    __table_args__ = (UniqueConstraint(
                        'bio_id',
                        'origin_id',
                        'type',
                        name="biologicalobject_constraint_1"),
                      Index('bio_obj_idx1', 'bio_id', 'type', 'origin_id',
                            postgresql_using='btree'),
                      Index('bio_obj_idx2', 'type', 'bio_id', 'origin_id',
                            postgresql_using='btree')
                     )

    origin_id = Column(Integer,
                       ForeignKey('origin.id',
                                  onupdate="CASCADE",
                                  ondelete="CASCADE"),
                       nullable=False)
    bio_id    = Column(String(80), nullable=False)
    type      = Column(String(20))

    # src_object & dst_object attributes will be reachable in Association object
    assoc_src_objects = relationship("Association",
                                     backref="src_object",
                                     foreign_keys="Association.src_object_id")
    assoc_dst_objects = relationship("Association",
                                     backref="dst_object",
                                     foreign_keys="Association.dst_object_id")

    # Relation between synonyms & biological object is made
    # with an association_table because it's configured as many to many.
    # => 'has_synonyms'.
    synonyms          = relationship("Synonym",
                                     secondary=association_table,
                                     backref="biologicalobjects")

#    reactants = relationship("Reactant", back_populates="reaction", foreign_keys="Reactant.reaction_id")
#    reactions = relationship("Reactant", back_populates="metabolite", foreign_keys="Reactant.metabolite_id")

    # 'type' is the discrimiator column of single table inheritance
    __mapper_args__ = {
        'polymorphic_on':type,
        'polymorphic_identity':'biologicalobject'
    }

    def __init__(self, bio_id, origin):
        """Constructor for the BiologicalObject class

        .. note:: This constructor is called from inherited classes.

        :param arg1: Biological id according to the given Origin object.
        :param arg2: Origin object which defines the database used.
        :type arg1: <str>
        :type arg2: <Origin>
        """
        self.bio_id = bio_id
        self.origin = origin

    def update_synonyms(self, session, new_synonyms):
        """Fill & update synonyms for the current object.

        .. note:: Unique constraint is verified by this function.

        :param arg1: SQLAlchemy session.
        :param arg2: Iterable of synonyms.
        :type arg1: <SQL session object>
        :type arg2: <set>
        """

        # new_synonyms is a set...
        # but we secure this variable with a forced transtypage
        synonyms_to_be_inserted = set(new_synonyms) - \
                                  {synonym.name for synonym in self.synonyms}

        [self.synonyms.append(Synonym.test_object(session, synonym))
            for synonym in synonyms_to_be_inserted]

    def get_attributes(self):
        """Return a dictionary of attributes used in the website.

        :return: Dictionary of useful attributes.
        :rtype: <dict>
        """
        return {'origin': self.origin.name,
                'id': self.bio_id,
                'names': [synonym.name for synonym in self.synonyms]
               }

    def __repr__(self):
        return \
            "BiologicalObject(id:{}, bio_id:{}, origin_id:{}, synonyms:{})".format(
                self.id,
                self.bio_id,
                self.origin.id,
                self.synonyms)

    @staticmethod
    def test_object(session, bio_id, obj_type, origin=None):
        """Test if BiologicalObject with the given type & bio_id exists in DB.

        If True => Returns the object(s) in DB.
        Otherwise => Creates it.

        .. note:: If there are many objects with same bio_id,
            all of them are returned.
        .. note:: Origin verified ONLY if it is a given parameter.
            If Origin is given, this function returns a BiologicalObject
            instead of an iterable of BiologicalObjects !
            (Take a look at return definition below)

        :param arg1: SQLAlchemy session.
        :param arg2: BiologicalObject's identifier.
        :param arg3: BiologicalObject's type (Metabolite or Reaction)
        :param arg4: (Optional), Origin object of the identifier.
        :type arg1: <SQL session object>
        :type arg2: <str>
        :type arg3: <classname>
        :type arg4: <Origin>
        :return: /!\ A list with the new object or objects with same bio_id,
            IF origin is not set !
            IF origin is set, we expect 1 unique BiologicalObject,
            SO => the type is not an iterable !
            BE CAREFUL WITH THAT PLEASE !
        :rtype: <list <BiologicalObject>> or <BiologicalObject>
        """

        # Forge select query

        if origin is None:
            # Search on bio_id only !
            query = session.query(obj_type).filter_by(
                        bio_id=bio_id
                    ).all()
        else:
            # Search on bio_id AND origin !
            query = session.query(obj_type).filter_by(
                        bio_id=bio_id,
                        origin_id=origin.id
                    ).all()

        results = [entity for entity in query]

        if len(results) == 0:
            # The biological object is not in DB
            # => we create it with the given origin
            LOGGER.debug(bio_id + " First time for : " + str(obj_type.__name__))

            if origin is None:
                # Search on bio_id only !
                return [obj_type(bio_id, origin)]
            # Search on bio_id AND origin !
            return obj_type(bio_id, origin)
        else:
            # Many or 1 object(s) => We return all of them
            if len(results) > 1:

                if origin is not None:
                    # If origin is set, we expect 1 unique Biological object in DB.
                    # If there are more than 1 object uniqueness is not complied.

                    LOGGER.error("Integrity problem: many " + \
                                 str(obj_type.__name__) + \
                                 " retrieved when 1 object is expected for " + \
                                 bio_id)

                    raise AssertionError("Integrity problem: "
                                         "many objects retrieved when 1 object is expected")

            else:
                LOGGER.debug("Only 1 " + str(obj_type.__name__) + \
                            " in DB for "+bio_id)
                if origin is not None:
                    # Search on bio_id AND origin !
                    return results[0]
            # Search on bio_id only !
            return results


class Metabolite(BiologicalObject):
    """relationship with Association

    .. note:: Omit the __tablename__,
        indicating they do not have a mapped table of their own.

    Additional attributes:
        * 1 formula string: \
        formula

        * A list of reactions where this metabolite is involved \
        reactions
    """

    formula = Column("formula", String(255), nullable=True)

    reactions = relationship("Reactant",
                             back_populates="metabolite",
                             foreign_keys="Reactant.metabolite_id",
                             cascade="save-update, merge, delete, delete-orphan")

    __mapper_args__ = {
        'polymorphic_identity':'metabolite'
    }

    def __init__(self, bio_id, origin, formula=""):
        """Constructor for the Metabolite class

        .. note:: This constructor calls the constructor of the parent class.

        :param arg1: Biological id according to the given Origin object.
        :param arg2: Origin object which defines the database used.
        :param arg3: Formula for the given metabolite.
        :type arg1: <str>
        :type arg2: <Origin>
        :type arg3: <str>
        """
        BiologicalObject.__init__(self, bio_id, origin)
        self.formula = formula

    def get_attributes(self):
        """Return a dictionary of attributes used in the website.

        :return: Dictionary of useful attributes.
        :rtype: <dict>
        """
        attribs = BiologicalObject.get_attributes(self)
        attribs['formula'] = self.formula

        return attribs

class Reaction(BiologicalObject):
    """relationship with Association

    TODO: pas de vérification de la liste des réactants lorsqu'on vérifie
    la présence de la réaction dans la base de données.

    .. note:: Omit the __tablename__,
        indicating they do not have a mapped table of their own.

    Additional attributes:
        * A list of reactants where involved in this reaction\
        reactants
    """

    reactants = relationship("Reactant",
                             back_populates="reaction",
                             foreign_keys="Reactant.reaction_id",
                             cascade="save-update, merge, delete, delete-orphan", lazy="joined")

    # Limit to 4 EC numbers (see the setter)
    ecnumber  = Column(String(70), nullable=True)

    __mapper_args__ = {
        'polymorphic_identity':'reaction'
    }

    def __init__(self, bio_id, origin):
        """Constructor for the Metabolite class

        .. note:: This constructor calls the constructor of the parent class.

        :param arg1: Biological id according to the given Origin object.
        :param arg2: Origin object which defines the database used.
        :type arg1: <str>
        :type arg2: <Origin>
        """
        BiologicalObject.__init__(self, bio_id, origin)

    @property
    def reagents(self):
        """Getter & convenience function to retrieve all reagents
        for this reaction.

        :return: List of reagents (Metabolites).
        :rtype: <list>
        """
        return [reactant.metabolite for reactant in self.reactants
                if reactant.role == 'R']

    @property
    def products(self):
        """Getter & convenience function to retrieve all products
        for this reaction.

        :return: List of products (Metabolites).
        :rtype: <list>
        """
        return [reactant.metabolite for reactant in self.reactants
                if reactant.role == 'P']

    def set_reactants_with_ids(self, session, origin, reagents=list(), products=list()):
        """This setter allows you to set the list of reactants for this reaction.
        You must pass a list of identifiers of reagents & products.

        .. note:: `Reactant` is the association class which will link
            the metabolite class with the reaction class.

        .. warning:: This setter DOES NOT UPDATE any previous list !
            Previous data in reactants attribute will be overwritten.

        .. warning:: Named parameters (reagents & products) ARE NOT optional !
            Well.. it will be your unique & last chance to
            make the insertions securely !

        :param arg1: SQLAlchemy session.
        :param arg2: Origin object that defines the exact provenance of
            the given reactants. Usually, it is from user source during
            a creation of an experiment, and it is a database during
            matching process.
        :param arg3: List of ids of reagents for this reaction.
        :param arg4: List of ids of products for this reaction.
        :type arg1: <SQL session object>
        :type arg2: <Origin>
        :type arg3: <list <str>>
        :type arg4: <list <str>>
        """

        reagents_objs = \
            (Reactant(role="R",
                      metabolite=BiologicalObject.test_object(session,
                                                              metab_bio_id,
                                                              Metabolite,
                                                              origin)
                     ) for metab_bio_id in reagents)

        products_objs = \
            (Reactant(role="P",
                      metabolite=BiologicalObject.test_object(session,
                                                              metab_bio_id,
                                                              Metabolite,
                                                              origin)
                     ) for metab_bio_id in products)

        self.reactants = list(it.chain(reagents_objs, products_objs))

    def set_reactants_with_objects(self, reagents=list(), products=list()):
        """This setter allows you to set the list of reactants for this reaction.
        You must pass a list of objects of reagents & products.

        .. note:: `Reactant` is the association class which will link
            the metabolite class with the reaction class.

        .. warning:: This setter DOES NOT UPDATE any previous list !
            Previous data in reactants attribute will be overwritten.

        .. warning:: Named parameters (reagents & products) ARE NOT optional !
            Well.. it will be your unique & last chance to
            make the insertions securely !

        :param arg1: List of objects of reagents for this reaction.
        :param arg2: List of objects of products for this reaction.
        :type arg1: <list <Metabolite>>
        :type arg2: <list <Metabolite>>

        """

        reagents_objs = \
            (Reactant(role="R",
                      metabolite=metab
                     ) for metab in reagents)

        products_objs = \
            (Reactant(role="P",
                      metabolite=metab
                     ) for metab in products)

        self.reactants = list(it.chain(reagents_objs, products_objs))

    def get_reagent_bio_ids(self):
        """Getter & convenience function to retrieve all ids of reagents
        for this reaction.

        :return: List of reagent ids (Metabolite ids).
        :rtype: <list>
        """
        return [reactant.metabolite.bio_id for reactant in self.reactants
                if reactant.role == 'R']

    def get_product_bio_ids(self):
        """Getter & convenience function to retrieve all ids of products
        for this reaction.

        :return: List of product ids (Metabolite ids).
        :rtype: <list>
        """
        return [reactant.metabolite.bio_id for reactant in self.reactants
                if reactant.role == 'P']

    def set_ecnumber(self, ecnumbers):
        """Ensure that this field fits with the limitation in the database.

        .. note:: This setter is typically used when someone retrieve
            ecnumber from the reference database.

        :param: Iterable of EC numbers.
        :type: <set>

        """

        assert isinstance(ecnumbers, list) or isinstance(ecnumbers, set)
        # Limit to 70
        self.ecnumber = ", ".join(ecnumbers)[:69]


    def __repr__(self):
        return \
            "Reaction(id:{}, bio_id:{}, origin_id:{}, synonyms:{}, reactants:{})".format(
                self.id,
                self.bio_id,
                self.origin.id,
                self.synonyms,
                self.reactants)

    def get_attributes(self):
        """Return a dictionary of attributes used in the website.

        :return: Dictionary of useful attributes.
        :rtype: <dict>
        """
        attribs = BiologicalObject.get_attributes(self)
        attribs['reagents'] = self.get_reagent_bio_ids()
        attribs['products'] = self.get_product_bio_ids()
        attribs['ecnumber']  = self.ecnumber

        return attribs


class Reactant(Base):
    """Association class involved in a many to many relationship
    between Reaction & Metabolite classes.

    This class/association table carries a column that defines
    the nature of the role of a metabolite in a reaction.

    .. note:: Reaction & Metabolite classes are both inherited from
        BiologicalObject class.
        Pay attention to the relationship settings !

    Attributes:
        * role: Metabolite role in the given reaction.

        * metabolite: Metabolite involved in the given reaction.

        * reaction: Reaction involved.
    """

    __tablename__ = 'is_involved'

    # Note the composite primary key on metabolite, reaction and role.
    metabolite_id = Column(Integer,
                           ForeignKey('biologicalobject.id',
                                      onupdate="CASCADE",
                                      ondelete="CASCADE"),
                           primary_key=True)
    reaction_id   = Column(Integer,
                           ForeignKey('biologicalobject.id',
                                      onupdate="CASCADE",
                                      ondelete="CASCADE"),
                           primary_key=True)
    role          = Column(String(1),
                           primary_key=True)

    reaction      = relationship("Reaction",
                                 back_populates="reactants",
                                 primaryjoin="Reaction.id==Reactant.reaction_id")
    metabolite    = relationship("Metabolite",
                                 back_populates="reactions",
                                 primaryjoin="Metabolite.id==Reactant.metabolite_id", lazy="joined")

    def __repr__(self):
        return \
            "Reactant(metab_id:{}, reac_id:{}, role:{})".format(
                self.metabolite_id,
                self.reaction_id,
                self.role)


class Association(Tablename, HasId, Base, Item): # Child / Many
    """Defines table structure & attributes for an Association object.

    Attributes:
        * 1 distance score: This score informs the user of the confidence level \
        of the association. \
        sim_score

        * 1 number of validations by community users: The number of experiments \
        where the association was validated. \
        validation_community

        * 1 number of validations by trusted users: The number of experiments \
        where the association was validated by an admin. \
        validation_trusted

        * 1 Status attached: a bidirectionnal relationship in many-to-one, \
        where the "reverse" side is a one-to-many 'associations' \
        attribute in "Status" object. \
        status

        * 1 Step attached: a bidirectionnal relationship in many-to-one, \
        where the "reverse" side is a one-to-many 'associations' \
        attribute in "Step" object. \
        step

        * 1 src BiologicalObject attached: a bidirectionnal relationship in many-to-one, \
        where the “reverse” side is a one-to-many, specified by the backref \
        'assoc_src_objects' option in "BiologicalObject" object. \
        src_object

        * 1 dst BiologicalObject attached: a bidirectionnal relationship in many-to-one, \
        where the “reverse” side is a one-to-many, specified by the backref \
        'assoc_dst_objects' option in "BiologicalObject" object. \
        dst_object

        * 1 Experiment attached: \
        experiment

    """

    # SQLAlchemy will automatically set AUTO_INCREMENT on the first
    #Integer primary key column which is not marked as a foreign key
    # Limitation of MySQL: autoincrement MUST be on a primary Key...
    # BUT: i prefer use a nice constraint on the 3 covered columns
    # Unicity creates an index
    # => http://docs.sqlalchemy.org/en/latest/dialects/mysql.html#mysql-unique-constraints-and-reflection
    __table_args__ = (
        UniqueConstraint("experiment_id",
                         "src_object_id",
                         "dst_object_id",
                         name="association_constraint_1"),
        Index('association_idx', 'experiment_id', 'status_id',
              'src_object_id', 'dst_object_id',
              postgresql_using='btree')
    )

    experiment_id = Column(Integer,
                           ForeignKey('experiment.id',
                                      onupdate="CASCADE",
                                      ondelete="CASCADE"),
                           nullable=False)
    status_id     = Column(Integer,
                           ForeignKey('status.id',
                                      onupdate="CASCADE",
                                      ondelete="CASCADE"),
                           nullable=False)
    step_id       = Column(Integer,
                           ForeignKey('step.id',
                                      onupdate="CASCADE",
                                      ondelete="CASCADE"),
                           nullable=False)
    src_object_id = Column(Integer,
                           ForeignKey('biologicalobject.id',
                                      onupdate="CASCADE",
                                      ondelete="CASCADE"),
                           nullable=False)
    dst_object_id = Column(Integer,
                           ForeignKey('biologicalobject.id',
                                      onupdate="CASCADE",
                                      ondelete="CASCADE"),
                           nullable=True)

    sim_score            = Column("sim_score", Float, nullable=True)
    validation_community = Column("validation_community", Integer, default=0)
    validation_trusted   = Column("validation_trusted", Integer, default=0)

    def __init__(self, experiment, status, step):
        """Constructor takes experiment id and the current session.

        .. warning:: Be careful here !
        Because we can't let the user create an Association Object with
        BiologicalObjects in it while the referenced Experiment doesn't exists.
        You HAVE TO verify the existence of your Experiment here !

        :param arg1: SQLAlchemy session.
        :param arg2: Experiment ID used to retrieve the Experiment in DB.
            Each Association is linked to an Experiment !
        :param arg3: Status object attached to the Association.
        :param arg4: Step object attached to the Association.
        :type arg1: <SQL session object>
        :type arg2: <str>/<int>
        :type arg3: <Status>
        :type arg4: <Step>
        """

        self.status     = status
        self.step       = step
        self.experiment = experiment
        self.src_object = None
        self.dst_object = None

    def use_status(self, sta_va=None, sta_prop=None):
        """This function is used during set_biological_objects() and during
        the duplication of association in validation process.

        These functions need these 2 attributes.

        :param arg1: "Validation" status object.
        :param arg2: "Proposal" status object.
        :type arg1: <Status>
        :type arg2: <Status>

        """

        assert ((sta_prop != None) and (sta_va != None))

        self.sta_prop = sta_prop
        self.sta_va   = sta_va

    # Intelligent setter
    def set_biological_objects(self, session, src_data, dst_data=None):
        """Intelligent setter for BiologicalObjects in Association.

        - 2 params => vérif du type commun + contrainte
        - 1 param src => on vérifie instantanément la contrainte

        .. warning:: Si erreur:
            Lève une exception (assert) qui empechera quiconque de faire
            session.add() sur cette association.
            Il faudra faire un session.expugne() sur l'association qu'on
            s'appretait à ajouter.

        .. note:: This functions uses 2 attributes sta_va, sta_prop;
            These params can BE passed via use_status
            (for optimization purposes).

        .. TODO:: ajouter 1 requete aussi pour rappatrier 1 objet précis
            en tenant compte de l'origine

        :param arg1: SQLAlchemy session.
        :param arg2: Tuple of <bio_id>, <src_origin>, <src_obj_type>
        :param arg3: Tuple of <bio_id>, <dst_origin>, <dst_obj_type>
        :param arg4: Status "Validation"
        :param arg5: Status "Proposal"
        :type arg1: <SQL session object>
        :type arg2: <tuple>
        :type arg3: <tuple>
        :type arg4: <Status>
        :type arg5: <Status>
        :return: None or Assertion that have to be catched for expungning
            the Association object.
        :rtype: None or <AssertionError>
        """

        # Get source object settings
        src_bio_id, src_origin, src_obj_type = src_data
        # Incomplete or full association ?
        if dst_data is not None:
            # Full
            dst_bio_id, dst_origin, dst_obj_type = dst_data

            # Type verification (same between the 2 objects).
            assert (dst_obj_type == src_obj_type), \
                "Dest type must be the same as Src type"

            dst_object = BiologicalObject.test_object(session,
                                                      dst_bio_id,
                                                      dst_obj_type,
                                                      dst_origin)

        else:
            # Incomplete
            dst_object  = None

        # Src obj is always given
        src_object = BiologicalObject.test_object(session,
                                                  src_bio_id,
                                                  src_obj_type,
                                                  src_origin)

        # Vérification si la création de l'association partielle est légitime
        # => objet source n'est pas déjà impliqué dans une association complète.
        # (par ex via 1 mapping manuel)
        if dst_data is None:
            # Toutes les associations completes liées à l'expérience en cours,
            # où l'objet source est présent en tant qu'objet source.
            # Si il y en a: on ne crée pas d'association partielle
            # => l'objet est déjà mappé

            # Obj is in src in these associations
            # AND, dst_object of these associations must not be None
            # if the obj is already involved in full association,
            # we don't recreate an incomplete association.
            already_involved_objs = \
                [assoc for assoc in src_object.assoc_src_objects
                    if assoc.experiment.id == self.experiment.id
                    and assoc.dst_object is not None]

            # Multiple full associations ? => stop
            if len(already_involved_objs) != 0:
                LOGGER.debug("Src object is already in full association: " +\
                             str(len(already_involved_objs)) +\
                             " => stop insert")

                raise AssertionError("Src object is already in full association")


            ####################################################################

            # Duplication of all associations with the status "validation",
            # were objects are involved.
            # List of objects with the same id, in the DB
            src_objects = BiologicalObject.test_object(session,
                                                       src_bio_id,
                                                       src_obj_type)

            duplicated_associations = self.create_association(session, src_objects)

            # Association were duplicated => we don't add the current
            # incomplete association to the session
            if len(duplicated_associations) != 0:
                LOGGER.debug("Src object is already in full Validation association: " +\
                             str(len(duplicated_associations)) +\
                             " => stop insert")

                raise AssertionError("Src object is already in full validation association")


        # Vérification si on peut créer l'association partielle
        # ou si on peut créer l'association complete (si un objet de destination
        # a été donné.
        # Integrity constraint verification
        # Return None if we can create it, the already created object otherwise
        # If None: we raise an assertion, and the Association will be expugned
        # from the current session.
        object = Association.verify_object(
                    session,
                    self.experiment.id,
                    src_object.id,
                    dst_object_id=None if dst_object is None else dst_object.id
                 )

        assert (object is None), \
            "The association is already in database !"

        # Construct the object
        self.src_object = src_object
        self.dst_object = dst_object


    # Setters
    def set_dst(self, session, bio_id, origin, obj_type):
        """/!\ Deprecated
        Set the destination object of the association.

        This functions seeks if the object already exists in database.
        If it is the case, we search if the Association has a 'validation'
        status. If it is the case, this association is duplicated and attached
        to the current Experiment.

        .. note:: There is a verification that the given object is the same type
            of the object in src.

        :param arg1: SQLAlchemy session.
        :param arg2: Identifier of the BiologicalObject to construct/retrieve.
        :param arg3: Origin object (database etc.) of the BiologicalObject.
        :param arg4: Type of the BiologicalObject to construct/retrieve.
        :type arg1: <SQL session object>
        :type arg2: <str>
        :type arg3: <Origin>
        :type arg4: <str>
        """

        # Type verification (same between the 2 objects).
        if self.src_object is not None:
            assert (type(self.src_object) == obj_type), \
                "Dest type must be the same as Src type"

        self.dst_object = BiologicalObject.test_object(session,
                                                       bio_id,
                                                       obj_type,
                                                       origin)

#        print("dst TYPE:", self.dst_object.__name__)


    def get_attributes(self):
        """Return a dictionary of attributes used in the website.

        :return: Dictionary of useful attributes.
        :rtype: <dict>
        """
        return {'dst': self.dst_object.get_attributes() \
                    if (self.dst_object is not None) else 'null',
                'src': self.src_object.get_attributes(),
                'id': self.id,
                'sim_score': self.sim_score,
                'validation_community': self.validation_community,
                'validation_trusted': self.validation_trusted,
                'step': self.step.name
               }

    @classmethod
    def verify_object(cls, session, experiment_id, src_object_id, dst_object_id=None):
        """This function verifies integrity constraints on:
            - exp id,
            - src object id,
            - dst object id (if not None)

        This is a similar function to test_object in other objects but here,
        there, is no creation of the object if it is not in database.
        This is why, we called it 'verify_object' instead of 'test_object'.

        TL;DR: Returns the Association if it is already in DB,
            otherwise returns None

        :param arg1: SQLAlchemy session.
        :param arg2: Experiment id.
        :param arg3: Src object id.
        :param arg4: Dst object id.
        :type arg1: <SQL session object>
        :type arg2: <int>
        :type arg3: <int>
        :type arg4: <int>
        :return: Association if it is already in DB, otherwise returns None.
        :rtype: None or <Association>
        """
        try:
            # one() raises an exception if no result or if more than 1 result
            query = session.query(cls).filter_by(
                        experiment_id=experiment_id,
                        src_object_id=src_object_id,
                        dst_object_id=dst_object_id).one()
            LOGGER.debug(str(query.id) + " is already in database")
            # Return the object
            return query
        except NoResultFound:
            LOGGER.debug(
                "Association(exp_id:{}, src_id:{}, dst_id:{}) not in database".format(
                    experiment_id,
                    src_object_id,
                    dst_object_id))
            return None
        except MultipleResultsFound:
            # This error makes the program crash. We can't continue on it.
            # Begin a new transaction after exception
            # All flush & pending objects will be deleted !
            session.rollback()
            raise


    def create_association(self, session, bio_objs):
        """This function searches if the objects are in associations with
        'validation' status and if these associations have the same destination
        origin than the current one.
        If it is the case, we copy these associations and attach them to the
        current Experiment.

        .. note:: Used by set_biological_objects() during the validation of
            a new Association.

        :param arg1: SQLAlchemy session.
        :param arg2: Iterable of BiologicalObjects that may be involved in
            the current association.
        :type arg1: <SQL session object>
        :type arg2: <list>
        """

        # Get associations attached to each object (as src or dst)
        # Filter only if associations have the 'validation' status
        # (by community or trusted user)
        # Filter also on destination origin

        g_assoc_src = (bio_obj.assoc_src_objects for bio_obj in bio_objs)
        g_assoc_src = (assoc for assoc in it.chain(*g_assoc_src)
                       if assoc.status.id == self.sta_va.id
                       and assoc.dst_object.origin is self.experiment.dst_origin)

#        g_assoc_dst = (bio_obj.assoc_dst_objects for bio_obj in bio_objs)
#        g_assoc_dst = (assoc for assoc in it.chain(*g_assoc_dst)
#                       if assoc.status.id == self.sta_va.id)

        # Merge generators
        # Avoid duplicate entries
        # In case of same metabolite in 2 versions of metacyc for example
        # (metacyc 18 & 19); the same association could appear twice;
        # With a set the association will appear only once.
#        valid_assocs = {obj for obj in it.chain(g_assoc_src, g_assoc_dst)}

        valid_assocs = {assoc for assoc in g_assoc_src}

        LOGGER.debug("Create association : unique set : " + str(valid_assocs))

        # We take all these associations and duplicate them specially for
        # the current experiment.
        # The new association will have the status "proposal"

        return [self.duplicate_association(session, association)
                for association in valid_assocs]


    def duplicate_association(self, session, association):
        """This function takes an Association, duplicates it, and then,
        attaches it to the current experiment.

        .. warning:: used only by create_association().

        .. note:: This function verifies if the association that we attribute
            to a user is not opposed to the integrity constraint.

        :param arg1: SQLAlchemy session.
        :param arg2: Association object that will be duplicated because it
            has the correct Status and it already contains the BiologicalObject.
        :type arg1: <SQL session object>
        :type arg2: <Association>
        :return:
        :rtype:
        """

#TODO: Gérer le cas obj dst /src .. réorienter l'assoc qu'on s'apprete à dupliquer


        # PS: why self.experiment.id and not self.experiment_id ?
        # Because the current Association is not currently commited nor flushed,
        # so internal fields are not filled.
        # Whereas the Experiment object attached, is already committed !
        object = Association.verify_object(
                    session,
                    self.experiment.id,
                    association.src_object_id,
                    dst_object_id=association.dst_object_id)

        # If the association we will create later is already in database,
        # we stop the duplication process.
        if object is not None:
            LOGGER.debug("Stop duplication !")
            return

        # removes the association from the Session,
        # sending persistent instances to the detached state,
        # and pending instances to the transient state:
        # http://docs.sqlalchemy.org/en/latest/orm/session_state_management.html#expunging
        session.expunge(association)

        # Transient - an instance that’s not in a session,
        # and is not saved to the database; i.e. it has no database identity.
        # The function will remove its association with any Session
        make_transient(association)
        association.id = None
        association.experiment = self.experiment
        association.status = self.sta_prop
        session.add(association)
        LOGGER.debug("Association duplication after: {}".format(association))
        # TODO: verifier le bien fondé d'un flush ici...
        # l'objet association n'est pas fini à ce stade hein...
        # => déconseillé
        # session.flush()
        return association


    @staticmethod
    def change_status(session, experiment, assoc_id, dst_status):
        """Change status of an association.

        C'est une association partielle que l'on crée ssi l'objet source que
        l'on vient de mettre en annulé n'est pas mappé dans une autre
        association encore active.

        .. note:: La contrainte d'intégrité (déjà présente) de l'association
            annulée l'empechera d'etre recréee par la suite.

        .. note:: We verify if incomplete association is already in DB.

        .. note:: There are commits here !!

        Validation:
            - proposal => already matched
            - cancelled => already matched ?
        Deletion:
            - proposal => cancelled
            - already matched => cancelled

        :param arg1: SQLAlchemy session.
        :param arg2: Experiment which possess the given association id.
        :param arg3: Association id to modify.
        :param arg4: Status to set. It have to be cm.STA_AM or cm.STA_CANC !
        :type arg1: <SQL session object>
        :type arg2: <Experiment>
        :type arg3: <int>
        :type arg4: <Status>
        :return: Nothing or an Exception if something went wrong.
        :rtype: None or Exception or NoResultFound
        """

        if dst_status not in (cm.STA_AM, cm.STA_CANC):
            LOGGER.warning("Destination status is not allowed")
            raise Exception("Destination status is not allowed")

        # Validation: Search proposal, cancelled
        if dst_status == cm.STA_AM:
            query = [assoc for assoc in experiment.associations
                if assoc.id == assoc_id
                and (assoc.status.name == cm.STA_PROP
                or assoc.status.name == cm.STA_CANC)]

        # Deletion: Search already matched, proposal
        elif dst_status == cm.STA_CANC:
            query = [assoc for assoc in experiment.associations
                if assoc.id == assoc_id
                and (assoc.status.name in (cm.STA_AM, cm.STA_PROP, cm.STA_VA))]


        # Exception handling
        if len(query) == 0:
            LOGGER.warning("Association " + str(assoc_id) + " wasn't found.")
            raise NoResultFound("Association " + str(assoc_id) + " wasn't found.")
#        elif len(query) > 1:
#            # This error makes the program crash. We can't continue on it.
#            # Begin a new transaction after exception
#            # All flush & pending objects will be deleted !
#            session.rollback()
#            raise MultipleResultsFound('Multiple results !')


        # There is only 1 result
        marked_assoc = query[0]
        LOGGER.debug("Association " + str(assoc_id) + " was found.")
        # Change status
        marked_assoc.status = Status.test_object(session, dst_status)


        # If it is a validation (from proposal or cancelled) we search all
        # associations with the same source object and proposal status.
        # These associations will be cancelled.
        if dst_status == cm.STA_AM:
            def switch_to_cancelled(association):
                association.status = Status.test_object(session, cm.STA_CANC)

            [switch_to_cancelled(assoc) for assoc in experiment.associations
                if assoc.src_object.id == marked_assoc.src_object.id
                and assoc.status.name == cm.STA_PROP]

            # Commit changes to DB
            session.commit()
            return


        ########################################################################
        # dst_status == cm.STA_AM here:

        # Vérification que l'objet source existe encore dans au moins
        # une autre association complète.
        # Si non, on doit créer une association partielle
        # (remise en jeu de l'objet source)

        # Obj is src in at least 1 full association.
        # If not, we have to recreate an incomplete association.
        full_assocs = \
            [assoc for assoc in experiment.associations
                if marked_assoc.src_object.id == assoc.src_object.id
                and assoc.dst_object is not None
                and assoc.status.name != cm.STA_CANC]

        LOGGER.debug("Full associations with src involved: " + str(full_assocs))

        if len(full_assocs) == 0:
            # Create incomplete association
            # & verify if we have the rights to do it (integrity constraint)
            ret = Association.verify_object(session,
                                            experiment.id,
                                            marked_assoc.src_object_id,
                                            dst_object_id=None)
            if ret is None:
                new_assoc = Association(experiment,
                                        Status.test_object(session, cm.STA_TBM),
                                        marked_assoc.step)
                new_assoc.src_object = marked_assoc.src_object

        # Commit changes to DB
        session.commit()


    def __repr__(self):
        return \
            "Association(id:{}, status_id:{}, step_id:{}, exp_id:{}, src:{}, dst:{})".format(
                self.id,
                self.status_id,
                self.step_id,
                self.experiment_id,
                self.src_object,
                self.dst_object)


if __name__ == "__main__":

    with SQLA_Wrapper() as session:

        exp = Experiment.test_object(session, 212)
#        exp.validate(session)
        exp.delete(session)
        session.delete(exp)

#        vacuum(session)
        exit()

        orig = Origin.test_object(session, cm.O_USRSRC)
        print(exp.get_statistics())

        with session.no_autoflush:


            reac = Reaction.test_object(session, "2DGLCNRxxx", Reaction, orig)

            print(reac)
            print(reac.reagents)
            print(reac.products)
            print(reac.get_reagent_bio_ids())
            print(reac.get_product_bio_ids())

            raise Exception('coucou')
            exit()
            reac = Reaction("2DGLCNRx", orig)
            reac.update_synonyms(session, ["2-dehydro-D-gluconate reductase (NADH)"])

            r1 = Reactant(role="R",
                         metabolite=Metabolite.test_object(session,
                                                                 "2dhglcn",
                                                                 Metabolite,
                                                                 orig)
                        )

            r2 = Reactant(role="R",
                         metabolite=BiologicalObject.test_object(session,
                                                                 "h",
                                                                 Metabolite,
                                                                 orig)
                        )

            r3 = Reactant(role="R",
                         metabolite=BiologicalObject.test_object(session,
                                                                 "nadh",
                                                                 Metabolite,
                                                                 orig)
                        )

            p1 = Reactant(role="P",
                         metabolite=BiologicalObject.test_object(session,
                                                                 "glcn",
                                                                 Metabolite,
                                                                 orig)
                        )

            p2 = Reactant(role="P",
                         metabolite=BiologicalObject.test_object(session,
                                                                 "nad",
                                                                 Metabolite,
                                                                 orig)
                        )

            reac.reactants.append(r1)
            reac.reactants.append(r2)
            reac.reactants.append(r3)
            reac.reactants.append(p1)
            reac.reactants.append(p2)


        exit()
        print("Nb status:", Status.get_number(session))
        print("All status:", Status.get_all(session))
        print("Nb Associations:", Association.get_number(session))
        print("All Associations:", Association.get_all(session))

        # using a session.no_autoflush block if this flush is occurring prematurely)
        # (pymysql.err.IntegrityError)
        # Return a context manager that disables autoflush.

        # Operations that proceed within the with:
        # block will not be subject to flushes occurring upon query access.
        # This is useful when initializing a series of objects which
        # involve existing database queries, where the uncompleted object
        # should not yet be flushed.

#        with session.no_autoflush:
#            assoc = Association(session, 17, STA_AM, STE_EX)
#            assoc.sim_score = 30
#            assoc.set_src(session, "Metab6", O_MET, Metabolite)
#            assoc.set_dst(session, "Metab6", O_BIG, Metabolite)
#
#            # Add object to SQLite DB
#            print("contrainte0?")
#            session.add(assoc)
#            print("contrainte1?")
#            # génère une erreur d'intégrité si l'association existe !
#            try:
#                session.flush()
#            except IntegrityError:
#                session.rollback()
#            print("contrainte2?")
#
#        session.commit()
