# -*- coding: utf-8 -*-
"""
This module is used to check the integrity of files uploaded by a user
when he is creating an experiment.

There are csv, pure text and sbml file supports.

"""

# Standard imports
import csv
from mimetypes import MimeTypes, add_type
import itertools as it
import re
import html

# Custom imports
from matching_kernel import commons as cm

try:
    import libsbml as sbml
except ImportError:
    raise ImportError("python-libsbml seems not to be installed. \
          Please install the module with the following command:\n \
          sudo pip-3.5 install python-libsbml \n \
          or \
          pip-3.5 install --user python-libsbml")


LOGGER = cm.logger()

def detect_mimetype(filepath):
    """Return mimetype of the given file

    :param: Filepath that the mime type is asked.
    :type: <str>
    :return: Valid mimetypes 'text/plain','application/xml' or bad mimetypes.
    :rtype: <str>
    """
    # Add at the last moment, the mimetype for sbml files
    add_type('application/xml', '.sbml')

    # ('text/plain', None)
    # ('application/xml', None)
    return MimeTypes().guess_type(filepath)[0]


def check_integrity_sbml(filepath, **kwargs):
    """Verify if the given sbml file contains a valid model.

    :param: Filepath that the mime type is asked.
    :type: <str>
    :return: An Exception or None
    :rtype: <Exception> or <None>

    """

    # Disable all not required packages of libsbml
    # comp, fbc, layout, qual
    # PS: getAllRegisteredPackageNames() is not functional...
    # Awesome Python port.
    for i in range(0, sbml.SBMLExtensionRegistry.getNumRegisteredPackages()):
        sbml.SBMLExtensionRegistry.disablePackage(
            sbml.SBMLExtensionRegistry.getRegisteredPackageName(i)
        )

    document = sbml.readSBML(filepath)
    level    = document.getLevel()
    version  = document.getVersion()
    model    = document.getModel()

    LOGGER.info("SBML loaded - level:{}, version:{}, model:{}".format(level,
                                                                      version,
                                                                      model))

    # Quick model validation
    if (model == None):
        LOGGER.error("Error: <" + filepath + "> No model present.")
        raise Exception("<" + filepath + "> Incorrect file format")

    # Verification that all identifiers are defined etc..
    check_document_consistency(document)

    # Check validity of identifiers
    check_identifiers_norm(model)


def check_document_consistency(document):
    """Verification that all identifiers are defined etc..

    Serious errors lead to the raise of an exception, while warnings do nothing.

    .. note:: Get all errors linked to a package:
        http://sbml.org/Software/libSBML/docs/python-api/classlibsbml_1_1_s_b_m_l_error.html

        [sbml.__dict__[code] for code in sbml.__dict__
         if 'Fbc' in code and (type(sbml.__dict__[code]) == int)]

    :param: SBML document.
    :type: <libSBML document>
    :return: An Exception or None
    :rtype: <Exception> or <None>

    """

    # Verification that all identifiers are defined etc..
    errors        = document.getNumErrors()
    seriousErrors = False
    numReadErr    = 0
    numReadWarn   = 0
    # End-users notification
    errMsgRead    = ""

    if errors > 0:
        for i in range(errors):
            severity = document.getError(i).getSeverity()

            # Record only serious errors
            if severity in (sbml.LIBSBML_SEV_ERROR, sbml.LIBSBML_SEV_FATAL):
                seriousErrors = True
                numReadErr += 1

                # Example of printed message:
                #  General SBML conformance
                #  line 3
                #  The syntax of 'id' attribute values....
                # PS: getShortMessage() is too short.
                # PS: we escape html entities in the message !
                errMsgRead += \
                    "{}; line {}; <em>{}</em><br>".format(
                        document.getError(i).getCategoryAsString(),
                        document.getError(i).getLine(),
                        html.escape(document.getError(i).getMessage()),
                    )
            else:
                numReadWarn += 1

        # Text formatting for end users & logging
        if seriousErrors:

            error_log = \
                "<strong>Quick validation failed: Some serious ({}) \
                errors were encountered with ({}) warnings. \
                Here is the list of these errors:</strong><br>{}".format(
                    numReadErr,
                    numReadWarn,
                    errMsgRead,
                )

            LOGGER.error(
                "Quick validation failed: Some serious ({}) \
                errors were encountered with ({}) warnings. \
                Here is the list of these errors: {}"
                "".format(
                    numReadErr,
                    numReadWarn,
                    document.getErrorLog().toString(),
                )
            )
            raise Exception(error_log)

        LOGGER.debug(
            "Quick validation: {} warnings. {}"
            "".format(numReadWarn,
                      document.getErrorLog().toString()
            )
        )


def check_identifiers_norm(model):
    """Check if identifiers respect the SBML norm.

    If there are bad identifiers, an exception is raised.

    .. note:: The specifications of SBML format are available at this url:
        https://sourceforge.net/projects/sbml/files/specifications/.

        SIds have to match this regular expression: ``[a-ZA-Z0-9_]``.
        No more, no less !

    :param: SBML model.
    :type: <libSBML Model>
    :return: An Exception or None
    :rtype: <Exception> or <None>

    """

    g_species_ids = \
        (specie.getId().upper() for specie in model.getListOfSpecies())
    g_reactions_ids = \
        (reac.getId().upper() for reac in model.getListOfReactions())

    all_ids = it.chain(g_species_ids, g_reactions_ids)

    # Verification of identifiers charset
    reg_sbml_norm = re.compile('^[A-Z0-9_]*$')

    for id in all_ids:
        if reg_sbml_norm.match(id) is None:
            LOGGER.error("Error: The given file is not a valid SBML file.")
            raise Exception("Oh no ! Id <" + id + "> in the given file is not "
                            "a valid SId type. Please take a look at "
                            "https://sourceforge.net/projects/sbml/files/specifications/")


def check_integrity_text(filepath, filetype=''):
    """Check integrity of a text file.

    .. note:: If filetype is 'input', the given file will be processed
        as a csv file, i.e. as a mapping file made by hand.
        In other case, the file is processed as a simple text file
        (1 id per line)

        :Example of mapping file:
            .. code-block:: bash

                SOURCE DESTINATION FORMULA ECNUMBER
                M1     M2          C6H12O6
                R1     R2                  EC1

    .. note:: For CSV files, only the delimiters `;\\t` are accepted.

    :param arg1: Filepath that the mime type is asked.
    :param arg2:
    :type arg1: <str>
    :type arg2: <str>
    :return: An Exception or None
    :rtype: <Exception> or <None>
    """

    # Handle file as a csv (mapping file made by hand)
    if filetype is not "input":
        with open(filepath, 'r') as csvfile:
            try:
                # Detect settings of the csv file
                dialect = csv.Sniffer().sniff(csvfile.read(), delimiters=',;\t')
#                print(dialect.__dict__)
            except csv.Error as e:
                LOGGER.error("Error: <" + filepath + ">" + str(e))
                raise

            # Rewind the file
            csvfile.seek(0)

            good_headers = {'SOURCE', 'DESTINATION', 'FORMULA', 'ECNUMBER'}
            s = set(csv.DictReader(csvfile, dialect=dialect).fieldnames)
            LOGGER.debug("Check: Set of headers in given csv file:" + str(s))

            if good_headers != s:
                LOGGER.error("Error: <" + filepath + "> Bad headers in csv file.")
                raise Exception("Error: Mapping file: Bad headers in csv file. "
                                "Expected: " + str(good_headers))

    else:
        # Handle file as a txt file
        # We want to crash the program if we could split the first line
        # on delimiters ' \t;'.
        with open(filepath, 'r') as csvfile:

            def auto_rewind(sep):
                # Called at each generator iteration
                # We rewind the current file & return a csv reader
                # with correct separator
                csvfile.seek(0)
                return csv.reader(csvfile, delimiter=sep)

            g = (auto_rewind(sep) for sep in (' \t;'))

            error = {True for reader in g for row in reader if len(row) != 1}

            if True in error:

                LOGGER.error("Error: <" + filepath + \
                    "> Input file must have only one ID per line.")
                raise Exception("Input file must have only one ID per line.")


def check_integrity(filepath, filetype=''):
    """Entry point: Check integrity of the given file; Autodetect the mimetype.

    :param arg1: Filepath that the mime type is asked.
    :param arg2: (Optional) Type of file : 'input' or '' (default)
        For more informations, take a look at `check_integrity_text()`.
    :type arg1: <str>
    :type arg2: <str>
    :return: The result of check functions : Exception or None
    :rtype: <Exception> or <None>
    """

    LOGGER.debug(filepath + ';' + detect_mimetype(filepath))

    mime_funcs = {'text/plain': check_integrity_text,
                  'text/csv': check_integrity_text,
                  'application/xml': check_integrity_sbml
                 }

    return mime_funcs[detect_mimetype(filepath)](filepath, filetype=filetype)


if __name__ == "__main__":
    check_integrity(
        '/media/DATA/Projets/m2_stage/matching_kernel/data/AraGEM_Camille.sbml', 'input')
