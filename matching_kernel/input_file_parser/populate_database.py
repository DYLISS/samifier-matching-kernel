# -*- coding: utf-8 -*-
"""
This module is used to fill the database with files uploaded by a user
when he is creating an experiment.

There are csv, pure text and sbml file supports.

"""

# Standard imports
import csv
import re
from collections import defaultdict
from traceback import print_tb
from functools import partial
from logging import DEBUG
try:
    import libsbml as sbml
except ImportError:
    raise ImportError("python-libsbml seems not to be installed. \
          Please install the module with the following command:\n \
          sudo pip install python-libsbml \n \
          or \
          pip install --user python-libsbml")

# Custom imports
from matching_kernel import commons as cm
from matching_kernel.unifier_database import *
from matching_kernel.input_file_parser.check_integrity import detect_mimetype

import matching_kernel.convert as cv


LOGGER = cm.logger()

def get_notes_field(notes, element):
    """Extract formula or ecnumber from notes of specie or reaction element.

    .. note:: The format of 'notes' field comes from SBML level 2 format.
        ".*<html:p>FORMULA: (.*)</html:p>.*" or ".*<p>FORMULA: (.*)</p>.*"
        "<html:p>PROTEIN_CLASS: 2.4.1.175</html:p>" or "<p>EC: 2.6.1.1</p>"

    :param arg1: Notes string.
    :param arg2: 'ecnumber' or 'formula' is the type of the returned element.
    :return: Formula or EC number string. None if no matching field.
    :type arg1: <str>
    :type arg2: <str>
    :rtype: <str> or None
    """

    assert element in ('ecnumber', 'formula')

    if element == 'formula':
        expr = re.compile(
            '.*(<html:p>|<p>)(FORMULA): (.*)(</html:p>|</p>).*'
        )
    else:
        expr = re.compile(
            '.*(<html:p>|<p>)(EC Number|PROTEIN_CLASS|EC): (([0-9]{1,3}\.){3}[0-9]{1,4})(</html:p>|</p>).*'
        )

    for line in notes.split('\n'):
        try:
            return expr.match(line).group(3)
        except AttributeError:
            # Not found
            pass


def get_fbc_formula(fbc_specie):
    """Get formula for the given specie if SBML file has fbc.

    .. note:: fbc (flux balancing constraints) must be in sbml level 3,
        fbc ver = 2.

    .. note:: http://cbmpy.sourceforge.net/reference/_modules/cbmpy/CBXML.html

    .. note:: Formula is encoded according to the Hill system order.
        (https://en.wikipedia.org/wiki/Chemical_formula#Hill_system)

    :param: SBML specie with FBC plugin activated.
    :type: <libsbml.FbcSpeciesPlugin>
    :return: Formula string. None if no matching field.
    :rtype: <str> or None
    """

    return fbc_specie.getChemicalFormula() if fbc_specie else None


def parse_rdf_ressources(cv_terms):
    """Parse rdf annotations in some sbml level 3 v2 /level 2 v5 files.

    These rdf annotations contains mappings...

    .. warning:: If there are multiple keys, only the last one will
        be taken. In the example below, only chebi:698 will be taken.

    .. note:: bigg.metabolite, bigg.reaction, kegg.metabolite, kegg.reaction
        are transformed to bigg, kegg keys.

    .. note:: Examples of resources:
        http://identifiers.org/bigg.metabolite/10fthf
        http://identifiers.org/biocyc/10-FORMYL-THF
        http://identifiers.org/chebi/11304
        http://identifiers.org/chebi/15637
        http://identifiers.org/chebi/19108
        http://identifiers.org/chebi/19109
        http://identifiers.org/reactome/419151
        http://identifiers.org/chebi/57454
        http://identifiers.org/chebi/698
        http://identifiers.org/brenda/BG36253
        http://identifiers.org/kegg.compound/C00234
        http://identifiers.org/hmdb/HMDB00972
        http://identifiers.org/unipathway.compound/UPC00234
        http://identifiers.org/seed/cpd00201

    .. todo:: If the data in annotation is not as expected;
        we will have to handle errors...

    :param: List of rdf terms.
    :type: <libsbml.ListWrapperCVTerm>
    :return: Dictionary of database names and the corresponding mapping.
    :rtype: <dict>
    """

    uri_mappings = dict()

    # Transform bigg.metabolite to bigg, and
    # kegg.metabolite to kegg
    manual_correction = \
        {
            'bigg.metabolite': 'bigg',
            'bigg.reaction': 'bigg',
            'kegg.compound': 'kegg',
            'kegg.reaction': 'kegg',
            'biocyc': 'metacyc',
        }

    # Look at the beautiful pythonic libsbml lists...
    for i in range(cv_terms.getSize()):
        rcs = cv_terms.get(i).getResources()
        for j in range(0, rcs.getLength()):
            print(rcs.getValue(j))

            # Get last element of uri
            rc_uri = rcs.getValue(j).split('/')

            # Feed the dict
            # (search manual correction or take the term as it is)
            uri_mappings[manual_correction.get(rc_uri[-2], rc_uri[-2])] = \
                rc_uri[-1]

    return uri_mappings


def import_sbml_metabolites(session, experiment, species, have_fbc):
    """Import all species from SBML to the database.

    For each specie, we retrieve the names and the identifier.
    An incomplete Association object is created with the specie in src_object,
    and nothing in the dst_object.
    If the specie is already involved in an association under validation,
    the association is duplicated and attached to the current experiment.
    Please take a look at the function `Association.set_biological_objects()`.

    :param arg1: SQLAlchemy session.
    :param arg2: List of species from libsbml.
        Be careful ! This list is not iterable !
    :param arg3: Experiment object which will receive Associations.
    :type arg1: <SQL session object>
    :type arg2: <libsbml.ListOfSpecies>
    :type arg3: <Experiment>
    :return: Nothing or an AssertionError
    :rtype: <AssertionError>
    """

    # Get Step & Status objects
    sta_tbm  = Status.test_object(session, cm.STA_TBM)
    sta_am   = Status.test_object(session, cm.STA_AM)
    ste_ex   = Step.test_object(session, cm.STE_EX)
    ste_exid = Step.test_object(session, cm.STE_EX_ID)
    o_usrsrc = Origin.test_object(session, cm.O_USRSRC)
    o_usrdst = Origin.test_object(session, cm.O_USRDST)
    sta_prop = Status.test_object(session, cm.STA_PROP)
    sta_va   = Status.test_object(session, cm.STA_VA)

    # Dict of bio_ids & unique synonyms
    specie_names   = defaultdict(set)
    specie_formula = dict()
    specie_mapping = dict()

    # PS: Bigg lvl2 export adds '_' to names that begin with a digit
    # In position '0', universal_decoder() returns the id,
    # in '1' it returns the compartment.
    decoder = partial(cv.universal_decoder,
                      source_database=experiment.src_origin.name \
                      if experiment.src_origin is not None else None)


    def clean_elements(specie):

        # Clean identifier
        try:
            specie_proper_id = decoder(specie.getId())[0]
        except TypeError:
            LOGGER.error("SBML: Id <" + specie.getId() + "> "
                         "was decoded to None. User selected bad decoder ?")
            raise Exception("Id <" + specie.getId() + "> was decoded to None. "
                            "User selected bad decoder ?")
        # Clean names
        specie_names[specie_proper_id].add(cv.clean_name(specie.getName()))
        # Get formula
        if have_fbc:
            # SBML file with fbc fields (level 3, fbc ver = 2)
            specie_formula[specie_proper_id] = \
                get_fbc_formula(specie.getPlugin("fbc"))
        else:
            # Fallback to previous version of SBML (notes field)
            specie_formula[specie_proper_id] = \
                get_notes_field(specie.getNotesString(), 'formula')
        # Get possible annotation (sbml level 3 v2 / level 2 v5)
        if specie.isSetAnnotation():
            specie_mapping[specie_proper_id] = \
                parse_rdf_ressources(specie.getCVTerms())


    [clean_elements(specie) for specie in species]

    with session.no_autoflush:
        for src_bio_id, names in specie_names.items():

            LOGGER.debug("SBML: Source Metab identifier: '" + \
                         src_bio_id + "' " + str(names))


            # Create incomplete/complete association
            mapping = specie_mapping.get(src_bio_id, None)
            will_be_complete_assoc = \
                True if mapping and \
                    mapping.get(experiment.dst_origin.name, None) else False

            # Change status to already_matched and step to exact_id if
            # if complete association instead of to_be_matched and exact_name
            if will_be_complete_assoc:
                LOGGER.debug("SBML: Complete association")
                sta = sta_am
                ste = ste_exid
            else:
                LOGGER.debug("SBML: Incomplete association")
                sta = sta_tbm
                ste = ste_ex

            # Create association
            assoc = Association(experiment, sta, ste)
            assoc.use_status(sta_va=sta_va, sta_prop=sta_prop)


            try:
                if will_be_complete_assoc:
                    # Use rdf annotations to construct a full association
                    assoc.set_biological_objects(
                        session,
                        (src_bio_id, o_usrsrc, Metabolite),
                        (mapping[experiment.dst_origin.name], o_usrdst, Metabolite)
                    )
                    # Set formula
                    #if assoc.dst_object.formula is None:
                    #    assoc.dst_object.formula = specie_formula[src_bio_id]
                else:
                    assoc.set_biological_objects(
                        session,
                        (src_bio_id, o_usrsrc, Metabolite),
                    )
                # Set names
                assoc.src_object.update_synonyms(session, names)
                # Set formula
                assoc.src_object.formula = specie_formula[src_bio_id]
                # session.add(assoc) # Useless => see except below

            except AssertionError as e:
                # Remove the object from the session
                # Why ?
                # Because assoc is in the session since we attached
                # an Experiment to it.
                LOGGER.debug("SBML: Association is not added: " + str(e))
                session.expunge(assoc)

            session.flush()


def import_sbml_reactions(session, experiment, reactions):
    """Import all reactions from SBML to the database.

    For each reaction, we retrieve the name, the identifier and all the
    products & reagents.
    An incomplete Association object is created with the reaction in src_object,
    and nothing in the dst_object.
    If products & reagents are not already in database they are created,
    BUT without their names. For a proper insertion of these species, please
    take a look at `import_sbml_metabolites()` function.
    If the reaction is already involved in an association under validation,
    the association is duplicated and attached to the current experiment.
    Please take a look at the function `Association.set_biological_objects()`.

    :param arg1: SQLAlchemy session.
    :param arg2: List of reactions from libsbml.
        Be careful ! This list is not iterable !
    :param arg3: Experiment object which will receive Associations.
    :type arg1: <SQL session object>
    :type arg2: <libsbml.ListOfReactions>
    :type arg3: <Experiment>
    :return: Nothing or an AssertionError
    :rtype: <AssertionError>
    """

    # Get Step & Status objects
    sta_tbm  = Status.test_object(session, cm.STA_TBM)
    sta_am   = Status.test_object(session, cm.STA_AM)
    ste_ex   = Step.test_object(session, cm.STE_EX)
    ste_exid = Step.test_object(session, cm.STE_EX_ID)
    o_usrsrc = Origin.test_object(session, cm.O_USRSRC)
    o_usrdst = Origin.test_object(session, cm.O_USRDST)
    sta_prop = Status.test_object(session, cm.STA_PROP)
    sta_va   = Status.test_object(session, cm.STA_VA)

    decoder = partial(cv.universal_decoder,
                      source_database=experiment.src_origin.name \
                      if experiment.src_origin is not None else None)

    with session.no_autoflush:

        for reaction in reactions:
            # SBML information retrieving...
            reac_bio_id   = decoder(reaction.getId())[0]
            reac_name     = cv.clean_name(reaction.getName())
            reac_products = \
                {decoder(p.getSpecies())[0]
                    for p in reaction.getListOfProducts()}
            reac_reagents = \
                {decoder(p.getSpecies())[0]
                    for p in reaction.getListOfReactants()}

            LOGGER.debug("SBML: Source Reac identifier: '" + \
                         reac_bio_id + "' '" + reac_name + "'")


            # Get possible annotation (sbml level 3 v2 / level 2 v5)
            mapping = dict()
            if reaction.isSetAnnotation():
                mapping = parse_rdf_ressources(reaction.getCVTerms())

            # Create incomplete/complete association
            will_be_complete_assoc = \
                True if mapping.get(experiment.dst_origin.name, None) else False

            # Change status to already_matched and step to exact_id if
            # if complete association instead of to_be_matched and exact_name
            if will_be_complete_assoc:
                LOGGER.debug("SBML: Complete association")
                sta = sta_am
                ste = ste_exid
            else:
                LOGGER.debug("SBML: Incomplete association")
                sta = sta_tbm
                ste = ste_ex

            # Create incomplete association
            assoc = Association(experiment, sta, ste)
            assoc.use_status(sta_va=sta_va, sta_prop=sta_prop)


            try:
                if will_be_complete_assoc:
                    # Use rdf annotations to construct a full association
                    assoc.set_biological_objects(
                        session,
                        (reac_bio_id, o_usrsrc, Reaction),
                        (mapping[experiment.dst_origin.name], o_usrdst, Reaction)
                    )
                else:
                    assoc.set_biological_objects(
                        session,
                        (reac_bio_id, o_usrsrc, Reaction),
                    )
                # Set names
                assoc.src_object.update_synonyms(session, [reac_name])
                # Set EC number
                assoc.src_object.ecnumber = \
                    get_notes_field(reaction.getNotesString(),
                                    'ecnumber')

                # If reactants is empty, we assume that it's a new reaction
                # => insert reagents & products
                if len(assoc.src_object.reactants) == 0:
                    assoc.src_object.set_reactants_with_ids(
                        session,
                        o_usrsrc,
                        reagents=reac_reagents,
                        products=reac_products
                    )

                if LOGGER.getEffectiveLevel() == DEBUG:
                    LOGGER.debug("SBML: " + str(assoc.src_object.get_attributes()))
                    LOGGER.debug("SBML: " + str(assoc.src_object))
                # session.add(assoc) # Useless => see except below

            except AssertionError as e:
                # Remove the object from the session
                # Why ?
                # Because assoc is in the session since we attached
                # an Experiment to it.
                LOGGER.debug("SBML: Association is not added: " + str(e))
                session.expunge(assoc)

            session.flush()


def handle_text_file(session, filepath, experiment):
    """This function handles a text file (input file).
    All elements are extracted and inserted in the database as Metabolites objs.

    Partial associations are created (only source objs are not None).

    :param arg1: SQLAlchemy session.
    :param arg2: Filepath.
    :param arg3: Experiment object which will receive Associations.
    :type arg1: <SQL session object>
    :type arg2: <str>
    :type arg3: <Experiment>
    :return: All exceptions encountered.
    :rtype: <Exception>
    """

    def all_metabolites_in_text_file(reader):
        """Yield all elem in non empty line.

        :param: CSV reader iterable.
        :type: <csv_reader>
        :return: Generator of elements.
        :rtype: <generator>
        """

        # Set of unique metabolites please !
        # All lines except empty lines
        g = {line[0] for line in reader if len(line) != 0}
        for elem in g:
            yield elem


    # Get Step & Status objects
    sta_tbm  = Status.test_object(session, cm.STA_TBM)
    ste_ex   = Step.test_object(session, cm.STE_EX)
    o_usrsrc = Origin.test_object(session, cm.O_USRSRC)
    sta_prop = Status.test_object(session, cm.STA_PROP)
    sta_va   = Status.test_object(session, cm.STA_VA)

    with open(filepath, 'r') as csvfile:
        # Iterate on csv_reader
        g = all_metabolites_in_text_file(csv.reader(csvfile))


        try:
            with session.no_autoflush:
                for src in g:

                    print(src)
                    # Create incomplete association
                    assoc = Association(experiment, sta_tbm, ste_ex)
                    assoc.use_status(sta_va=sta_va, sta_prop=sta_prop)
                    try:
                        assoc.set_biological_objects(
                            session,
                            (src, o_usrsrc, Metabolite),
                        )
                        session.add(assoc) # Useless => see except below
                        session.flush()
                    except AssertionError:
                        # Remove the object from the session
                        # Why ?
                        # Because assoc is in the session since we attached
                        # an Experiment to it.
                        session.expunge(assoc)
                        pass

        except Exception as e:
            print_tb(e.__traceback__)

            LOGGER.error("TEXT: Exception encountered => ROLLBACK")
            session.rollback()
            raise


def handle_csv_file(session, filepath, experiment):
    """This function handles a csv file (mapping file).
    All elements are extracted and inserted in the database as Metabolites objs.

    Incomplete Associations are fullfilled in this case.

    .. note:: If the user uploads identifiers that are not in his 'input file',
        those identifiers are not inserted !

    .. note:: We expect this kind of file; If you want to change headers, do not
        forget to update the function input_file_parser.check_integrity_text().

        :Example of mapping file:
            .. code-block:: bash

                SOURCE DESTINATION FORMULA ECNUMBER
                M1     M2          C6H12O6
                R1     R2                  EC1

    .. note:: If the csv does not contain destination ids, it's a partial
        mapping file; additional data (formula or EC number) will merely
        update the source object instead of create a full matching.

    :param arg1: SQLAlchemy session.
    :param arg2: Filepath.
    :param arg3: Experiment object which will receive Associations.
    :type arg1: <SQL session object>
    :type arg2: <str>
    :type arg3: <Experiment>
    :return: All exceptions encountered.
    :rtype: <Exception>
    """

    # Get Step & Status objects
    sta_am = Status.test_object(session, cm.STA_AM)
    ste_ex = Step.test_object(session, cm.STE_EX)
    o_usrdst = Origin.test_object(session, cm.O_USRDST)

    with open(filepath, 'r') as csvfile:
        try:
            # Detect settings of the csv file
            dialect = csv.Sniffer().sniff(csvfile.read(), delimiters=',;\t')
#            print(dialect.__dict__)
        except csv.Error as e:
            LOGGER.error("CSV: Error: " + str(e))
            raise

        # Rewind the file
        csvfile.seek(0)

        # Get all to be matched associations
        # Dict of source ids as keys, and associations as values
        tbm_assocs_by_src_bio_id = \
            {assoc.src_object.bio_id: assoc
                for assoc in experiment.get_associations_with_status(cm.STA_TBM)}

        # Iterate over the given file
        try:
            for row_dict in csv.DictReader(csvfile, dialect=dialect):
#                print(row_dict)

                if row_dict['SOURCE'] not in tbm_assocs_by_src_bio_id.keys():
                    continue

                assoc = tbm_assocs_by_src_bio_id[row_dict['SOURCE']]

                # Full or partial csv ?
                if row_dict['DESTINATION'] != '':
                    # Create the destination object

                    dst_object = \
                        BiologicalObject.test_object(
                            session,
                            row_dict['DESTINATION'],
                            type(assoc.src_object),
                            o_usrdst
                        )

                    # Insert formula or OTHER FIELDS !
                    # PS: We don't update fields that are already filled !
                    if type(assoc.src_object) is Metabolite:
                        assoc.src_object.formula = row_dict['FORMULA']

                        if dst_object.formula is None:
                            dst_object.formula = row_dict['FORMULA']

                    elif type(assoc.src_object) is Reaction:
                        assoc.src_object.set_ecnumber([row_dict['ECNUMBER']])

                        if dst_object.ecnumber is None:
                            # Quick verif of length
                            dst_object.set_ecnumber([row_dict['ECNUMBER']])

                    # Update the association
                    assoc.dst_object = dst_object
                    assoc.status = sta_am
                    assoc.step = ste_ex

                else:
                    # Do not update the association but just update src object
                    # with additional data
                    if type(assoc.src_object) is Metabolite:
                        assoc.src_object.formula = row_dict['FORMULA']
                    elif type(assoc.src_object) is Reaction:
                        assoc.src_object.set_ecnumber([row_dict['ECNUMBER']])


        except Exception as e:
            print_tb(e.__traceback__)

            LOGGER.error("CSV: Exception encountered => ROLLBACK")
            session.rollback()
            raise


def handle_sbml_file(session, filepath, experiment):
    """This function handles a SBML file.
    All metabolites & reactions are extracted and inserted in the database.

    Partial associations are created (only source objs are not None).

    .. note:: In case of exception thrown,
        a rollback is done on this entire file
        (which leads to a failure in creating the experiment for the user).

    :param arg1: SQLAlchemy session.
    :param arg2: Filepath.
    :param arg3: Experiment object which will receive Associations.
    :type arg1: <SQL session object>
    :type arg2: <str>
    :type arg3: <Experiment>
    :return: All exceptions encountered.
    :rtype: <Exception>
    """

    def is_model_with_fbc_plugin(model):
        """Detect fbc plugin in SBML model.

        .. notes:: More info at:
            http://sbml.org/Software/libSBML/docs/python-api/classlibsbml_1_1_fbc_species_plugin.html
            http://cbmpy.sourceforge.net/reference/_modules/cbmpy/CBXML.html

        :return: True or False according to fbc presence.
        :rtype: <boolean>
        """

        FBCplg = model.getPlugin(str("fbc"))
        if FBCplg is None:
            return False
        else:
            FBCver    = FBCplg.getPackageVersion()
            FBCstrict = FBCplg.getStrict()

            LOGGER.info('SBML: FBC version: {}'.format(FBCver))

            if FBCver == 2 and not FBCstrict:
                LOGGER.warning(
                    "SBML: This model has fbc:strict=\"false\" this means that \
                    is not necessarily a linear program and may contain a number \
                    of unsupported features containing aribtrary mathematical \
                    expressions such as, InitialAssignments, Rules, Events etc."
                )
            return True

    document = sbml.readSBML(filepath)
    model = document.getModel()

    LOGGER.info("SBML: Load model: " + str(model))

    have_fbc = is_model_with_fbc_plugin(model)

    try:
        import_sbml_metabolites(session, experiment,
                                model.getListOfSpecies(), have_fbc)
        import_sbml_reactions(session, experiment,
                              model.getListOfReactions())
    except Exception as e:
        print_tb(e.__traceback__)

        LOGGER.error("SBML: Exception encountered => ROLLBACK")
        session.rollback()
        raise


def import_all_elements(session, filepath, experiment, filetype=''):
    """Entry point. Open the given file, and attach all metabolites & reactions
    to the given experiment.
    The type of the file is autodetected.

    .. note:: For the moment, case of inputfile we handle it like a pure text file
        (1 element per line).

    :param arg1: SQLAlchemy session.
    :param arg2: Filepath.
    :param arg3: Experiment object which will receive Associations.
    :param arg4: Type of file used to handle as a pure text file or a csv file.
    :type arg1: <SQL session object>
    :type arg2: <str>
    :type arg3: <Experiment>
    :type arg4: <str>
    :return: All exceptions encountered.
    :rtype: <Exception>
    """

    # Workaround: Fix mimetype lack for sbml files
    mime_funcs = {'application/xml': handle_sbml_file}

    # Input ? => pure text file. In other case => csv file.
    method = handle_text_file if filetype is 'input' else handle_csv_file
    mime_funcs['text/plain'] = method
    mime_funcs['text/csv'] = method

    try:
        mime_funcs[detect_mimetype(filepath)](session, filepath, experiment)
    except Exception as e:
        LOGGER.error('Exception: <' + filepath + '> ' + str(type(e)) + str(e))
        raise


if __name__ == "__main__":
    """
    """


#    document = sbml.readSBML("/media/DATA/Projets/m2_stage/matching_kernel/data/iJR904.xml")
    document = sbml.readSBML("/media/DATA/Projets/m2_stage/matching_kernel/data/synechocystis_6803_metabo.xml")
    model = document.getModel()
    print(len(model.getListOfSpecies()))
    print(len(model.getListOfReactions()))

    from matching_kernel import unifier_database as db
    with db.SQLA_Wrapper() as session:
        #exp = db.Experiment.test_object(session, 291)
        exp = db.Experiment.test_object(session, 293)

        r = [r for r in exp.associations if type(r.src_object) == db.Metabolite]
        print(len(r))
        r = [r for r in exp.associations if type(r.src_object) == db.Reaction]
        print(len(r))
    exit()

#    from matching_kernel import unifier_database as db
#    with db.SQLA_Wrapper() as session:
#        handle_csv_file(session, '/media/DATA/Projets/m2_stage/matching_kernel/data/mapping_file.txt', db.Experiment.test_object(session, 196))
#        exit()
#        print(dir(Association))
#        Association.set_biological_objects = partialmethod(Association.set_biological_objects,
#                                                 sta_va=Status.test_object(session, cm.STA_VA),
#                                                 sta_prop=Status.test_object(session, cm.STA_PROP)
#                                                 )
##        setattr(Association, 'set_biological_objects', func)
#        print(Association.set_biological_objects)
#        sta_tbm = Status.test_object(session, cm.STA_TBM)
#        ste_ex = Step.test_object(session, cm.STE_EX)
#        o_usrsrc = Origin.test_object(session, cm.O_USRSRC)
#
#        # def set_biological_objects(self, session, src_data, dst_data=None, sta_va=None, sta_prop=None):
#        assoc = Association(db.Experiment.test_object(session, 170), sta_tbm, ste_ex)
#        assoc.set_biological_objects(
#                                     session,
#                    ("coucou", o_usrsrc, Metabolite),
#                )
#        print(assoc)

    #lvl 3
#    handle_sbml_file('', '/media/DATA/Projets/m2_stage/matching_kernel/data/iJR904.xml', 17)
    # lvl 2
#    handle_sbml_file('', '/media/DATA/Projets/m2_stage/matching_kernel/data/Ec_iJR904.xml', 17)
    exit()
    from matching_kernel import unifier_database as db
    with db.SQLA_Wrapper() as session:
        exp = db.Experiment.test_object(session, 35)
        handle_text_file(session, '/var/www/html/Symfony/web/uploads/user/35/input_file_c95aad18a1ed1fe5ef2ad75ce703b04f.txt', exp)
        handle_csv_file(session, '/var/www/html/Symfony/web/uploads/user/35/input_file_93c5fe95ab7a5a1bec2226671a87e96e.txt', exp)
        print('coucou')
        session.commit()





#Generate Dictonary for MetaCyc
# Read classes.dat (classes from MetaCyc)
# Read compounds.dat (compounds from MetaCyc)
# => 15925 MetaCyc compounds
# Read reactions.dat
# => 12594 MetaCyc reactions

#Generate Dictonary for Ec_iJR904.xml
# Read Ec_iJR904.xml (SBML compounds and reactions)
# => 625 compounds
# => 1075 reactions

#Try exact matching
#0 already unified compounds
#309 new compounds unified
