# -*- coding: utf-8 -*-
"""
Created on Feb 2016

..sectionauthor:: Pierre.VIGNET <pierre.vignet@inria.fr>
"""

# Standard imports
import argparse
import os

# Custom imports
from matching_kernel import commons
from matching_kernel.info import PACKAGE_VERSION  # , PACKAGE_NAME

LOGGER = commons.logger()


def parse_input_file(args):
    """Load compounds & reactions data from raw files (metanetx.org, metacyc)"""
    from matching_kernel import reference_database
    param = args_to_param(args)
    reference_database.main(**param)

def make_postgres_indexes(args):
    """Load compounds & reactions from raw files and make indexes for names"""
    from matching_kernel.reference_database.import_metacyc \
        import load_postgres_indexes

    load_postgres_indexes()

def init_db(args):
    """Initialize the database with structure and objects handled by SQLAlchemy"""
    from matching_kernel.unifier_database import sqla_wrapper as db
    param = args_to_param(args)
    with db.SQLA_Wrapper() as session:
        db.populate_default_database(session, **param)

def test(args):
    import matching_kernel.matching.samir as s
    s.main_temp()

def args_to_param(args):
    """Return argparse namespace as a dict {variable name: value}"""
    return {k: v for k, v in vars(args).items() if k != 'func'}


class readable_dir(argparse.Action):
    """
    http://stackoverflow.com/questions/11415570/directory-path-types-with-argparse
    """

    def __call__(self, parser, namespace, values, option_string=None):
        prospective_dir = values

        if not os.path.isdir(prospective_dir):
            raise argparse.ArgumentTypeError(
                    "readable_dir:{0} is not a valid path".format(prospective_dir))

        if os.access(prospective_dir, os.R_OK):
            setattr(namespace, self.dest, prospective_dir)
        else:
            raise argparse.ArgumentTypeError(
                    "readable_dir:{0} is not a readable dir".format(prospective_dir))


if __name__ == '__main__':
    # parser configuration
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('-v, --version', action='version',
                        version='%(prog)s ' + PACKAGE_VERSION, )
    subparsers = parser.add_subparsers(title='subcommands')

    # subparser: parse input_file
    parser_input_file = subparsers.add_parser('parse_input_file',
                                              help=parse_input_file.__doc__, )
    parser_input_file.set_defaults(func=parse_input_file)

    # subparser: init database
    parser_init_db = subparsers.add_parser('init_db',
                                           help=init_db.__doc__, )
    parser_init_db.set_defaults(func=init_db)

    # subparser: init database indexes
    parser_init_db_indexes = subparsers.add_parser('init_db_idx',
                                           help=make_postgres_indexes.__doc__, )
    parser_init_db_indexes.set_defaults(func=make_postgres_indexes)

    parser_test = subparsers.add_parser('test')
    parser_test.set_defaults(func=test)

    # get program args and launch associated command
    args = parser.parse_args()
    args.func(args)
