# -*- coding: utf-8 -*-
"""
This module is the entry point of the REST API.

.. sectionauthor:: Pierre.VIGNET <pierre.vignet@inria.fr>

"""

# Standard imports
from flask import Flask, jsonify, abort
from collections import Counter
from traceback import print_tb
import os
from logging import DEBUG

# Custom imports
from matching_kernel import commons as cm
from matching_kernel import unifier_database as db
from matching_kernel.input_file_parser import check_integrity, import_all_elements
import matching_kernel.matching as matching
import matching_kernel.output_file_writer as fw

LOGGER = cm.logger()

# jsonify():
# This function’s response will be pretty printed if it was not requested
# with X-Requested-With: XMLHttpRequest to simplify debugging unless the
# JSONIFY_PRETTYPRINT_REGULAR config parameter is set to false.
app = Flask(__name__)
#app.config['JSONIFY_PRETTYPRINT_REGULAR'] = False

# Initialize SQLAlchemy session (flask auto-removes the session later
session = db.loading_sql()
#db.populate_default_database(session)
# Remove all locks on experiments
db.remove_experiments_locks(session)

@app.route('/check_files_integrity/<int:experiment_id>/<string:username>/<string:src_file>',
           methods=['GET'])
@app.route('/check_files_integrity/<int:experiment_id>/<string:username>/<string:src_file>/<string:dst_file>',
           methods=['GET'])
def check_files_integrity(experiment_id=None, username=None, src_file=None, dst_file=None):
    """Check integrity of the files attached to the experiment.

    This route is called when after the peristence of an Experiment by Doctrine.
    If there are errors, the experiment is automatically deleted by Doctrine.

    .. note:: dst_file is not required in http form. This is why there is a
        second route.

    .. note:: There is no route if there is no parameter.
        => The settings never have the value None.

    :Example:

        .. code-block:: javascript
            :linenos:

            {
              "dst_file": null,
              "error": "success",
              "src_file": null
            }

    """

    LOGGER.debug('Receiving: ' + \
                 str(experiment_id) + ', ' + username + ', ' + \
                 src_file + '' if dst_file is None else ',' + dst_file)

    # Test experiment
#    exp = experiment_exists(experiment_id)

    # Check integrity of the files attached to the experiment
    ret = {'error': 'success',
           'src_file': None,
           'dst_file': None
          }
    experiment_path = cm.DIR_SERVER_USER_DATA + username + '/' + \
                      str(experiment_id) + '/'

    # Source file (sbml or pure text file)
    try:
        ret['src_file'] = check_integrity(experiment_path + src_file,
                                          "input")
    except Exception as e:
        print_tb(e.__traceback__)
        ret['error'] = 'error'
        ret['src_file'] = type(e).__name__ + " - " + str(e)
        LOGGER.error('src_file: ' + str(type(e)) + " - " + str(e))


    # Destination file (csv file)
    try:
        if dst_file is not None:
            ret['dst_file'] = check_integrity(experiment_path + dst_file)
    except Exception as e:
        print_tb(e.__traceback__)
        ret['error'] = 'error'
        ret['dst_file'] = type(e).__name__ + " - " + str(e)
        LOGGER.error('dst_file: ' + str(type(e)) + " - " + str(e))

    # Return the state for each file
    LOGGER.debug(str(ret))
    return jsonify(ret)


@app.route('/upload_mapping/<int:experiment_id>/<string:dst_file>',
           methods=['GET'])
def upload_mapping(experiment_id=None, dst_file=None):
    """Merge a mapping file with the database.

    .. note:: At this step, destination file is not persisted
        in bdd by doctrine; i.e, the current experiment contains None or
        the old mapping file.

    .. note:: We expect this kind of file; If you want to change headers, do not
        forget to update the function check_integrity.check_integrity_text().
        For more information see populate_database.handle_csv_file()

        :Example of mapping file:

            .. code-block:: bash

                SOURCE DESTINATION FORMULA ECNUMBER
                M1     M2          C6H12O6
                R1     R2                  EC1

    :Example:

        .. code-block:: javascript
            :linenos:

            {
              "dst_file": null,
              "error": "success",
            }

    """

    # Test experiment
    exp = experiment_exists(experiment_id)
    # PS: we can't lock the experiment here, because Doctrine already holds the
    # lock.

    # Check integrity of the files attached to the experiment
    ret = {'error': 'success',
           'dst_file': None
          }
    experiment_path = cm.DIR_SERVER_USER_DATA + exp.user.username_canonical + \
                      '/' + str(experiment_id) + '/'

    # Check & import destination file (csv file)
    try:
        # Check
        ret['dst_file'] = check_integrity(experiment_path + dst_file)

        # Delete previous mapping file from HDD
        # This step could not be done by Doctrine ><
        # Verif if it is the same file...
        if (exp.dst_file is not None) and (exp.dst_file.name not in dst_file):
            try:
                # Problem in rights attribution...
                os.remove(experiment_path + exp.dst_file.name)
            except:
                pass

        # Delete previous Proposal associations
        exp.delete_proposal(session)

        # Import
        ret['dst_file'] = import_all_elements(session,
                                              experiment_path + dst_file,
                                              exp)

    except Exception as e:
        print_tb(e.__traceback__)
        ret['error'] = 'error'
        ret['dst_file'] = type(e).__name__ + " - " + str(e)
        LOGGER.error('dst_file: ' + str(type(e)) + " - " + str(e))


    # Return the state for each file
    return jsonify(ret)


@app.route('/populate_database/<int:experiment_id>',
           methods=['GET'])
def populate_database(experiment_id=None):
    """Import data taken from input file/mapping file given by a user.

    ..Warning: We ABSOLUTELY NEED to insert the mapping
    uploaded by the user first; Because it creates the BiologicalObjects and
    puts them directly in complete Associations.

    The second file: basic list of ids, creates incomplete Associations
    (only source objects are filled).

    :Example:
        .. code-block:: javascript
            :linenos:

            {
              "dst_file": null,
              "error": "success",
              "src_file": null
            }
    """

    # Test experiment
    exp = experiment_exists(experiment_id)

    # Populates database with the files attached to the experiment
    ret = {'error': 'success',
           'src_file': None,
           'dst_file': None
          }
    experiment_path = cm.DIR_SERVER_USER_DATA + exp.user.username_canonical + \
                      '/' + str(experiment_id) + '/'

    # Source file (sbml or pure text file)
    try:
        ret['src_file'] = import_all_elements(session,
                                          experiment_path + exp.src_file.name,
                                          exp,
                                          "input")
    except Exception as e:
        print_tb(e.__traceback__)
        ret['error'] = 'error'

        if LOGGER.getEffectiveLevel() == DEBUG:
            ret['src_file'] = type(e).__name__ + " - " + str(e)
        else:
            ret['src_file'] = type(e).__name__

    # Destination file (csv file)
    try:
        if exp.dst_file is not None:
            ret['dst_file'] = import_all_elements(session,
                                              experiment_path + exp.dst_file.name,
                                              exp)
    except Exception as e:
        print_tb(e.__traceback__)
        ret['error'] = 'error'

        if LOGGER.getEffectiveLevel() == DEBUG:
            ret['dst_file'] = type(e).__name__ + " - " + str(e)
        else:
            ret['dst_file'] = type(e).__name__

    # Rollback if errors
    if ret['error'] == 'error':
        session.rollback()
    else:
        session.commit()

    # Return the state for each file
    return jsonify(ret)


@app.route('/get_data/<int:experiment_id>/<string:status>',
           methods=['GET'])
def get_data(experiment_id=None, status=None, sort='default', reverse=False):
    """Return all attributes of Associations objets which match the
    given status & experiment.

    This function returns a list of duplicate ids in 'error' list.

    :Example:
        .. code-block:: javascript
            :linenos:

            {
              "associations": [
                {
                  "dst": "null",
                  "id": 47,
                  "sim_score": null,
                  "src": {
                    "formula": "",
                    "id": "a",
                    "ecnumber": "null",
                    "names": [],
                    "origin": "user src"
                  },
                  "step": "exact",
                  "validation_community": 0,
                  "validation_trusted": 0
                },
              "error": []
            }

    :param arg3: Sorting behaviour 'default' (src id first),
        or 'score' (score first).
    :param arg4: Sorting order: True/False
    :type arg3: <str>
    :type arg4: <bool>

    """

    # Test experiment
    exp = experiment_exists(experiment_id)

    # Verification of authorized status
    if status not in cm.AUTH_STATUS:
        abort(404)


    # Hack for administrators
    # => We display only associations with Validation status for Experiment 0
    if exp.user.is_admin() and (experiment_id == 0) and (status == cm.STA_AM):
        status = cm.STA_VA


    # Comparative functions for all full associations
    if status != cm.STA_TBM:
        def default_src_first(assoc):
            # TODO: inverser sim_score / dst id
            return assoc['src']['id'], assoc['dst']['id'], assoc['sim_score']

        def score_first(assoc):
            return assoc['sim_score'], assoc['src']['id'], assoc['dst']['id']

        sorting_behaviour = {'default': default_src_first,
                             'score': score_first,
                            }

    # Comparative functions for all incomplete associations
    # No score, no destination object here !
    else:
        def default_src_first(assoc):
            return assoc['src']['id']

        sorting_behaviour = {'default': default_src_first}


    # Get all associations
    associations = exp.forge_data(session, status)

    # Detect duplication of ids if the status is Already Matched
    # => we can't construct the sbml output in this case
    duplicate_ids = None

    if status == cm.STA_AM:
        source_ids = Counter((assoc['src']['id'] for assoc in associations))

        duplicate_ids = [id for id, number in source_ids.items() if number > 1]

    # Please see the default function default_src_first()
    return jsonify(associations=sorted(associations,
                                       key=sorting_behaviour.get(sort,
                                                                 default_src_first),
                                       reverse=reverse),
                   error=duplicate_ids)


@app.route('/vacuum')
def vacuum():
    """Erase all orphans in database (synonyms + biologicalobjects).

    :Example:
        .. code-block:: javascript
            :linenos:

            {
                "log": "0 BiologicalObjects; 0 Synonyms."
            }
    """

    return jsonify(log=db.vacuum(session))


@app.route('/validate/<int:experiment_id>',
           methods=['GET'])
def validate(experiment_id=None):
    """

    """

    # Test experiment
    exp = experiment_exists(experiment_id)
    try_lock_experiment(exp)

    try:
        # Validate the experiment
        exp.validate(session)

        return jsonify(log='done')
    finally:
        exp.unlock(session)


@app.route('/get_in_validation/<int:experiment_id>',
           methods=['GET'])
def get_in_validation(experiment_id=None):
    """Retrieve associations that have the status "validation".

    :Example:
        .. code-block:: javascript
            :linenos:

            {
                "log": "Associations in validation step duplicated: 10"
                "error": "Exception occured"
            }

    """

    # Test experiment
    exp = experiment_exists(experiment_id)
    try_lock_experiment(exp)

    try:
        matching.get_in_validation(session, exp)

        session.commit()

        return jsonify(log=exp.get_logfile_content())

    except Exception as e:
        print_tb(e.__traceback__)
        LOGGER.error('get in validation: ' + str(type(e)) + " - " + str(e))
        return jsonify(log=None, error=type(e).__name__ + " - " + str(e))
    finally:
        exp.unlock(session)


@app.route('/exact_id_matching/<int:experiment_id>',
           methods=['GET'])
def exact_id_matching(experiment_id=None):
    """Try to match identifiers on reference database.

    :Example:
        .. code-block:: javascript
            :linenos:

            {
                "log": "Exact ids {} matching: {} established matches."
                "error": "Exception occured"
            }

    """

    # Test experiment
    exp = experiment_exists(experiment_id)
    try_lock_experiment(exp)

    try:
        matching.generate_matching_data(session, exp)

        session.commit()

        return jsonify(log=exp.get_logfile_content())

    except Exception as e:
        print_tb(e.__traceback__)
        LOGGER.error('exact id matching: ' + str(type(e)) + " - " + str(e))
        return jsonify(log=None, error=type(e).__name__ + " - " + str(e))
    finally:
        exp.unlock(session)


@app.route('/name_matching/<int:experiment_id>/<string:method>/<string:biological_object_type>',
           methods=['GET'])
def name_matching(experiment_id=None,
                  method=None,
                  biological_object_type=None):
    """This function calls methods in charge of the name matching for
    metabolites or reactions.

    :Example:
        .. code-block:: javascript
            :linenos:

            {
                "log": "Exact name matching: 0 established matches."
                "error": "Exception occured"
            }

    .. todo:: Handle these functions for reactions ?
        Apart from this function, exact_name_matching & inferred_name_matching
        modules are not designed yet to handle reactions...

    :param arg2: Name of the method used to search names: 'exact' or 'inferred'.
    :type arg2: <str>

    """

    # Test experiment
    exp = experiment_exists(experiment_id)
    try_lock_experiment(exp)

    # Test method
    methods = {'exact': matching.exact_name_matching,
               'inferred': matching.inferred_name_matching,
              }

    # Test Biological object types:
    bio_types = {'metabolite': db.Metabolite,
                 'reaction': db.Reaction,
                }

    try:
        # Test of authorized origin/bdd_queried & biological object queried &
        # method
        if (biological_object_type not in bio_types.keys()) \
            or (method not in methods.keys()):
            abort(404)

        methods[method](session,
                        exp,
                        bio_types[biological_object_type])

        session.commit()

        return jsonify(log=exp.get_logfile_content())

    except Exception as e:
        print_tb(e.__traceback__)
        LOGGER.error('name matching: ' + str(type(e)) + " - " + str(e))
        return jsonify(log=None, error=type(e).__name__ + " - " + str(e))
    finally:
        exp.unlock(session)


@app.route('/samir_matching/<int:experiment_id>',
           methods=['GET'])
def samir_matching(experiment_id=None):
    """Try to match identifiers of reactions with samir algorithm.

    :Example:
        .. code-block:: javascript
            :linenos:

            {
                "log": "Samir matching: {} established matches."
                "error": "Exception occured"
            }

    """

    # Test experiment
    exp = experiment_exists(experiment_id)
    try_lock_experiment(exp)

    try:
        matching.samir(session, exp)

        session.commit()

        return jsonify(log=exp.get_logfile_content())

    except Exception as e:
        print_tb(e.__traceback__)
        LOGGER.error('samir matching: ' + str(type(e)) + " - " + str(e))
        return jsonify(log=None, error=type(e).__name__ + " - " + str(e))
    finally:
        exp.unlock(session)


@app.route('/validate_association/<int:experiment_id>/<int:assoc_id>',
           defaults={'status': cm.STA_AM},
           methods=['GET'])
@app.route('/delete_association/<int:experiment_id>/<int:assoc_id>',
           defaults={'status': cm.STA_CANC},
           methods=['GET'])
def modify_association(experiment_id=None, assoc_id=None, status=None):
    """Change status of an association.

    Validation:
            - proposal => already matched
            - cancelled => already matched ?
    Deletion:
            - proposal => cancelled
            - already matched => cancelled

    :Example:
        .. code-block:: javascript
            :linenos:

            {'error': 'success'}
    """

    # Test experiment
    exp = experiment_exists(experiment_id)

    # Verification of authorized status
    if status not in (cm.STA_CANC, cm.STA_AM):
        abort(404)

    ret = {'error': 'success'}
    try:
        db.Association.change_status(session,
                                     exp,
                                     assoc_id,
                                     status)

    except Exception as e:
        print_tb(e.__traceback__)
        ret = {'error': type(e).__name__ + " - " + str(e)}
        LOGGER.error('modify assoc: ' + str(type(e)) + " - " + str(e))

    # Return the state for each file
    return jsonify(ret)


@app.route('/reconstruct_sbml/<int:experiment_id>', methods=['GET'])
def reconstruct_sbml(experiment_id=None):
    """Reconstruct a new SBML file where all already matched identifiers
    replace their original version.

    :Example:
        .. code-block:: javascript
            :linenos:

            {
              "log": "error...."
            }
    """

    # Test experiment
    exp = experiment_exists(experiment_id)
    try_lock_experiment(exp)

    try:
        return jsonify(log=fw.sbml_writer(exp))
    finally:
        exp.unlock(session)


@app.route('/get_statistics/<int:experiment_id>', methods=['GET'])
def get_statistics(experiment_id=None):
    """Get the number of all associations in the current exp,
    grouped by their status.

    :Example:
        .. code-block:: javascript
            :linenos:

            {
              "already matched": 1,
              "cancelled": 2,
              "proposal": 1,
              "to be matched": 6
            }
    """

    # Test experiment
    exp = experiment_exists(experiment_id)

    # Return the counter
    return jsonify(exp.get_statistics())


@app.teardown_appcontext
def shutdown_session(exception=None):
    """Close the SQLAlchemy session => MAJOR IMPROVMENT !!!

    http://flask.pocoo.org/docs/0.10/patterns/sqlalchemy/

    PAY ATTENTION HERE:

    http://stackoverflow.com/questions/21078696/why-is-my-scoped-session-raising-an-attributeerror-session-object-has-no-attr
    """
    LOGGER.debug("SQLA : Closing session...")
    session.commit()
    session.remove()


def experiment_exists(experiment_id):
    """Test if experiment exists.

    :param: experiment id.
    :type: <int>/<str>
    :return: Boolean: True if experiment exists, False otherwise.
    :rtype: <boolean>
    """

    try:
        # Verify if the Experiment exists
        return db.Experiment.test_object(session, experiment_id)
    except Exception as e:
        print_tb(e.__traceback__)
        # Display error
        LOGGER.error("Experiment " + str(experiment_id) + \
            " - " + str(type(e)) + " - " + str(e))
        abort(404)


def try_lock_experiment(experiment):
    """Test if the user can lock the experiment.
    (Try to avoid server overload)

    """

    # Verify if experiment is not locked
    if experiment.lock_state == True:
        LOGGER.error("Experiment " + str(experiment.id) + " - Already locked")
        abort(403)
    else:
        experiment.lock(session)


def main():

    app.run(host='127.0.0.1',
            debug=True)

if __name__ == "__main__":

    main()
