# -*- coding: utf-8 -*-
"""
This module is the config of Gunicorn server.

http://docs.gunicorn.org/en/stable/settings.html#settings

"""

bind = "unix:/run/matching_kernel.sock"

# Switch worker processes to run as this user.
# PS: check rights of files in 'logs' directory
user = "www-data"

# The number of worker processes for handling requests.
#import multiprocessing
# workers = multiprocessing.cpu_count() * 2
workers = 2

# The number of worker threads for handling requests.
# Run each worker with the specified number of threads.
threads = 4

# The maximum number of pending connections.
# This refers to the number of clients that can be waiting to be served.
# Exceeding this number results in the client getting an error when attempting
# to connect. It should only affect servers under significant load.
# backlog = 2048

pid = "/run/matching_kernel.pid"

# A bit mask for the file mode on files written by Gunicorn.
# Note that this affects unix socket permissions.
#umask = 7
