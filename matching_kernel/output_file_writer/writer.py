# -*- coding: utf-8 -*-
"""This modules writes a new SBML file according to the established matches
made by the user.
"""

# Standard imports
from functools import partial
try:
    import libsbml as sbml
except ImportError:
    raise ImportError("python-libsbml seems not to be installed. \
          Please install the module with the following command:\n \
          sudo pip-3.5 install python-libsbml \n \
          or \
          pip-3.5 install --user python-libsbml")

# Custom imports
from matching_kernel import commons as cm
from matching_kernel.unifier_database import sqla_wrapper as db
import matching_kernel.convert as cv

LOGGER = cm.logger()

def entity_renaming(entity, new_id):
    """Rename the given entity"""
    entity.setMetaId(new_id)
    entity.setId(new_id)


def handle_entities(experiment, established_matchs, raw_established_matchs, entities):
    """This function renames species (metabolites) in the SBML file,
    according to the established matches.

    :param arg1: Experiment object.
    :param arg2: Dictionary of matches established by Samifier.
        {src_object.bio_id: dst_object.bio_id}
    :param arg3: Raw identifiers matchs used to handle the references in
        SBML file. This dictionary is modified in place by this func but
        it is not returned by it.
        These identifiers are encoded for the destination database
        with compartment information.
        {original_id: new_id}
    :param arg4: libSBML specie object.
    :type arg1: <Experiment>
    :type arg2: <dict>
    :type arg3: <dict>
    :type arg4: <libSBML specie>
    """

    decoder = partial(cv.universal_decoder,
                      source_database=experiment.src_origin.name if experiment.src_origin is not None else None)
    encoder = partial(cv.universal_encoder,
                      source_database=experiment.dst_origin.name)

    for entity in entities:

        # Get original id,
        # extract entity (reaction or specie) id and compartment
        original_id = entity.getId()
        original_specie_id, compartment = decoder(original_id)

        # print(original_specie_id, compartment)

        # Try to convert the original id
        new_specie_id = established_matchs.get(original_specie_id, None)

        # No matching done here => continue
        if new_specie_id is None:
            continue

        # Encode new_id + compartment
        # in standard format for the bdd querried
        new_id =  encoder(new_specie_id, compartment=compartment)

        # Switch entity's (specie or reaction) id
        entity_renaming(entity, new_id)

        # Memorize the match between raw identifiers
        raw_established_matchs[original_id] = new_id


def handle_references(raw_established_matchs, elements):
    """This function updates all references of established matches in the
    SBML file that the user uploaded.

    .. note:: This function could be quite timeconsuming.
        In fact the complexity of the referencies update is quadratic.

    .. note:: You may want to update other referencies than SIdRefs:

        - Replaces all uses of a given meta identifier attribute value \
        with another value. \
        renameMetaIdRefs(original_id, new_id)

        - Replaces all uses of a given SIdRef type attribute value \
        with another value. \
        renameSIdRefs(original_id, new_id)

        - ???? \
        renameUnitSIdRefs(original_id, new_id)

    :param arg1: Raw identifiers matchs used to handle the references in
        SBML file. These identifiers are encoded for the destination database
        with compartment information.
        {original_id: new_id}
    :param arg2: libSBML elements (all entities).
    :type arg1: <dict>
    :type arg2: <libSBML specie>
    """

    # Note the amazing Pythonic syntax of libSBML... Have fun...
    g = (elements.get(i) for i in range(elements.getSize()))

    # Becareful with the order of this nested comprehension list
    # The generator is "depletable" (épuisable), so while you are reading
    # it only 1 time, you have to read the entier dictionary each time.
    [element.renameSIdRefs(original_id, new_id)
        for element in g
        for original_id, new_id in raw_established_matchs.items()]


def sbml_writer(experiment):
    """This functions handle the update of identifiers and their referencies
    in the SBML file uploaded by the user.

    Matches are taken from the database, from Associations with the status
    "Already Matched".

    .. note:: Remarques au sujet de libSBML:
        Sur le site de la doc, il FAUT connaitre le début du nom
        de la fonction pour pouvoir espérer la trouver...

        Librairie rendue compatible avec Python n'importe comment.

        :Exemple:
            - En C on a la fonction  document->getAllElements()
            - En python: document.getListOfAllElements() ! \
            Pourquoi renommer la fonction ? En plus ce n'est meme pas itérable \
            que l'on récupère là !!


    .. note:: Methods to handle update:

        Manually: https://github.com/mschubert/biomodels_curation/blob/master/SBMLCleanup.py

        From libSBML examples:

            examples/c++/renameSId.cpp
                for (unsigned int i = 0; i < allElements->getSize(); ++i)
                    static_cast<SBase*>(allElements->get(i))->renameSIdRefs(oldSId, newSId);

            examples/python/SetIdFromNames.py
                Classe libsbml.IdentifierTransformer
                document.getModel().renameIDs(allElements, trans); renameAllIds ??

    .. note:: This function could be quite timeconsuming.
        In fact the complexity of the referencies update is quadratic.

    .. note:: For more information about the update of identifiers, please
        take a look at `handle_references()`.

    .. todo:: get the filepath here
    .. todo:: compress final file
    .. todo:: multiprocess for reactions & species

        import gzip
        import shutil
        with open('/home/joe/file.txt', 'rb') as f_in:
            with gzip.open('/home/joe/file.txt.gz', 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)

    :param arg1: Experiment object.
    :type arg1: <Experiment>
    """

    document = sbml.readSBML(experiment.get_server_directory() + \
                             experiment.src_file.name)
    model = document.getModel()


    # Get all association with status "To be matched"
    matched_assocs = experiment.get_associations_with_status(cm.STA_AM)

    # Get all matchs with proper/clean identifiers
    entities_established_matchs = \
        {assoc.src_object.bio_id: assoc.dst_object.bio_id
         for assoc in matched_assocs if type(assoc.src_object) is db.Metabolite}
    reactions_established_matchs = \
        {assoc.src_object.bio_id: assoc.dst_object.bio_id
         for assoc in matched_assocs if type(assoc.src_object) is db.Reaction}

    # Raw identifiers matchs used to handle the references in SBML file
    raw_established_matchs = dict()

    # Handle species
    handle_entities(experiment,
                    entities_established_matchs,
                    raw_established_matchs,
                    model.getListOfSpecies())

    mapped_species = len(raw_established_matchs)
    LOGGER.debug("Writer: Mapped species {}/{}".format(
                    mapped_species,
                    len(model.getListOfSpecies())))

    # Handle species references
    # TODO: can we make this faster ?
    # Take references from reactions:
    # http://sbml.org/Special/Software/libSBML/3.4.1/docs/
    # java-api/org/sbml/libsbml/class-use/ListOfSpeciesReferences.html
    handle_references(raw_established_matchs,
                      document.getListOfAllElements())

    # Handle reactions
    handle_entities(experiment,
                    reactions_established_matchs,
                    raw_established_matchs,
                    model.getListOfReactions())

    LOGGER.debug("Writer: Mapped reactions {}/{}".format(
                    len(raw_established_matchs) - mapped_species,
                    len(model.getListOfReactions())))

    # Write SBML
    sbml.writeSBMLToFile(document,
                         experiment.get_server_directory() + 'output.sbml')


if __name__ == "__main__":

    with db.SQLA_Wrapper() as session:
        # Test experiment
        # raises NoResultFound exception if not found
        experiment = db.Experiment.test_object(session, 25)

        #filepath = "/media/DATA/Projets/m2_stage/matching_kernel/data/iJR904.xml"
        sbml_writer(experiment)

    exit()

    filepath = "/media/DATA/Projets/m2_stage/matching_kernel/data/iJR904.xml"
    document = sbml.readSBML(filepath)
    model = document.getModel()

    print([compartiment for compartiment in model.getListOfCompartments()])

    # Manuellement https://github.com/mschubert/biomodels_curation/blob/master/SBMLCleanup.py

    # examples/c++/renameSId.cpp
    #    for (unsigned int i = 0; i < allElements->getSize(); ++i)
    #        static_cast<SBase*>(allElements->get(i))->renameSIdRefs(oldSId, newSId);

    # examples/python/SetIdFromNames.py
    #    Classe libsbml.IdentifierTransformer
    #    document.getModel().renameIDs(allElements, trans); renameAllIds ??

    # FAUT connaitre le début du nom de la fonction pour pouvoir la trouver dans la doc
    # En C on a la fonction  document->getAllElements()
    # En python: document.getListOfAllElements() LOL et en plus c'est pas itérable

    print([i for i in dir(model) if 'rename' in i])
    print([i for i in dir(document) if 'ements' in i])
    print(help(model.renameIDs))
    specie = document.getElementBySId('M_10fthf_c')
    specie.setMetaId('test')
    specie.setId('test')
    elements = document.getListOfAllElements()
    print(dir(elements))


    g = (elements.get(i) for i in range(elements.getSize()))

    for element in g:
        element.renameSIdRefs('M_10fthf_c', 'test')
#        element.renameMetaIdRefs('M_10fthf_c', 'test')
#        element.renameUnitSIdRefs('M_10fthf_c', 'test') # C'EST QUOI ?


#    document.setModel(model)
#    import time
#    start = time.time()
    # CETTE CONVERSION ne MARCHE PAS ! Par contre ça prend du temps...
#    document.setLevelAndVersion(sbml.SBMLDocument.getDefaultLevel(),
#                                sbml.SBMLDocument.getDefaultVersion())
#    print(time.time() - start)
    # 217.55523371696472
    sbml.writeSBMLToFile(document, 'test.sbml')
