# -*- coding: utf-8 -*-
"""This module is used to retrieve associations that have the status
"validation" where source objects have the same bio_id than association
with the status "to be matched" attached to an experiment.

"""

# Standard imports
from functools import partial

# Custom imports
from matching_kernel.unifier_database import sqla_wrapper as db
from matching_kernel import commons as cm

LOGGER = cm.logger()


def search_and_duplicate(session, sta_va, sta_prop, association):
    """Search all objects with the same bio_id in database.
    Get all associations and duplicate them for the current experiment if they
    have the status "validation". These new associations get the status
    "proposal".

    :param arg1: SQLAlchemy session.
    :param arg2: "Validation" status object.
    :param arg3: "Proposal" status object.
    :param arg4: Current association with a lonely source object.
    :type arg1: <SQL session object>
    :type arg1: <Status>
    :type arg2: <Status>
    :type arg3: <Association>

    """

    # Duplication of all associations with the status "validation",
    # were objects are involved.
    # List of objects with the same id, in the DB
    association.use_status(sta_va, sta_prop)
    src_objects = db.BiologicalObject.test_object(session,
                                                  association.src_object.bio_id,
                                                  type(association.src_object))

    duplicated_associations = \
        association.create_association(session,
                                       src_objects)

    # Association were duplicated => we don't add the current
    # incomplete association to the session
    if len(duplicated_associations) != 0:
        LOGGER.debug(association.src_object.bio_id + \
                    " found in full Validation association: " + \
                     str(len(duplicated_associations)))

        # Delete the original incomplete association from database
        # (replaced by new "proposal" (full) associations)
        session.delete(association)

    return len(duplicated_associations)


def get_in_validation(session, experiment):
    """Entry point for this module.

    This function retrieves associations that have the status
    "validation" where source objects have the same bio_id than association
    with the status "to be matched" attached to an experiment.

    :param arg1: SQLAlchemy session.
    :param arg2: Current experiment.
    :type arg1: <SQL session object>
    :type arg2: <Experiment>

    """

    # Status objects
    sta_prop = db.Status.test_object(session, cm.STA_PROP)
    sta_va   = db.Status.test_object(session, cm.STA_VA)

    convenient_search_and_duplicate = partial(search_and_duplicate,
                                              session,
                                              sta_va,
                                              sta_prop)

    # Delete Proposal associations
    experiment.delete_proposal(session)

    # Get all known names stored in reference index (similarity of 1)

    # Get all association with status "To be matched"
    # AND where source object type matchs the given type
    # => not reactions or not metabolites
    duplications = \
        [convenient_search_and_duplicate(assoc)
            for assoc in experiment.get_associations_with_status(cm.STA_TBM)]

    # Flush changes
    session.flush()

    LOGGER.info("Associations in validation step duplicated: " + \
                str(sum(duplications)))
    experiment.append_to_logfile(
        "Associations in validation step duplicated: " + str(sum(duplications))
    )


if __name__ == "__main__":

    with db.SQLA_Wrapper() as session:
        # Test experiment
        # raises NoResultFound exception if not found
        exp = db.Experiment.test_object(session, 212)

        get_in_validation(session, exp)
