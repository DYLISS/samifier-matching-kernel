# -*- coding: utf-8 -*-
"""This modules is used for the exact name matching.

Documentation about SQLA connections:
    http://docs.sqlalchemy.org/en/latest/core/connections.html

"""

# Standard imports
from sqlalchemy import text
from multiprocessing import Pool, cpu_count
from functools import partial

# Custom imports
import matching_kernel.reference_database as rdb
from matching_kernel.unifier_database import sqla_wrapper as db
from matching_kernel import commons as cm

LOGGER = cm.logger()


def search_known_names_with_trigrams(names, biological_object_type):
    """This function gets exact matches of the given names in databases.
    (i.e names that are already known).

    The biological_object_type is used to:
        - remove any ambiguity while querying names of Metabolites agains a \
        table which also contains names of Reactions.
        - improve the effectiveness of similarity functions that are iterating \
        over the whole table.

    :param arg1: Iterable of names.
    :param arg2: Type of the BiologicalObject queried.
    :type arg1: <list> or <tuple>
    :type arg2: <Metabolite> or <Reaction>
    :return: Dictionary of names:
        key: user_name (bad name), value (good name)

        .. note:: It's an exact matching, so we have 1 result per name queried.

    :rtype: <dict>

    """

    # Get raw cursor (we don't need any ORM here)
    raw_connection = db.simple_engine()

    # Adjust the threshold of the similarity func.
    # We want a score of 1 here but exact 1 forgets some results.
    raw_connection.execute("set pg_similarity." + \
                           cm.PG_SIMILARITY_FUNC + "_threshold to 0.999")

    # Chose table according to the given biological_object_type
    table_name = "metabolites_matching" \
                 if (biological_object_type == db.Metabolite) \
                 else "reactions_matching"

    # Create a temporary table named 'userland' with a single column: 'name'
    # This column contains all asked names
    # All names are compared "official" terms already in text_matching table.
    query = raw_connection.execute(
        text("SELECT \
             userland.name, {table}.name \
             FROM {table}, (select unnest(:names)) AS userland(name) \
             WHERE {table}.name ~%% userland.name".format(table=table_name)),
        names=list(names))

    return {userland_name: text_matching_name
                for userland_name, text_matching_name in query}


def fill_incomplete_associations(session,
                                 experiment,
                                 assocs, biological_object_type,
                                 valid_names, dst_given_name_identifiers,
                                 dst_identifier_synonyms):
    """This function is used to update incomplete associations in the database.

    These associations are updated:
        - Status: To Be Matched => Already Matched
        - Step: => Word
        - Origin: bdd_queried

    If there are multiple matches, new associations are created.

    :param arg1: SQLAlchemy session.
    :param arg2: Current Experiment object.
    :param arg3: Filtered To Be Match associations according to the given
        type of BiologicalObject.
    :param arg4: Type of the BiologicalObject queried.
    :param arg5: Dictionary of names:
        key: user_name (bad name), value (good name)
    :param arg6: All identifiers of each valid name:
            key: valid name, value: identifiers
    :param arg7: All synonyms of each identifier:
             key: identifier, value: synonyms
    :type arg1: <SQL session object>
    :type arg2: <Experiment>
    :type arg3: <list>
    :type arg4: <Metabolite> or <Reaction>
    :type arg5: <dict>
    :type arg6: <dict>
    :type arg7: <dict>

    """

    # Some initializations
    sta_am   = db.Status.test_object(session, cm.STA_AM)
    sta_prop = db.Status.test_object(session, cm.STA_PROP)
    step     = db.Step.test_object(session, cm.STE_WO)

    nb_matchs = 0

    with session.no_autoflush:

        for assoc in assocs:

            # Get set of destination identifiers for the synonyms
            #of the current source
            identifiers = set()
            # Iterate on synonyms of source object
            for syn in assoc.src_object.synonyms:

                try:
                    # valid_names dict converts user synonym to a synonym in db
                    # dst_given_name_identifiers converts valid synonym to id
                    identifiers.update(dst_given_name_identifiers[valid_names[syn.name]])
                except KeyError:
                    pass

            # Error or no identifier ?
            # If there are multiple exact propositions, we switch status to TBM
            if len(identifiers) == 0:
                continue
            elif len(identifiers) > 1:
                status = sta_prop
            else:
                status = sta_am

            # Get all synonyms of valid destination identifiers
            identifiers = {identifier: dst_identifier_synonyms[identifier]
                                for identifier in identifiers}

            # Multiple identifiers for 1 source object
            # (it is possible => Cf "D-Fructose 6-phosphate")
            for num, identifier in enumerate(identifiers):

                dst_object = db.BiologicalObject.test_object(
                                session,
                                identifier,
                                biological_object_type,
                                experiment.dst_origin
                             )

                # In case of new object
                # If we want to test the association later we must have
                # a dst_object with an id (flushed)
                session.flush()

                # Integrity constraint verification
                # Return None if we can create it, the already created object
                #otherwise
                # If None: we raise an assertion,
                #and the Association will be expugned from the current session.
                object = db.Association.verify_object(
                            session,
                            experiment.id,
                            assoc.src_object.id,
                            dst_object_id=dst_object.id
                         )

                # If object is None, we can create this association
                # Otherwise, we delete the destination object from session ?
                # si l'association existe, l'objet est déjà dans la bdd
                # On ne vient donc pas de le recréer
                #=> ne sert à rien de le purger
                if object is not None:
    #                session.expunge(dst_object) ??
                    continue

                if num > 0:
                    # Create empty association in place of the current one
                    # (which is already in session)
                    # Before, we duplicate the source object
                    src_obj = assoc.src_object
                    assoc   = db.Association(experiment, status, step)
                    assoc.src_object = src_obj
                    # Attach the new association to the session
                    session.add(assoc)
                    # Flush & go
                    session.flush()

                # One more match
                nb_matchs += 1

                dst_object.update_synonyms(session,
                                           dst_identifier_synonyms[identifier])

                assoc.dst_object = dst_object
                assoc.status     = status
                assoc.step       = step


    LOGGER.info("Exact name matching: {} established matches.".format(nb_matchs))
    experiment.append_to_logfile(
        "Exact name matching: {} established matches.".format(nb_matchs)
    )

    session.flush()
    session.commit()


def generate_matching_data(session, experiment, biological_object_type):
    """This functions generates all knew matches for the given experiment.

    .. note:: This step uses multiprocessing that is very efficient against a
        classical RDBMS like PostgreSQL.
        We split the query by the number of physical CPU cores.

    Phases:
        - Get all To Be Matched elements
        - Get all their names and query the "index" in PostgreSQL.
        - Get all known names stored in reference index (similarity of 1)
        - Retrieve data from reference database:
            - List of identifiers for 1 given name
            - List of synonyms for 1 given identifier

    .. note:: If you want to get other data from the database, you have to
        query the reference database in order to retrieve this kind of dictionary:
        {identifier: [List of data]}

    .. warning:: If an exception occurs here. The Pool of processes used during
        multiprocessing phase will contain remaining processes. This will be
        problematic until the stop of gunicorn.

    :param arg1: SQLAlchemy session.
    :param arg2: Current Experiment object.
    :param arg3: Type of the BiologicalObject queried.
    :type arg1: <SQL session object>
    :type arg2: <Experiment>
    :type arg3: <Metabolite> or <Reaction>

    """

    # We split the queries by the number of physical CPU cores.
    nb_process = cpu_count() # // 2

    # Unzip iterable
    unzip = lambda l: list(zip(*l))

    ############################################################################

    # Delete Proposal associations
    experiment.delete_proposal(session)

    # Get all known names stored in reference index (similarity of 1)

    # Get all association with status "To be matched"
    # AND where source object type matchs the given type
    # => not reactions or not metabolites
    filtered_to_be_matched_assocs = \
        [assoc for assoc in experiment.get_associations_with_status(cm.STA_TBM)
            if type(assoc.src_object) is biological_object_type]

    # Nothing to do ?
    if len(filtered_to_be_matched_assocs) == 0:
        LOGGER.info("Exact name matching: Nothing to do !")
        experiment.append_to_logfile("Exact name matching: Nothing to do !")
        return

    # Get all names of these associations
    names = {syn.name for assoc in filtered_to_be_matched_assocs
                for syn in assoc.src_object.synonyms}


    # Multiprocessing => split database queries
    # This step of multiprocessing is very efficient against a classical RDBMS
    p = Pool(processes=nb_process)


    try:
        # Results is a list of dictionaries returned
        # by search_known_names_with_trigrams()
        # Ex:
        # {original_value: valid_value}
        # key: user_name (bad name), value (good name)
        results = p.imap(partial(search_known_names_with_trigrams,
                                 biological_object_type=biological_object_type),
                         cm.chunk_this(names, len(names) // nb_process))

        # Merge dictionaries (Python 3.4 workaround, Python 3.5 is much prettier)
        valid_names = cm.merge_dicts(*[result for result in results])


        LOGGER.info("Exact name matching: {} known names.".format(
                len(valid_names)
            )
        )
        experiment.append_to_logfile(
            "Exact name matching: {} known names.".format(len(valid_names))
        )
        # Nothing to do ?
        if len(valid_names) == 0:
            experiment.append_to_logfile(" Nothing to do !")
            # Trust me, you don't want to not do this command:
            p.close()
            return

        ########################################################################

        # Retrieve data from reference database
        # This step of multiprocessing is less efficient against a NOSQL database
        # such like MongoDB.
        # /!\ On SPARQL endpoint, memory consumption can be quite big /!\

        results = p.imap(partial(rdb.search_ids_and_syns_with_names,
                                 bdd_queried=experiment.dst_origin.name),
                         cm.chunk_this(valid_names.values(),
                                    len(valid_names) // nb_process))

        # Results is a list of tuples of dictionaries returned
        # by search_ids_and_syns_with_names()
        #    (
        #        defaultdict(<class 'set'>,
        #            {'D-fructose 6-phosphate': {'CPD-15709', 'FRUCTOSE-6P'}}
        #        ),
        #        defaultdict(<class 'set'>,
        #            {'CPD-15709': {'D-fructose 6-phosphate'},
        #             'FRUCTOSE-6P': {'beta-D-fructofuranose 6-phosphate',
        #                             'fructose-6P'}
        #            }
        #        )
        #    )

        # Merge dictionaries (Python 3.4 workaround, Python 3.5 is much prettier)
    #    dst_given_name_identifiers = dict()
    #    dst_identifier_synonyms    = dict()
    #    for s, i in results:
    #        dst_given_name_identifiers = {**dst_given_name_identifiers, **s}
    #        dst_identifier_synonyms = {**dst_identifier_synonyms, **i}

        dst_given_name_identifiers, dst_identifier_synonyms = \
            unzip([(syns, ids) for syns, ids in results])

        dst_given_name_identifiers = cm.merge_dicts(*dst_given_name_identifiers)
        dst_identifier_synonyms = cm.merge_dicts(*dst_identifier_synonyms)

    except:
        raise
    finally:
        # Ensure the close of subprocesses
        p.close()


    LOGGER.debug("Exact name matching: {} synonyms.".format(
        len(dst_identifier_synonyms))
    )
    LOGGER.debug("Exact name matching: {} identifiers.".format(
        len(dst_given_name_identifiers))
    )

    ############################################################################
    # Don't forget to kill processes here please !!!!
    # Otherwise you will have all remaining processes in memory for eternity.
    p.close()

    # Fill incomplete associations with results
    return session, \
           experiment, \
           filtered_to_be_matched_assocs, biological_object_type, \
           valid_names, dst_given_name_identifiers, \
           dst_identifier_synonyms


def exact_name_matching(session, experiment, biological_object_type):
    """Entry point of name matching.

    :param arg1: SQLAlchemy session.
    :param arg2: Current Experiment object.
    :param arg3: Type of the BiologicalObject queried.
    :type arg1: <SQL session object>
    :type arg2: <Experiment>
    :type arg3: <Metabolite> or <Reaction>

    """

    try:
        fill_incomplete_associations(*generate_matching_data(
            session,
            experiment,
            biological_object_type)
        )
    except TypeError:
        # In case of generate_matching_data returns None (nothing to do)
        pass


if __name__ == "__main__":
#Names:  ['D-Fructose 6-phosphate']
#['CPD-15709', 'FRUCTOSE-6P']
#pause
#1683

#625
#459

#    print(rdb.search_ids_and_syns_with_names(["1-propanol", "propylalcohol"]))
#
#    input('pause: from db ?')

#print(search_known_names_with_trigrams(raw_connection, ["1-propanol", "propylalcohol", "D-Fructose 6-phosphate"]))
    import time

    with db.SQLA_Wrapper() as session:
        # Test experiment
        # raises NoResultFound exception if not found
        exp = db.Experiment.test_object(session, 161)

        start = time.time()
        exact_name_matching(session, exp, db.Metabolite)
        print(time.time() - start)

