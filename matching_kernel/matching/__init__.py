# -*- coding: utf-8 -*-
from matching_kernel.matching.exact_name_matching import exact_name_matching
from matching_kernel.matching.inferred_name_matching import inferred_name_matching
from matching_kernel.matching.get_in_validation import get_in_validation
from matching_kernel.matching.exact_identifier_matching import generate_matching_data
from matching_kernel.matching.samir import samir
