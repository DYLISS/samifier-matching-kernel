# -*- coding: utf-8 -*-
"""This modules is used to match bio_ids with identifiers in
the reference database. It is an exact matching of identifiers.

.. todo:: Il s'agit de croisement de bases de données,
    donc d'interrogation d'ontologies (les bases de données de réseaux
    métaboliques et de composés chimiques sont des ontologies);
    or ici on n'utilise rien de tout cela.
    En attendant, cette fonction est vraiment le minimum que l'on puisse
    faire sur ce projet pour des gens qui veulent mapper des identifiants
    connus dans leurs fichiers.

.. todo:: generate_matching_data() récupère des dictionnaires dans la base de
    données de référence. Ce n'est finalement pas une "bonne" pratique.
    En effet, au moment de l'insertion de ces données dans la BDD Samifier,
    il manque des attributs voir des objets (les métabolites des réactions);
    ces données doivent à nouveau etre requetées dans MongoDB...
    Quelque-soit la base de données utilisée il faut raisonner "objet".
    search_ids_with_ids() devrait retourner des objets qui sont le reflet
    de ce que contient la base de donnée (SPARQL ou Mongo).
    Une interface universelle devrait avoir un niveau d'abstraction suffisant
    pour ne pas tenir compte de la bdd sous-jacente; c'est le principe d'un ORM.
    Mongoengine fait cela; à nous de reproduire ce comportement sur une BDD RDF.
    Il faut reprendre les objets définis dans
    le module MongoWrapper et faire en sorte que n'importe quelle fonction
    manipulant du SPARQL retourne des objets équivalents.
    => des objets universels quelque-soit la BDD utilisée.

"""

# Standard imports
from multiprocessing import Pool, cpu_count
from functools import partial

# Custom imports
import matching_kernel.reference_database as rdb
from matching_kernel.unifier_database import sqla_wrapper as db
from matching_kernel import commons as cm

LOGGER = cm.logger()


def fill_incomplete_associations(session,
                                 experiment,
                                 assocs, biological_object_type,
                                 mapping_src_dst_ids, dst_identifier_synonyms):
    """This function is used to update incomplete associations in the database.

    These associations are updated:
        - Status: To Be Matched => Already Matched
        - Step: => Word
        - Origin: bdd_queried

    If there are multiple matches, new associations are created.

    :param arg1: SQLAlchemy session.
    :param arg2: Current Experiment object.
    :param arg3: Filtered To Be Match associations according to the given
        type of BiologicalObject.
    :param arg4: Type of the Samifier BiologicalObject queried.
    :param arg5: Dictionary of names:
        key: user_name (bad name), value (good name)
    :param arg6: All identifiers of each valid name:
            key: valid name, value: identifiers
    :param arg7: All synonyms of each identifier:
             key: identifier, value: synonyms
    :type arg1: <SQL session object>
    :type arg2: <Experiment>
    :type arg3: <list>
    :type arg4: <Metabolite> or <Reaction>
    :type arg5: <dict>
    :type arg6: <dict>
    :type arg7: <dict>

    """

    # Some initializations
    sta_am   = db.Status.test_object(session, cm.STA_AM)
    sta_prop = db.Status.test_object(session, cm.STA_PROP)
    step     = db.Step.test_object(session, cm.STE_EX_ID)
    dest_database = experiment.dst_origin.name.upper()


    def create_full_metab(metab):
        """Create a new metabolite"""

        # Given metab is a MongoDB metabolite
        object = db.BiologicalObject.test_object(session,
                                                 metab.databases[dest_database][0], #/!\ SIMPLIFIC !!!
                                                 db.Metabolite,
                                                 experiment.dst_origin)
        object.update_synonyms(session, metab.names)
        object.formula = metab.formula

        return object


    def create_full_reac(reac, reagents, products):
        """Create a new reaction"""

        # Given reaction is a MongoDB reaction,
        # reagents & products are Samifier objects.
        object = db.BiologicalObject.test_object(session,
                                                 identifier,
                                                 db.Reaction,
                                                 experiment.dst_origin)

        object.update_synonyms(session, reac.names)
        object.set_ecnumber(reac.databases.get('ECNUMBER', set()))

        object.set_reactants_with_objects(reagents=reagents,
                                          products=products)

        return object


    nb_matchs = 0

    with session.no_autoflush, rdb.Mongo_Wrapper(alias='default'):

        for assoc in assocs:

            src_bio_id = assoc.src_object.bio_id

            # No identifier ?
            # If there are multiple exact propositions, we switch status to TBM
            if src_bio_id not in mapping_src_dst_ids.keys():
                continue
            elif len(mapping_src_dst_ids[src_bio_id]) > 1:
                # Many identifiers
                LOGGER.debug("Exact ids matching: Many identifiers for '{}' => {}".\
                             format(src_bio_id,
                                    mapping_src_dst_ids[src_bio_id])
                )

                status = sta_prop
            else:
                status = sta_am

            # Multiple identifiers for 1 source object
            for num, identifier in enumerate(mapping_src_dst_ids[src_bio_id]):

                # Samifier type test
                if biological_object_type == db.Reaction:
                    # Handle Reactions

                    # Get reaction object...
                    reacs = \
                        rdb.search_document_with_database_id([identifier],
                                                             dest_database,
                                                             bio_object_type=rdb.Reaction)

                    assert (len(reacs) >= 1), identifier + ' not found ?'

                    # Upsert reagents in db
                    reagents = \
                        [create_full_metab(metab) for metab in reacs[0].reagents]
                    # Flush here: avoid duplicate key error in case of metabolites
                    # present twice (in reagents and products) and that were not
                    # in the db before.
                    session.flush()

                    # Upsert products in db
                    products = \
                        [create_full_metab(metab) for metab in reacs[0].products]

                    # Upsert reaction in db
                    dst_object = create_full_reac(reacs[0], reagents, products)

                else:
                    # Upsert Metabolite in db
                    # TODO: For the Metabolites createde here, formula is not set
                    dst_object = db.BiologicalObject.test_object(session,
                                         identifier,
                                         biological_object_type,
                                         experiment.dst_origin)


                # In case of new object
                # If we want to test the association later we must have
                # a dst_object with an id (flushed)
                session.flush()

                # Integrity constraint verification
                # Return None if we can create it, the already created object
                #otherwise
                # If None: we raise an assertion,
                #and the Association will be expugned from the current session.
                object = db.Association.verify_object(
                            session,
                            experiment.id,
                            assoc.src_object.id,
                            dst_object_id=dst_object.id
                         )

                # If object is None, we can create this association
                # Otherwise, we delete the destination object from session ?
                # si l'association existe, l'objet est déjà dans la bdd
                # On ne vient donc pas de le recréer
                #=> ne sert à rien de le purger
                if object is not None:
    #                session.expunge(dst_object) ??
                    continue

                if num > 0:
                    # Create empty association in place of the current one
                    # (which is already in session)
                    # Before, we duplicate the source object
                    src_obj = assoc.src_object
                    assoc   = db.Association(experiment, status, step)
                    assoc.src_object = src_obj
                    # Attach the new association to the session
                    session.add(assoc)
                    # Flush & go
                    session.flush()

                # One more match
                nb_matchs += 1


                assoc.dst_object = dst_object
                assoc.status     = status
                assoc.step       = step


    LOGGER.info("Exact ids {} matching: {} established matches.".\
                format(biological_object_type.__name__,
                       nb_matchs)
    )

    experiment.append_to_logfile(
        "Exact ids {} matching: {} established matches.".\
        format(nb_matchs,
               biological_object_type.__name__)
    )

    session.flush()
    session.commit()


def generate_matching_data(session, experiment):
    """Get all source objects in "to be matched" associations and try to
    match their bio_id with identifiers in the reference database.

    It is an exact matching of identifiers.

    :param arg1: SQLAlchemy session.
    :param arg2: Current experiment.
    :type arg1: <SQL session object>
    :type arg2: <Experiment>

    """

    # Dans le meilleur des mondes où la base de données serait capable de
    # trouver (ou de se passer) de l'origine d'un identifiant pour trouver
    # un document (donc un NoSQL / RDF...) cette condition serait inutile...
    if experiment.src_origin is None:
        return

    # Unzip iterable
    unzip = lambda l: list(zip(*l))

    # Delete Proposal associations
    experiment.delete_proposal(session)

    # We split the queries by the number of physical CPU cores.
    nb_process = cpu_count() # // 2

    # Multiprocessing => split database queries
    # This step of multiprocessing is very efficient against a classical RDBMS
    p = Pool(processes=nb_process)

    # Mapping of objects accross the 2 databases
    objects_equivalence = {db.Metabolite: rdb.Metabolite,
                           db.Reaction: rdb.Reaction
                          }

    try:
        # Reactions, then, Metabolites
        for bio_obj_unifier, bio_obj_ref in objects_equivalence.items():

            # Get all association with status "To be matched"
            # AND where source object type is Reaction or Metabolite
            # Status Already Match = Full association
            to_be_matched_assocs = \
                [assoc for assoc in experiment.get_associations_with_status(cm.STA_TBM)
                    if type(assoc.src_object) is bio_obj_unifier]

            # Get all src identifiers involved in these associations
            src_identifiers = \
                [assoc.src_object.bio_id for assoc in to_be_matched_assocs]


            if len(src_identifiers) == 0:
                continue

            # identifiers, src_bdd, dst_bdd, biological_object_type
    #        mapping_src_dst_ids = rdb.search_ids_with_ids(src_identifiers,
    #                                                      experiment.src_origin.name,
    #                                                      experiment.dst_origin.name,
    #                                                      bio_obj_ref)

            # Results is a list of dictionaries returned
            # by search_known_names_with_trigrams()
            # Ex:
            # {original_value: valid_value}
            # key: user_name (bad name), value (good name)
            results = \
                p.imap(partial(rdb.search_ids_with_ids,
                               src_bdd=experiment.src_origin.name,
                               dst_bdd=experiment.dst_origin.name,
                               biological_object_type=bio_obj_ref),
                       cm.chunk_this(src_identifiers,
                                     len(src_identifiers) // nb_process)
                )

            mapping_src_dst_ids, dst_identifier_synonyms = \
                unzip([(syns, ids) for syns, ids in results])


            # Merge dictionaries (Python 3.4 workaround, Python 3.5 is much prettier)
            mapping_src_dst_ids = cm.merge_dicts(*mapping_src_dst_ids)
            dst_identifier_synonyms = cm.merge_dicts(*dst_identifier_synonyms)

    #        print(mapping_src_dst_ids)
    #        print(dst_identifier_synonyms)
    #        print(len(mapping_src_dst_ids))
            ############################################################################

            fill_incomplete_associations(
                session,
                experiment,
                to_be_matched_assocs, bio_obj_unifier,
                mapping_src_dst_ids, dst_identifier_synonyms
            )

    except:
        raise
    finally:
        # Ensure the close of processes
        p.close()



#    print(all_matched_metabolites.keys())
#    print(filtered_to_be_matched_assocs)
#    print(len(filtered_to_be_matched_assocs))
#    print(len(all_matched_metabolites))


if __name__ == "__main__":

    with db.SQLA_Wrapper() as session:
        # Test experiment
        # raises NoResultFound exception if not found
        exp = db.Experiment.test_object(session, 212)
        generate_matching_data(session, exp)




