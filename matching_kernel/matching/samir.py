# -*- coding: utf-8 -*-
"""This modules is an implementation of SAMIR algorithm.

"""

# Standard imports
from itertools import zip_longest, product
from multiprocessing import Pool, cpu_count
from functools import partial
import jellyfish
from mongoengine import Q
import time

# Custom imports
import matching_kernel.reference_database as rdb
from matching_kernel.unifier_database import sqla_wrapper as db
from matching_kernel import commons as cm


LOGGER = cm.logger()


def multiprocess_this(data, **kwargs):
    """Iterate on the given chunk of tuples, query the reference database,
    and compute the score for each purposed reaction.

    .. note:: Here we are in a separated process. This function is called by
        multiprocess_this().

    :param arg1: List of Tuples of reaction's bio_id, reaction's synonyms,
        reaction's EC Number, reaction's reagents ids & reaction's products ids.
    :param arg2: Named parameter 'dest_database' is expected.
    :return: Dictionary of mapped reactions.

        - keys: reaction's biological id; *
        - values: dictionary of purposed reactions:
            - mongo objects in keys,
            - scores in values.
    :type arg1: <list <<tuple <str>, <list>, <str>, <list>, <list>>>
    :type arg2: <str>
    :rtype: <dict <str>, <dict <db.Reaction>, <float>>>

    """

    dest_database = kwargs['dest_database']

    # Init a new connection
    with rdb.Mongo_Wrapper(alias='default') as (alias, _):

        LOGGER.debug("Mongo multiprocessing: We are in connection " + alias)

        # Partial unpackging (we need SMF_reac_id for the returned dictionary)
        return {SMF_reac_id: compute_scores(SMF_reac_id, tpl, dest_database)
            for SMF_reac_id, *tpl in data}


def compute_scores(SMF_reac_id, data, dest_database):
    """This is the core of SAMIR algorithm.

    Processing:
        - Get lists of reagents & products objects.
        - Get all reactions where these objects are involved.
        - Compute scores based on presence of metabolites, \
        similarity between names, similarity between ec numbers.
        - Mask these scores in 1 final score that priorize them.
            1: EC number similarity
            2: Names similarity
            4: Metabolites similarity
        - Filter final masks (keep above 4; highest mask is 7)

    :param arg1: Reaction's biological identifier.
    :param arg2: Tuple of reaction's synonyms,
        reaction's EC Number, reaction's reagents ids & reaction's products ids.
    :param arg3: Destination database string. Be careful, this string must be
        in uppercase!
    :return: Dictionary of reactions objects as keys, and scores as values.
    :type arg1: <str>
    :type arg2: <tuple <list>, <str>, <list>, <list>>
    :type arg3: <str>
    :rtype: <dict <db.Reaction>, <float>>

    """

    SMF_reac_names, SMF_reac_ecnumber, \
    SMF_products_ids, SMF_reagents_ids = data

#    print(type(SMF_reac_names), type(SMF_reac_ecnumber),
#                      type(SMF_products_ids), type(SMF_reagents_ids),
#                      type(dest_database))

    # Get all docs (MongoObjects) with these ids
    reagents = rdb.search_document_with_database_id(SMF_reagents_ids,
                                                    dest_database,
                                                    bio_object_type=rdb.Metabolite)
    products = rdb.search_document_with_database_id(SMF_products_ids,
                                                    dest_database,
                                                    bio_object_type=rdb.Metabolite)

    # Get all reactions where these metabolites are involved
    # default & rev directions are supported
    intersect = get_intersect(reagents, products)

    # If Intersect is empty we return an empty dict (this will be handled in
    # fill_incomplete_associations() with length of 0 for reacs_with_scores var.
    if len(intersect) == 0:
        return dict()

    # Init the structure that will be returned by this func
    proposed_reactions_with_scores = dict()

    # Scoring
    for proposed_reaction in intersect:
        LOGGER.debug("List proposed ids for '" + SMF_reac_id + "': " +\
                     str(proposed_reaction.databases.get(dest_database, None)))

        # Jaccard index
        metabolites_score = \
            reactant_similarity(reagents, products, proposed_reaction)

        # Personal similarity score
        ecnumber_score = \
            ecnumber_similarity(SMF_reac_ecnumber,
                                proposed_reaction.databases.get('ECNUMBER', None))

        # Jaro similarity
        name_score = \
            name_similarity(SMF_reac_names, proposed_reaction.names)

        LOGGER.debug("Raw scores: m {}, e {}, n{}".format(metabolites_score,
                                                          ecnumber_score,
                                                          name_score))

        masked_score = \
            mask_these_scores(metabolites_score, name_score, ecnumber_score)

        # Filter bad masks
        if masked_score < 4:
            continue

        # Keep the remains
        proposed_reactions_with_scores[proposed_reaction] = masked_score

    LOGGER.debug("Final scores: " + \
                 str(proposed_reactions_with_scores.values()))
    return proposed_reactions_with_scores


def mask_these_scores(metabolites_score, name_score, ecnumber_score):
    """Mask the given scores in 1 final score that priorize them.

    - 1: EC number similarity
    - 2: Names similarity
    - 4: Metabolites similarity

    :param: Scores.
    :type: <float>, <float>, <float>
    :return: Masked score.
    :rtype: <float>

    """

    # Masking step
    metabolites_score = 4 if metabolites_score >= 0.77 else 0
    name_score        = 2 if name_score >= 0.75 else 0
    ecnumber_score    = 1 if ecnumber_score >= 0.75 else 0

    return metabolites_score + ecnumber_score + name_score


def get_samifier_reaction_attributes(association, all_matched_metabolites):
    """Verify if the given association involves only mapped Metabolites.

    If the involved metabolites are not in all_matched_metabolites.keys(),
    this function returns None.

    Otherwise, the structure returned is ready for SAMIR mapping.
    All fields that are returned concern the reaction in Samifier DB
    (not reference database).

    :param arg1: Current tested association.
    :param arg2: Dictionary of mapped metabolites (sources as keys, and
        destinations as values).
    :type arg1: <Association>
    :type arg2: <dict>
    :return: Tuple of reaction's bio_id, reaction's synonyms,
        reaction's EC Number, reaction's reagents ids & reaction's products ids.
    :rtype: <tuple <str>, <list>, <str>, <list>, <list>>

    """

    # Get the source object => reaction object
    reaction = association.src_object

    # Get lists of reagents & products
    reagents_ids = list()
    products_ids = list()

    # Return None if at least 1 reactant is not mapped
    # against the destination DB
    for reactant in reaction.reactants:

        # Stop
        if reactant.metabolite not in all_matched_metabolites.keys():
            return None

        # Get lists of identifiers of reactants (reagents + products)
        if reactant.role == 'P':
            products_ids.append(all_matched_metabolites[reactant.metabolite].bio_id)
        else:
            reagents_ids.append(all_matched_metabolites[reactant.metabolite].bio_id)


    LOGGER.debug("Source DB reac: '" + reaction.bio_id + "';\n"
                 "Mapped reagents: " + str(reagents_ids) + ";\n"
                 "Mapped products: " + str(products_ids))


    # Reaction names, ec number, reagents ids, products ids
    return reaction.bio_id, \
           [syn.name for syn in reaction.synonyms], \
           reaction.ecnumber, reagents_ids, products_ids


def filter_reactions_not_fully_mapped(associations, all_matched_metabolites):
    """Prepare the structure of data for SAMIR algorithm.

    .. note:: This step is necessary for the use of multiprocessing.
        Since the session variable can not be shared between processes,
        We CAN NOT use/persist objects attached to SQLAlchemy ORM in different
        processes. So The returned values are only basic and picklable objects.

    .. note:: Returning generator instead of list,
        may increase memory footprint but for debugging reasons, it's a list.

    :param arg1: Associations to be tested.
    :param arg2: Dictionary of mapped metabolites (sources as keys, and
        destinations as values).
    :type arg1: <Association>
    :type arg2: <dict>
    :return: List of Tuples of reaction's bio_id, reaction's synonyms,
        reaction's EC Number, reaction's reagents ids & reaction's products ids.
    :rtype: <list <<tuple <str>, <list>, <str>, <list>, <list>>>

    """

    reactions_data = \
        (get_samifier_reaction_attributes(
            assoc,
            all_matched_metabolites) for assoc in associations)

    reactions_data = [elem for elem in reactions_data if elem is not None]

    LOGGER.debug("Filtered reactions: " + str(len(reactions_data)))

    return reactions_data


def multiprocess_init(reactions_data, dest_database):
    """Take prepared reactions data, and initialize multiprocessing.

    :param arg1: List of Tuples of reaction's bio_id, reaction's synonyms,
        reaction's EC Number, reaction's reagents ids & reaction's products ids.
        This structure comes from filter_reactions_not_fully_mapped().
    :param arg2: Destination database string. Be careful, this string must be
        in uppercase!
    :type arg1: <list <<tuple <str>, <list>, <str>, <list>, <list>>>
    :type arg2: <str>

    """

#    res = multiprocess_this(reactions_data, dest_database)
#    print(res)
#    exit()

    nb_process = cpu_count() # // 2
    p = Pool(processes=nb_process)

    # Launch multiprocessing & secure it with finally block...
    try:
        # The tuple is chunked in multiple parts
        results = p.imap(partial(multiprocess_this,
                                 dest_database=dest_database),
                         cm.chunk_this(reactions_data,
                                       len(reactions_data) // nb_process))

        # Merge dictionaries (Python 3.4 workaround, Python 3.5 is much prettier)
        reacs_matchs_with_scores = \
            cm.merge_dicts(*[result for result in results])

        LOGGER.debug("Total of potentially mapped reactions: " + \
                     str(len(reacs_matchs_with_scores)))
    except:
        pass
    finally:
        # Don't forget to kill processes here please !!!!
        # Otherwise you will have all remaining processes in memory for eternity.
        p.close()

    # Dict of Samifier reactions in keys,
    # purposed reactions are in values (reac_id in keys and score in values)
    return reacs_matchs_with_scores


def fill_incomplete_associations(session,
                                 experiment,
                                 associations,
                                 reacs_matchs_with_scores):
    """This function is used to update incomplete associations in the database.

    These associations are updated:
        - Status: To Be Matched => Already Matched
        - Step: => Samir
        - Origin: bdd_queried

    If there are multiple matches, new associations are created.
    In this case, all associations have the status of "Proposal".

    .. warning:: If there are many ids for the destination database, only
        the first is taken !
        See the use of db.BiologicalObject.test_object() below.

    .. warning:: EC numbers from mongo are concatenated into a single string.


    :param arg1: SQLAlchemy session.
    :param arg2: Current Experiment object.
    :param arg3: Filtered To Be Match associations according to the given
        type of BiologicalObject.
    :param arg4: Type of the BiologicalObject queried.
    :param arg5: Dictionary of names:
        key: user_name (bad name), value (good name)
    :param arg6: All identifiers of each valid name:
            key: valid name, value: identifiers
    :param arg7: All synonyms of each identifier:
             key: identifier, value: synonyms
    :type arg1: <SQL session object>
    :type arg2: <Experiment>
    :type arg3: <list>
    :type arg4: <Metabolite> or <Reaction>
    :type arg5: <dict>
    :type arg6: <dict>
    :type arg7: <dict>

    """

    # Some initializations
    sta_am   = db.Status.test_object(session, cm.STA_AM)
    sta_prop = db.Status.test_object(session, cm.STA_PROP)
    step     = db.Step.test_object(session, cm.STE_SAMIR)
    dest_database = experiment.dst_origin.name.upper()


    def create_full_metab(metab):
        """Create a new metabolite"""

        # Given metab is a MongoDB metabolite
        object = db.BiologicalObject.test_object(session,
                                                 metab.databases[dest_database][0], #/!\ SIMPLIFIC !!!
                                                 db.Metabolite,
                                                 experiment.dst_origin)
        object.update_synonyms(session, metab.names)
        object.formula = metab.formula

        return object


    def create_full_reac(reac, reagents, products):
        """Create a new reaction"""

        # Given reaction is a MongoDB reaction,
        # reagents & products are Samifier objects.
        object = db.BiologicalObject.test_object(session,
                                                 reac.databases[dest_database][0], #/!\ SIMPLIFIC !!!
                                                 db.Reaction,
                                                 experiment.dst_origin)
        object.update_synonyms(session, reac.names)
        object.set_ecnumber(reac.databases.get('ECNUMBER', None))

        object.set_reactants_with_objects(reagents=reagents,
                                          products=products)

        return object


    nb_matchs = 0

    with session.no_autoflush, rdb.Mongo_Wrapper(alias='default'):

        g = {assoc: reacs_matchs_with_scores[assoc.src_object.bio_id] for assoc in associations
            if assoc.src_object.bio_id in reacs_matchs_with_scores.keys()}

        for assoc, reacs_with_scores in g.items():

            # If there are multiple exact propositions, we switch status to TBM
            if len(reacs_with_scores) == 0:
                continue
            if len(reacs_with_scores) > 1:
                status = sta_prop
            else:
                status = sta_am

            for num, (purposed_reac, score) in enumerate(reacs_with_scores.items()):
                # print("dbs", purposed_reac.databases[dest_database])
                # print("score", score)

                # Upsert reagents in db
                reagents = [create_full_metab(metab) for metab in purposed_reac.reagents]

                # Upsert products in db
                products = [create_full_metab(metab) for metab in purposed_reac.products]

                # Upsert reaction in db
                dst_object = create_full_reac(purposed_reac, reagents, products)

                # In case of new object
                # If we want to test the association later we must have
                # a dst_object with an id (flushed)
                session.flush()

                # Integrity constraint verification for Association
                # Return None if we can create it, the already created object
                #otherwise
                # If None: we raise an assertion,
                #and the Association will be expugned from the current session.
                object = db.Association.verify_object(
                            session,
                            experiment.id,
                            assoc.src_object.id,
                            dst_object_id=dst_object.id
                         )


                # If object is None, we can create this association
                # Otherwise, we delete the destination object from session ?
                # si l'association existe, l'objet est déjà dans la bdd
                # On ne vient donc pas de le recréer
                #=> ne sert à rien de le purger
                if object is not None:
    #                session.expunge(dst_object) ??
                    continue

                if num > 0:
                    # Create empty association in place of the current one
                    # (which is already in session)
                    # Before, we duplicate the source object
                    src_obj = assoc.src_object
                    assoc   = db.Association(experiment, status, step)
                    assoc.src_object = src_obj
                    # Attach the new association to the session
                    session.add(assoc)
                    # Flush & go
                    session.flush()

                # One more match
                nb_matchs += 1

                assoc.dst_object = dst_object
                assoc.status     = status
                assoc.step       = step
                assoc.sim_score  = score




    LOGGER.info("SAMIR matching: {} established matches.".format(nb_matchs))
    experiment.append_to_logfile(
        "SAMIR matching: {} established matches.".format(nb_matchs)
    )

    session.flush()
    session.commit()


def metabolites_to_strings(metabolites):
    """Debug function used in reactant_similarity() if you want to
    print & manipulate Mongo ids instead of the metabolite objects.

    :param: Set of metabolites.
    :return: Set of strings.
    :type: <set>
    :rtype: <set <str>>

    """

    ret = {metab.id for metab in metabolites}
    LOGGER.debug(ret)
    return ret


def get_intersect(reagents, products):
    """Return a list of reactions where the given metabolites are involved.

    .. note:: Reactions are queried in 2 directions (reversible & default dir).

    :param arg1: Iterable of reagents.
    :param arg2: Iterable of products.
    :type arg1: <list>
    :type arg2: <list>
    :return: List of reactions where the given metabolites are involved.
    :rtype: <QuerySet>

    """

    reactions = set(rdb.Reaction.objects(
        (Q(reagents__all=list(reagents)) & Q(products__all=list(products))) |
        (Q(reagents__all=list(products)) & Q(products__all=list(reagents)))
    ))

    LOGGER.debug("Reactions found: " + str(len(reactions)))

##    reactions_r = set(rdb.Reaction.objects(reagents__all=list(reagents)))
##    reactions_p = set(rdb.Reaction.objects(products__all=list(products)))

#    print("reacs found: ", len(reactions_r), len(reactions_p))
#    print("intersect: ", len(reactions_r & reactions_p))
#    return reactions_r & reactions_p
    return reactions


def jaccard_similarity(query:iter, result:iter) -> float:
    """Compute Jaccard index for the 2 given sets of elements.

    :param arg1: Set of Metabolites.
    :param arg2: Set of Metabolites.
    :return: Jaccard index.
    :type arg1: <iter>
    :type arg2: <iter>
    :rtype: <float>

    """

    query  = set(query)
    result = set(result)

    return len(query & result) / len(query | result)


def name_similarity(reac_query_names, reac_result_names):
    """Compute a similarity score between 2 strings (reaction names).

    Jaro similarity function is used.

    .. note:: It is not the same function than PostgreSQL with pg_similarity
        module. This func is provided by Jellyfish module.

    .. note:: If many names for 1 reaction are provided, the best score is
        returned.

    :param arg1: Samifier Reaction names.
    :param arg2: Mongo Reaction names.
    :return: Similarity score. See the paragraph above.
    :type arg1: <iter>
    :type arg2: <iter>
    :rtype: <float>

    """

    # Get names from Samifier
#    reac_query_names = [syn.name for syn in reac_query.synonyms]

    score = 0

    for name_1, name_2 in product(reac_query_names, reac_result_names):

        # Compute against None
        try:
            temp_score = jellyfish.jaro_distance(name_1, name_2)
        except:
            continue

        # Take the best score
        if temp_score > score:
            score = temp_score

    return score


def reactant_similarity(reagents:iter, products:iter, reaction,
                        r_weight=0.5, p_weight=0.5) -> float:
    """Compute a similarity score based on Jaccard index.

    Jaccard index is used for the comparaison of similarity & diversity
    of sets of metabolites. We try to eliminate reactions that involve
    too different metabolites or for which metabolites are lacking or in excess.

    .. todo:: Verify the ponderation if weights are used.

    :param arg1: Reagents taken from MongoDB that are involved in the
        Reaction to be matched (Userland reaction).
    :param arg2: Products taken from MongoDB that are involved in the
        Reaction to be matched (Userland reaction).
    :param arg3: Purposed reaction where the given reactants are involved.
        We try to determine if this reaction has all of them. No more, no less.
    :param arg4: Weight used to ponderate reagents similarity score.
    :param arg5: Weight used to ponderate products similarity score.
    :return: The highest similarity score among the two directions of the reac.
    :type arg1: <iter <Mongo Metabolite>>
    :type arg2: <iter <Mongo Metabolite>>
    :type arg3: <Mongo Reaction>
    :type arg4: <float>
    :type arg5: <float>
    :rtype: <float>

    """

    # /!\ In the future, reagents & products may be generators...
    # so, len() will not be happy...
    LOGGER.debug("Reagents Qry: " + str(len(reagents)) +\
                 "; Rslt:" + str(len(reaction.reagents)))
    LOGGER.debug("Products Qry: " + str(len(products)) +\
                 "; Rslt:" + str(len(reaction.products)))

    default_dir_sim = r_weight * jaccard_similarity(reagents, reaction.reagents) +\
                      p_weight * jaccard_similarity(products, reaction.products)

    rev_dir_sim = p_weight * jaccard_similarity(products, reaction.reagents) +\
                  r_weight * jaccard_similarity(reagents, reaction.products)

    # Debug if you want to print Mongo ids of the metabolites
#    print(jaccard_similarity(metabolites_to_strings(reagents),
#                             metabolites_to_strings(reaction.reagents)),
#          jaccard_similarity(metabolites_to_strings(products),
#                             metabolites_to_strings(reaction.products)))
#
#    print(jaccard_similarity(metabolites_to_strings(products),
#                             metabolites_to_strings(reaction.reagents)),
#          jaccard_similarity(metabolites_to_strings(reagents),
#                             metabolites_to_strings(reaction.products)))

    # The highest similarity is taken
    return default_dir_sim if (default_dir_sim > rev_dir_sim) else rev_dir_sim


def ecnumber_similarity(ec_query, ec_result):
    """Return a similarity score adapted to EC numbers.

    For each number in EC:
        - First, second: MUST be the same, otherwise we return 0.
        - Third: important, add 0.75 if equal, return 0.25 otherwise.
        - Fourth: could me erroneous, add 0.25 if equal, return 0.75 otherwise.

    .. note:: The EC numbers can be incomplete, but MUST be separated by '.'.

    .. warning:: We assume that the queried EC number is a string, unlike the
        result taken from MongoDB which is a list.

    :param arg1: EC Number !.
    :param arg1: EC NumberS !.
    :return: Similarity score. See the paragraph above.
    :type arg1: <str>
    :type arg1: <list>
    :rtype: <float>

    """

    # LOGGER.debug("EC sim: " + str(ec_query) + " vs " + str(ec_result))

    # If one of two EC are None, we can't compute a score
    if ec_query is None or ec_result is None:
        return 0

    assert isinstance(ec_query, str), "'ec_query' must be a string !"
    assert isinstance(ec_result, list), "'ec_result' must be a list !"

    # Inits
    score = 0

    # Transtype
    ec_query = [ec_query]


    def get_score():

        local_score = 0

        # Split EC Number on '.', fullfill incomplete lists with ''
        for i, (query_nb, result_nb) in enumerate(zip_longest(query.split('.'),
                                                              result.split('.'),
                                                              fillvalue='')):
            # First, Second
            if (i in (0, 1)) and (query_nb != result_nb):
                return 0

            # Third
            if i == 2:
                if query_nb == result_nb:
                    local_score += 0.75
                    continue

                return 0.25

            # Fourth
            if i == 3:
                if query_nb == result_nb:
                    local_score += 0.25

                return local_score

    # Take the best score between the EC in ec_query
    # and the multiple EC in ec_result
    for query, result in product(ec_query, ec_result):

        temp_score = get_score()

        if (temp_score > score):
            score = temp_score


    # LOGGER.debug("EC sim score: " + str(score))
    return score


def samir(session, experiment):
    """Entry point of SAMIR algorithm.

    :param arg1: SQLAlchemy session.
    :param arg2: Current Experiment object.
    :type arg1: <SQL session object>
    :type arg2: <Experiment>

    """

    # Delete pending Proposal associations
    experiment.delete_proposal(session)

    # Get all association withs status "To be matched"
    # AND where source object type is a Reaction
    # => not metabolites
    filtered_to_be_matched_assocs = \
        [assoc for assoc in experiment.get_associations_with_status(cm.STA_TBM)
            if type(assoc.src_object) is db.Reaction]

    # Get all mapped metabolites
    # Status Already Match = involved in full associations
    all_matched_metabolites = \
        {assoc.src_object: assoc.dst_object
            for assoc in experiment.get_associations_with_status(cm.STA_AM)
                if type(assoc.src_object) is db.Metabolite}

    LOGGER.debug("Reactions to be matched: " + \
                 str(len(filtered_to_be_matched_assocs)))
    LOGGER.debug("Metabolites mapped: " + \
                 str(len(all_matched_metabolites)))

    dest_database = experiment.dst_origin.name.upper()

    ############################################################################

    # Time it
    start = time.time()

    reacs_matchs_with_scores = \
        multiprocess_init(
            filter_reactions_not_fully_mapped(
                filtered_to_be_matched_assocs,
                all_matched_metabolites),
            dest_database)

    LOGGER.debug("SAMIR did its job in {} seconds".format(time.time() - start))

    ############################################################################

    # Fullfill incomplete associations
    fill_incomplete_associations(session,
                                 experiment,
                                 filtered_to_be_matched_assocs,
                                 reacs_matchs_with_scores)


def main_temp():

    with db.SQLA_Wrapper() as session:
        # Test experiment
        # raises NoResultFound exception if not found
        exp = db.Experiment.test_object(session, 255)
        samir(session, exp)

if __name__ == "__main__":

#
#    print(ecnumber_similarity('1.2.3.4', ['1.2.3']))
#    exit()

    # main_temp()
    pass

