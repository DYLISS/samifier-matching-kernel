# -*- coding: utf-8 -*-
"""This modules is used for the inferred name matching.

.. todo:: Full documentation... Not enough time...

"""

# Standard imports
from sqlalchemy import text
import itertools as it

# Custom imports
import matching_kernel.reference_database as rdb
from matching_kernel import commons as cm
from matching_kernel.unifier_database import sqla_wrapper as db


LOGGER = cm.logger()


def search_known_names_with_trigrams(connection, names, forbidden_names, biological_object_type):
    """
"SELECT \
text_matching.name, " + cm.PG_SIMILARITY_FUNC + "(text_matching.name, userland.name) AS sim \
FROM text_matching, \
  (select unnest(:names)) AS userland(name) \
WHERE text_matching.name " + pg_operator + " userland.name \
ORDER BY sim \
DESC LIMIT :limit"

"SELECT \
text_matching.name, " + cm.PG_SIMILARITY_FUNC + "(text_matching.name, userland.name) AS sim \
FROM text_matching, \
  (select unnest(:names)) AS userland(name), \
  (select unnest(:forbidden_names)) AS forbidden(name) \
WHERE forbidden.name != text_matching.name AND text_matching.name " + pg_operator + " userland.name \
ORDER BY sim \
DESC LIMIT :limit"
    """
#    print(names)

    pg_operator = cm.PG_SIMILARITY_FUNCS_OPS[cm.PG_SIMILARITY_FUNC]
    table_name = "metabolites_matching" \
                 if (biological_object_type == db.Metabolite) \
                 else "reactions_matching"

    if len(forbidden_names) == 0:

        query = connection.execute(
            text("SELECT \
                 {table}.name, {pg_func}({table}.name, userland.name) AS sim \
                 FROM {table}, \
                      (select unnest(:names)) AS userland(name) \
                 WHERE {table}.name {pg_operator} userland.name \
                    ORDER BY sim \
                    DESC LIMIT :limit".format(pg_func=cm.PG_SIMILARITY_FUNC,
                                              pg_operator=pg_operator,
                                              table=table_name)),
            names=list(names), limit=len(names) * 4)

    else:

        query = connection.execute(
            text("SELECT \
                 {table}.name, {pg_func}({table}.name, userland.name) AS sim \
                 FROM {table}, \
                      (select unnest(:names)) AS userland(name), \
                      (select unnest(:forbidden_names)) AS forbidden(name) \
                 WHERE forbidden.name != {table}.name AND {table}.name {pg_operator} userland.name \
                    ORDER BY sim \
                    DESC LIMIT :limit".format(pg_func=cm.PG_SIMILARITY_FUNC,
                                              pg_operator=pg_operator,
                                              table=table_name)),
            names=list(names),
            limit=len(names) * 4,
            forbidden_names=list(forbidden_names))

    return {official_name: score
                for official_name, score in query}


def fill_incomplete_associations(session,
                                 experiment, status, origin, step,
                                 assoc, biological_object_type,
                                 proposed_id_scores,
                                 dst_identifier_synonyms):
    """
    """
    # Match counter
    nb_matchs = 0

    for num, identifier in enumerate(dst_identifier_synonyms.keys()):

        dst_object = db.BiologicalObject.test_object(session,
                                 identifier,
                                 biological_object_type,
                                 origin)

        # In case of new object
        # If we want to test the association later we must have
        # a dst_object with an id (flushed)
        session.flush()

        # Integrity constraint verification
        # Return None if we can create it, the already created object otherwise
        # If None: we raise an assertion, and the Association will be expugned
        # from the current session.
        object = db.Association.verify_object(
                    session,
                    experiment.id,
                    assoc.src_object.id,
                    dst_object_id=dst_object.id
                 )

        # If object is None, we can create this association
        # Otherwise, we delete the destination object from session ?
        # si l'association existe, l'objet est déjà dans la bdd
        # On ne vient donc pas de le recréer => ne sert à rien de le purger
        if object is not None:
#                session.expunge(dst_object) ??
            continue

        if num > 0:
            # Create empty association in place of the current one
            # (which is already in session)
            # Before, we duplicate the source object
            src_obj = assoc.src_object
            assoc = db.Association(experiment, status, step)
            assoc.src_object = src_obj
            # Attach the new association to the session
            session.add(assoc)
            # Flush & go
            session.flush()

        # One more match
        nb_matchs += 1

        dst_object.update_synonyms(session,
                                   dst_identifier_synonyms[identifier])

        assoc.dst_object = dst_object
        assoc.status     = status
        assoc.step       = step
        assoc.sim_score  = round(proposed_id_scores[identifier], 6)

    return nb_matchs



def inferred_name_matching(session, experiment, biological_object_type):
    """
    """
    bdd_queried = experiment.dst_origin.name
    status  = db.Status.test_object(session, cm.STA_PROP)
    origin  = db.Origin.test_object(session, bdd_queried)
    step    = db.Step.test_object(session, cm.STE_WO)

    # Delete Proposal associations
    experiment.delete_proposal(session)

    # Get all associations with status "To be matched"
    to_be_matched_assocs = experiment.get_associations_with_status(cm.STA_TBM)

    # Get all associations with status "Cancelled"
    cancelled_assocs = experiment.get_associations_with_status(cm.STA_CANC)

    # Get all association with status "To be matched" AND where metabolites
    # are involved => not reactions
    to_be_matched_assocs_with_metabs = \
        [assoc for assoc in to_be_matched_assocs
            if type(assoc.src_object) is biological_object_type]

    # Nothing to do ?
    if len(to_be_matched_assocs_with_metabs) == 0:
        LOGGER.info("Exact name matching: Nothing to do !")
        experiment.append_to_logfile("Exact name matching: Nothing to do !")
        return

    # Synonyms to avoid
    # => they are in destination objects in cancelled associations
    g = (assoc.dst_object.synonyms for assoc in cancelled_assocs)
    forbidden_synonyms = {syn.name for syn in it.chain(*g)}

    # Common raw connection
    raw_connection = db.simple_engine()
    raw_connection.execute("set pg_similarity." + cm.PG_SIMILARITY_FUNC + \
                           "_threshold to " + str(cm.PG_SIMILARITY_THRESHOLD))

    def max_score(synonyms):
        """Return max score from a list of synonyms."""
        return max([valid_names.get(synonym, 0) for synonym in synonyms])


    nb_matchs = 0

    for assoc in to_be_matched_assocs_with_metabs:

        synonyms = [syn.name for syn in assoc.src_object.synonyms]

        # Nothing to do
        if len(synonyms) == 0:
            LOGGER.info("Inferred name matching: {} has no synonyms.".format(assoc.src_object.bio_id))
            experiment.append_to_logfile("Inferred name matching: {} has no synonyms.".format(assoc.src_object.bio_id))
            continue

        valid_names = search_known_names_with_trigrams(raw_connection,
                                                       synonyms,
                                                       forbidden_synonyms,
                                                       biological_object_type)

#        print(valid_names)


        dst_given_name_identifiers, dst_identifier_synonyms = \
            rdb.search_ids_and_syns_with_names(valid_names.keys(),
                                               bdd_queried)

#        print(dst_given_name_identifiers)
#        print(dst_identifier_synonyms)


        proposed_id_scores = \
            {identifier: max_score(synonyms)
                for identifier, synonyms in dst_identifier_synonyms.items()}


        best_identifiers = sorted(proposed_id_scores.values(),
                                  reverse=True)[:3]


#        print(best_identifiers)
#        print(proposed_id_scores)

        # Conserve uniquement les id intéressants => autant d'associations à dupliquer
        dst_identifier_synonyms = \
            {identifier: synonyms
                for identifier, synonyms in dst_identifier_synonyms.items()
                    if proposed_id_scores[identifier] in best_identifiers}

#        print(dst_identifier_synonyms)

#            input("pause")

        nb_matchs += fill_incomplete_associations(session,
                             experiment, status, origin, step,
                             assoc, biological_object_type,
                             proposed_id_scores,
                             dst_identifier_synonyms)


    LOGGER.info("Inferred name matching: {} established matches.".format(nb_matchs))
    experiment.append_to_logfile("Inferred name matching: {} established matches.".format(nb_matchs))


if __name__ == "__main__":
#    import jellyfish
#    print(jellyfish.jaro_distance("3-phospho-D-glyceroyl-phosphate", '3-Phospho-D-glyceroyl phosphate'))
#    print(jellyfish.jaro_distance('α-keto-methylvalerate', 'alpha-keto-methylvalerate'))
#    print(jellyfish.damerau_levenshtein_distance('α-keto-methylvalerate', 'alpha-keto-methylvalerate'))
#    exit()
    with db.SQLA_Wrapper() as session:
        # Test experiment
        # raises NoResultFound exception if not found
        exp = db.Experiment.test_object(session, 5)

        inferred_name_matching(session, exp, db.Metabolite, cm.O_MET)
    exit()

